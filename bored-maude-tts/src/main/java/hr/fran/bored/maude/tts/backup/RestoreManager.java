/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.backup;

import hr.fran.bored.maude.tts.Configuration;
import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.common.SteamCloud;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.common.ZipUtils;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModInfo;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class RestoreManager {

    private static final Pattern SAVES_MOD = Pattern.compile("Saves/.+\\.json");

    private static final Pattern WORKSHOP_MOD = Pattern.compile("Mods/Workshop/.+\\.json");

    private static final String TTSMODBACKUP_THUMBNAILS_DIR = "/Thumbnails";

    private static final Configuration CONFIG = Configuration.instance();

    private static final Logger log = LoggerFactory.getLogger(RestoreManager.class);

    private final ModInfoManager modInfoManager;

    private final ModManager modManager;

    public RestoreManager(ModInfoManager modInfoManager, ModManager modManager) {
        this.modInfoManager = modInfoManager;
        this.modManager = modManager;
    }

    public Mod restoreBackup(Path backupFile, Options options) {
        validateModBackupArchive(backupFile, options);

        log.info("Restoring backup from '{}'", backupFile);
        Mod mod = restoreFilesFromBackup(backupFile, options);
        log.info("{} restored from '{}'", mod, backupFile);

        return mod;
    }

    private void validateModBackupArchive(Path backupFile, Options options) {
        Collection<String> zipEntryNames = ZipUtils.applyToZipEntries(backupFile,
                (zipEntry, zipInputStream) -> Optional.of(zipEntry.getName())
        ).toList();

        validateModFile(zipEntryNames);
        validateFileExtensions(zipEntryNames);
        validateFilePaths(zipEntryNames, options);
    }

    private void validateModFile(Collection<String> zipEntryNames) {
        Collection<String> modFiles = zipEntryNames.stream().filter(this::isModFile).toList();
        if (modFiles.size() != 1) {
            throw new TtsException("One mod file expected, %s mods found: [%s]"
                    .formatted(modFiles.size(), String.join(", ", modFiles)));
        }
    }

    private void validateFileExtensions(Collection<String> zipEntryNames) {
        Collection<String> filesWithIllegalExtension = zipEntryNames.stream()
                .filter(filename -> !CONFIG.hasTtsFileExtension(filename))
                .toList();
        if (!filesWithIllegalExtension.isEmpty()) {
            throw new TtsException("Archive contains illegal files: %s"
                    .formatted(String.join(", ", filesWithIllegalExtension)));
        }
    }

    private void validateFilePaths(Collection<String> zipEntryNames, Options options) {
        Collection<String> filesWithIllegalPath = zipEntryNames.stream()
                .filter(zipEntryName -> !isInTtsBackupRestoreDir(zipEntryName, options))
                .toList();
        if (!filesWithIllegalPath.isEmpty()) {
            throw new TtsException("Files cannot be extracted outside Tabletop Simulator directories: [%s]"
                    .formatted(String.join(", ", filesWithIllegalPath)));
        }
    }

    private boolean isInTtsBackupRestoreDir(String zipEntryName, Options options) {
        Path file = getTargetFile(zipEntryName, options);
        Path parent = file.getParent();
        return parent != null && options.getTtsBackupRestoreDirs().contains(parent);
    }

    private Mod restoreFilesFromBackup(Path backupArchive, Options options) {
        ModInfo modInfo = ZipUtils.applyToZipEntries(backupArchive,
                        (zipEntry, zipIn) -> restoreFileFromBackup(zipEntry, zipIn, options)
                ).findFirst()
                .orElseThrow(() -> new TtsException("No mod file found in %s".formatted(backupArchive)));
        return this.modManager.getMod(modInfo, options);
    }

    private Optional<ModInfo> restoreFileFromBackup(ZipEntry zipEntry, ZipInputStream zipIn, Options options) {
        Path tempFile = null;
        try {
            Path targetFile = getTargetFile(zipEntry.getName(), options);
            Path parent = targetFile.getParent();
            if (parent == null) {
                throw new TtsException("Files cannot be extracted outside Tabletop Simulator directories: [%s]"
                        .formatted(zipEntry.getName()));
            }

            tempFile = TtsFileUtils.createTempFile(parent);
            extractZipEntryToFile(zipEntry, zipIn, tempFile);
            TtsFileUtils.move(tempFile, targetFile);

            log.debug("'{}' restored from backup", targetFile);

            if (isModFile(zipEntry.getName())) {
                return Optional.of(this.modInfoManager.getModInfo(targetFile, options));
            }
            return Optional.empty();
        } finally {
            TtsFileUtils.deleteIfExists(tempFile);
        }

    }

    private void extractZipEntryToFile(ZipEntry zipEntry, ZipInputStream zipIn, Path file) {
        try {
            Files.copy(zipIn, file, REPLACE_EXISTING);
            Files.setLastModifiedTime(file, zipEntry.getLastModifiedTime());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Path getTargetFile(String zipEntryName, Options options) {
        String zipPath = zipEntryName.replace(TTSMODBACKUP_THUMBNAILS_DIR, "");
        String newZipPath = SteamCloud.toNewFilename(zipPath);
        return options.getTtsDir().resolve(newZipPath);
    }

    private boolean isModFile(String zipEntryName) {
        return WORKSHOP_MOD.matcher(zipEntryName).matches()
                || SAVES_MOD.matcher(zipEntryName).matches();
    }

}
