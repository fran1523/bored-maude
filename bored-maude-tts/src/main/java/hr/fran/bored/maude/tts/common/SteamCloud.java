/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.common;

import java.util.regex.Pattern;
import java.util.stream.Stream;

public final class SteamCloud {

    private static final String OLD_STEAM_CLOUD_URL = "http://cloud-3.steamusercontent.com/";

    private static final String NEW_STEAM_CLOUD_URL = "https://steamusercontent-a.akamaihd.net/";

    private static final Pattern OLD_STEAM_CLOUD_URL_PATTERN =
            Pattern.compile(OLD_STEAM_CLOUD_URL, Pattern.CASE_INSENSITIVE);

    private static final Pattern NEW_STEAM_CLOUD_URL_PATTERN =
            Pattern.compile(NEW_STEAM_CLOUD_URL, Pattern.CASE_INSENSITIVE);

    private static final Pattern OLD_STEAM_CLOUD_FILE_PATTERN =
            Pattern.compile(TtsFileUtils.getCacheFileBaseName(OLD_STEAM_CLOUD_URL), Pattern.CASE_INSENSITIVE);

    private static final String NEW_STEAM_CLOUD_FILE = TtsFileUtils.getCacheFileBaseName(NEW_STEAM_CLOUD_URL);

    private SteamCloud() {
    }

    public static boolean isSteamCloudUrl(String url) {
        return Stream.of(OLD_STEAM_CLOUD_URL_PATTERN, NEW_STEAM_CLOUD_URL_PATTERN).anyMatch(p -> p.matcher(url).find());
    }

    public static String toNewUrl(String url) {
        return OLD_STEAM_CLOUD_URL_PATTERN.matcher(url).replaceFirst(NEW_STEAM_CLOUD_URL);
    }

    public static String toNewFilename(String filename) {
        return OLD_STEAM_CLOUD_FILE_PATTERN.matcher(filename).replaceFirst(NEW_STEAM_CLOUD_FILE);
    }

}
