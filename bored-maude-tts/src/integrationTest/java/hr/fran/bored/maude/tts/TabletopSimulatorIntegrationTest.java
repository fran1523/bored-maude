/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import hr.fran.bored.maude.tts.backup.BackupFile;
import hr.fran.bored.maude.tts.backup.BackupManager;
import hr.fran.bored.maude.tts.backup.RestoreManager;
import hr.fran.bored.maude.tts.cache.CleanupManager;
import hr.fran.bored.maude.tts.cache.DownloadManager;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import static org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
class TabletopSimulatorIntegrationTest {

    private final Options testOptions = Options.builder()
            .backupIncompleteMods(true)
            .connectTimeoutInSeconds(5)
            .build();

    private final Collection<ModInfo> modInfos = new LinkedList<>();

    private final Collection<Mod> mods = new LinkedList<>();

    private final Collection<Path> backups = new LinkedList<>();

    private final TabletopSimulator sut = createTabletopSimulator();

    private Collection<ModInfo> getModInfos() {
        return this.modInfos;
    }

    private Collection<Mod> getMods() {
        return this.mods;
    }

    private Collection<Path> getBackups() {
        return this.backups;
    }

    private TabletopSimulator createTabletopSimulator() {
        ModManager modManager = new ModManager();
        ModInfoManager modInfoManager = new ModInfoManager();
        CleanupManager cleanupManager = new CleanupManager(modInfoManager, modManager);
        DownloadManager downloadManager = new DownloadManager(modManager);
        BackupManager backupManager = new BackupManager();
        RestoreManager restoreManager = new RestoreManager(modInfoManager, modManager);

        return new TabletopSimulator(
                modInfoManager,
                modManager,
                cleanupManager,
                downloadManager,
                backupManager,
                restoreManager
        );
    }

    @Test
    @Order(10)
    void getModInfos_should_return_results_when_loading_workshop_mods() {
        Collection<ModInfo> result = this.sut.getModInfos(ModFilter.MATCH_ALL, this.testOptions);

        assertThat(result).isNotEmpty();

        this.modInfos.addAll(result);
    }

    @ParameterizedTest
    @MethodSource("getModInfos")
    @Order(20)
    void getMod_should_not_throw_exception_when_loading_mod(ModInfo modInfo) {
        assertThatNoException().isThrownBy(() -> {
            Mod result = this.sut.getMod(modInfo, this.testOptions);
            this.mods.add(result);
        });
    }

    @Test
    @Order(30)
    void cleanup_should_not_throw_exception_when_finding_orphan_files() {
        assertThatNoException().isThrownBy(
                () -> this.sut.cleanup(this.testOptions)
        );
    }

    @ParameterizedTest
    @MethodSource("getMods")
    @Order(40)
    void downloadAndCreateBackup_should_not_throw_exception_when_backing_up_mods(Mod mod) {
        assertThatNoException().isThrownBy(() -> {
            BackupFile result = this.sut.downloadAndCreateBackup(mod, this.testOptions);
            this.backups.add(result.getFile());
        });
    }

    @ParameterizedTest
    @MethodSource("getBackups")
    @Order(60)
    void restoreBackup_should_overwrite_original_files_when_restoring_existing_mod(Path backupFile) throws IOException {
        long fileCountBefore = getTtsFileCount();
        Mod result = this.sut.restoreBackup(backupFile, this.testOptions);
        long fileCountAfter = getTtsFileCount();

        assertThat(this.modInfos).extracting(ModInfo::getId).contains(result.getModInfo().getId());
        assertThat(fileCountAfter).isEqualTo(fileCountBefore);

        Files.delete(backupFile);
    }

    private long getTtsFileCount() {
        try (Stream<Path> ttsFiles = Files.find(this.testOptions.getTtsDir(), Integer.MAX_VALUE,
                (path, basicFileAttributes) -> basicFileAttributes.isRegularFile())
        ) {
            return ttsFiles.count();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @ParameterizedTest
    @MethodSource("getMods")
    void mod_should_have_no_assets_with_multiple_cache_files(Mod mod) {
        Collection<Asset> assetsWithMultipleFiles = mod.getAssets().stream()
                .filter(this::hasMultipleCacheFiles)
                .collect(Collectors.toUnmodifiableSet());

        assertThat(assetsWithMultipleFiles).isEmpty();
    }

    private boolean hasMultipleCacheFiles(Asset asset) {
        String error = asset.getError();
        return error != null && error.contains("Multiple cache files found");
    }
}
