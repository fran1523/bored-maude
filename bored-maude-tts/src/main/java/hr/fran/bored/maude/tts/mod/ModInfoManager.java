/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import hr.fran.bored.maude.tts.AbstractConfiguration;
import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static hr.fran.bored.maude.tts.mod.ModType.MOD;
import static hr.fran.bored.maude.tts.mod.ModType.SAVE;

public class ModInfoManager {

    private static final Set<String> FILEINFOS_FILENAMES = Set.of("SaveFileInfos.json", "WorkshopFileInfos.json");

    private static final Gson GSON = new GsonBuilder()
            .disableJdkUnsafe()
            .registerTypeAdapterFactory(new GsonAdaptersModInfo())
            .create();

    private static final Logger log = LoggerFactory.getLogger(ModInfoManager.class);

    public ModInfo getModInfo(Path modFile, Options options) {
        try (BufferedReader reader = Files.newBufferedReader(modFile)) {
            ModInfo modInfo = GSON.fromJson(reader, ModInfo.class);

            ModType type = Objects.equals(modFile.getParent(), options.getWorkshopDir())
                    ? MOD
                    : SAVE;

            return modInfo.withFile(modFile).withType(type);
        } catch (IOException | JsonParseException e) {
            throw new TtsException("Cannot read mod JSON '%s'".formatted(modFile), e);
        }
    }

    public Collection<ModInfo> getValidModInfos(ModFilter modFilter, Options options) {
        return getValidModInfos(options).stream()
                .filter(modFilter::matches)
                .toList();
    }

    private Collection<ModInfo> getValidModInfos(Options options) {
        Path modFilesDir = options.getModFilesDir();
        Collection<ModInfo> modInfos = getModFiles(modFilesDir).stream()
                .map(modFile -> getModInfoIfValid(modFile, options))
                .flatMap(Optional::stream)
                .toList();

        log.debug("Loaded {} mods from '{}'", modInfos.size(), modFilesDir);

        return modInfos;
    }

    private Collection<Path> getModFiles(Path modFilesDir) {
        if (!Files.isDirectory(modFilesDir)) {
            return List.of();
        }

        try (Stream<Path> modFiles = Files.find(modFilesDir, 1, (file, basicFileAttributes) ->
                basicFileAttributes.isRegularFile() && isModFile(file)
        )) {
            return modFiles.toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private boolean isModFile(Path file) {
        String fileName = Objects.toString(file.getFileName(), "");
        return FilenameUtils.getExtension(fileName).equalsIgnoreCase(AbstractConfiguration.MOD_FILE_EXTENSION)
                && !FILEINFOS_FILENAMES.contains(fileName);
    }

    private Optional<ModInfo> getModInfoIfValid(Path modFile, Options options) {
        try {
            ModInfo modInfo = getModInfo(modFile, options);
            return Optional.of(modInfo);
        } catch (Exception e) {
            log.warn("Error reading mod from '{}'", modFile, e);
            return Optional.empty();
        }
    }

    public Collection<ModInfo> getWorkshopAndSavesModInfos(Options options) {
        Collection<ModInfo> mods = getValidModInfos(options.withModType(MOD));
        Collection<ModInfo> saves = getValidModInfos(options.withModType(SAVE));
        return Stream.concat(mods.stream(), saves.stream()).toList();
    }

}
