/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModType;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.util.List;

import static hr.fran.bored.maude.tts.mod.ModType.MOD;
import static hr.fran.bored.maude.tts.mod.ModType.SAVE;

abstract class AbstractListCommand extends AbstractCommand {

    @Parameters(
            paramLabel = "mods",
            description = """
                    Filters matching id or name of the mods to list. \
                    Id must match exactly. Name must contain the filter, case insensitive. \
                    If omitted, all mods will be listed""")
    private final List<String> filters = List.of();

    @Option(
            names = {"-s", "--saves"},
            description = "List saves instead of workshop mods"
    )
    private boolean saves;

    protected AbstractListCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    protected Options.Builder getOptionsBuilder() {
        ModType modType = this.saves ? SAVE : MOD;
        return super.getOptionsBuilder().modType(modType);
    }

    protected ModFilter getModFilter() {
        if (this.filters.isEmpty()) {
            return ModFilter.MATCH_ALL;
        }

        return new ModFilter(this.filters);
    }

    protected String getFilterSuffix() {
        if (this.filters.isEmpty()) {
            return "";
        }

        return " matching %s".formatted(this.filters);
    }
}
