/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.cache.DownloadListener;
import hr.fran.bored.maude.tts.mod.Asset;
import org.checkerframework.checker.nullness.qual.Nullable;
import picocli.CommandLine.Option;

abstract class AbstractDownloadCommand extends AbstractListCommand {

    protected final DownloadListener listener = new DownloadPrintListener();

    @Option(
            names = {"-t", "--connect-timeout"},
            description = "HTTP connection timeout in seconds"
    )
    private @Nullable Integer connectTimeoutInSeconds;

    @Option(
            names = {"-o", "--overwrite"},
            description = "Download cached assets again, overwrite existing files"
    )
    private boolean overwrite;

    protected AbstractDownloadCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    protected Options.Builder getOptionsBuilder() {
        Options.Builder builder = super.getOptionsBuilder().overwriteCacheFiles(this.overwrite);
        if (this.connectTimeoutInSeconds != null) {
            builder.connectTimeoutInSeconds(this.connectTimeoutInSeconds);
        }

        return builder;
    }

    private class DownloadPrintListener implements DownloadListener {

        @Override
        public void downloadCompleted(Asset asset) {
            getOut().printf("Downloading %s ... saved as '%s'%n", asset.getUrl(), asset.getCacheFile());
        }

        @Override
        public void downloadFailed(Asset asset) {
            getOut().printf("Downloading %s ... ERROR: %s%n", asset.getUrl(), asset.getError());
        }
    }

}
