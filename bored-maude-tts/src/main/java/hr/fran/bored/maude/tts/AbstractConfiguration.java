/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import hr.fran.bored.maude.tts.mod.AssetType;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.immutables.value.Value.Derived;
import static org.immutables.value.Value.Immutable;
import static org.immutables.value.Value.Lazy;
import static org.immutables.value.Value.Style;

@Immutable(builder = false, singleton = true)
@Style(instance = "instance", typeImmutable = "*")
public class AbstractConfiguration {

    public static final String MOD_FILE_EXTENSION = "json";

    private static final Gson GSON = new GsonBuilder().disableJdkUnsafe().create();

    protected AbstractConfiguration() {
    }

    private String resourceToString(String name) {
        try {
            return IOUtils.resourceToString(name, UTF_8);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Derived
    public boolean isFieldNameBlacklisted(String fieldName) {
        return getAssetBlacklist().contains(fieldName);
    }

    @Lazy
    protected Collection<String> getAssetBlacklist() {
        String blacklistJson = resourceToString("/asset-blacklist.json");
        Set<String> blacklist = GSON.fromJson(blacklistJson, new TypeToken<>() {
        });
        return Collections.unmodifiableSet(blacklist);
    }

    @Derived
    public Optional<AssetType> getAssetType(String fieldName) {
        Map<String, AssetType> fieldNameToAssetType = getFieldNameToAssetTypeMap();
        return Optional.ofNullable(fieldNameToAssetType.get(fieldName));
    }

    @Lazy
    protected Map<String, AssetType> getFieldNameToAssetTypeMap() {
        String assetTypeToFieldNamesJson = resourceToString("/asset-types.json");
        Map<AssetType, List<String>> assetTypeToFieldNames = GSON.fromJson(assetTypeToFieldNamesJson,
                new TypeToken<>() {
                });

        return assetTypeToFieldNames.entrySet().stream()
                .flatMap(this::invert)
                .collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @SuppressWarnings("type.arguments.not.inferred")
    private <K, V> Stream<Map.Entry<V, K>> invert(Map.Entry<K, List<V>> entry) {
        return entry.getValue().stream()
                .filter(Objects::nonNull)
                .map(value -> Map.entry(value, entry.getKey()));
    }

    @Derived
    public Optional<String> getFileExtensionForMimeType(AssetType assetType, String mimeType) {
        Map<String, String> mimeTypeToFileExtension = getAssetTypeToMimeTypeToFileExtensionMap()
                .getOrDefault(assetType, Map.of());
        return Optional.ofNullable(mimeTypeToFileExtension.get(mimeType));
    }

    @Lazy
    // Supported file types according to Tabletop Simulator documentation:
    // * "Either PNG or JPG are acceptable image formats."
    // * "The music player lets you play audio files (.mp3, .wav, .ogg, ogv)"
    //
    // <a href="https://kb.tabletopsimulator.com/custom-content/asset-creation/">Asset Creation</a>
    // <a href="https://steamcommunity.com/games/TabletopSimulator/announcements/detail/3535809451399020344">Update v12.1 Music Player, PDF support</a>
    protected Map<AssetType, Map<String, String>> getAssetTypeToMimeTypeToFileExtensionMap() {
        String assetTypeToMimeTypeToFileExtensionJson = resourceToString("/asset-mime-types.json");
        Map<AssetType, Map<String, String>> assetTypeToMimeTypeToFileExtension =
                GSON.fromJson(assetTypeToMimeTypeToFileExtensionJson, new TypeToken<>() {
                });
        return Collections.unmodifiableMap(assetTypeToMimeTypeToFileExtension);
    }

    @Derived
    public boolean isMimeTypeValidForAssetType(String mimeType, AssetType assetType) {
        return getFileExtensionForMimeType(assetType, mimeType).isPresent();
    }

    @Derived
    public boolean hasTtsFileExtension(String file) {
        String ext = FilenameUtils.getExtension(file);
        return getTtsFileExtensions().contains(ext.toLowerCase(Locale.ROOT));
    }

    @Lazy
    protected Collection<String> getTtsFileExtensions() {
        Stream<String> assetFileExtensions = getAssetTypeToMimeTypeToFileExtensionMap().values().stream()
                .flatMap(map -> map.values().stream())
                .map(ext -> ext.toLowerCase(Locale.ROOT));

        return Stream.concat(
                assetFileExtensions,
                Stream.of(MOD_FILE_EXTENSION)
        ).collect(Collectors.toUnmodifiableSet());
    }

    public int getDownloadThreadPoolSize() {
        String poolSize = getAppProperties().getProperty("download.threadPool.size", "10");
        return Integer.parseInt(poolSize);
    }

    public String getGitlabProjectId() {
        return getAppProperties().getProperty("gitlab.project.id", "");
    }

    public String getGitlabReleasesUrl() {
        return getAppProperties().getProperty("gitlab.releases.url", "");
    }

    @Lazy
    protected Properties getAppProperties() {
        try (InputStream in = getClass().getResourceAsStream("/app.properties")) {
            if (in == null) {
                throw new IllegalStateException("Cannot find resource /app.properties");
            }

            Properties properties = new Properties();
            properties.load(in);
            return properties;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
