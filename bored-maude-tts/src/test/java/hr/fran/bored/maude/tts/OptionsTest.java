/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import com.google.common.jimfs.Jimfs;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import uk.org.webcompere.systemstubs.properties.SystemProperties;

import java.net.http.HttpClient;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.stream.Stream;

import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

class OptionsTest {

    private final Options sut = Options.builder().build();

    @ParameterizedTest
    @MethodSource("getOsNames")
    void getTtsDir_should_return_tts_path(String osName, String parent) throws Exception {
        new SystemProperties("os.name", osName).execute(() -> {
            Options options = Options.builder().build();

            Path result = options.getTtsDir();

            assertThat(result).hasFileName("Tabletop Simulator");
            assertThat(result.getParent()).hasFileName(parent);
        });
    }

    private static Stream<Arguments> getOsNames() {
        return Stream.of(
                Arguments.of("Linux", "share"),
                Arguments.of("Mac OS X", "Library"),
                Arguments.of("Windows 10", "My Games")
        );
    }

    @Test
    void getTtsDir_should_return_snap_path_when_it_exists() throws Exception {
        try (FileSystem testFs = Jimfs.newFileSystem()) {
            Path userHome = testFs.getPath("home", "fran");
            Path snapPath = testFs.getPath("home", "fran", "snap", "steam", "common", ".local", "share",
                    "Tabletop Simulator");
            Files.createDirectories(snapPath);

            new SystemProperties("os.name", "Linux", "user.home", userHome.toString()).execute(() -> {
                Options options = Options.builder().fileSystem(testFs).build();

                Path result = options.getTtsDir();

                assertThat(result).isEqualTo(snapPath);
            });
        }
    }

    @Test
    void getHttpClient_should_return_client_without_timeout_when_connection_timeout_is_not_specified() {
        HttpClient result = this.sut.getHttpClient();

        assertThat(result.connectTimeout()).isEmpty();
    }

    @Test
    void getHttpClient_should_return_client_with_timeout_when_connection_timeout_is_specified() {
        Options options = Options.builder()
                .connectTimeoutInSeconds(5)
                .build();

        HttpClient result = options.getHttpClient();

        assertThat(result.connectTimeout()).contains(Duration.of(5, SECONDS));
    }

    @Test
    void getModFilesDir_should_return_workshop_dir_when_saves_is_false() {
        Path result = this.sut.getModFilesDir();

        assertThat(result).isEqualTo(this.sut.getWorkshopDir());
    }

    @Test
    void getModFilesDir_should_return_saves_dir_when_saves_is_true() {
        Options options = Options.builder()
                .modType(SAVE)
                .build();

        Path result = options.getModFilesDir();

        assertThat(result).isEqualTo(options.getSavesDir());
    }

    @Test
    void getCacheDir_should_return_cache_dir_for_asset_type() {
        Path result = this.sut.getCacheDir(IMAGE);

        assertThat(result).hasFileName(IMAGE.getCacheDir());
    }
}
