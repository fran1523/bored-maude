/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import com.google.common.jimfs.Jimfs;
import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import org.apache.commons.io.file.PathUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import picocli.CommandLine;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestoreCommandTest extends CommandTestBase {

    @Spy
    private final RestoreCommand restoreCommand = new RestoreCommand(tabletopSimulator);

    private FileSystem testFs;

    private CommandLine sut;

    @BeforeEach
    protected void setup() {
        this.testFs = Jimfs.newFileSystem();
        when(this.restoreCommand.getFileSystem()).thenReturn(this.testFs);
        this.sut = new CommandLine(this.restoreCommand);
    }

    @AfterEach
    protected void tearDown() {
        try {
            this.testFs.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"backups/Chess (12345678).zip", "backups/Chess*.zip", "backups/*",
            "back*/Chess (12345678).zip", "*/Chess (12345678).zip", "**/*"})
    void should_restore_backup_when_matching_wildcard_pattern_with_base_dir(String pattern) {
        Path backupFile = givenTestBackupFile("backups", "Chess (12345678).zip");

        when(this.tabletopSimulator.restoreBackup(eq(backupFile), any(Options.class))).thenReturn(CHESS_CACHED);

        this.sut.execute(pattern);

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] restored from '%s'
                
                1/1 mods restored, 0 restores failed
                """.formatted(backupFile)
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {"Chess (12345678).zip", "Chess*.zip", "*"})
    void should_restore_backup_when_matching_wildcard_pattern_without_base_dir(String pattern) {
        Path backupFile = givenTestBackupFile("Chess (12345678).zip");

        when(this.tabletopSimulator.restoreBackup(eq(backupFile), any(Options.class))).thenReturn(CHESS_CACHED);

        this.sut.execute(pattern);

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] restored from '%s'
                
                1/1 mods restored, 0 restores failed
                """.formatted(backupFile)
        );
    }

    @Test
    void should_accept_only_backup_archives_when_matching_wildcard_pattern() {
        givenTestBackupFile("backups", "Not archive.exe");
        Path backupFile = givenTestBackupFile("backups", "Chess (12345678).zip");

        when(this.tabletopSimulator.restoreBackup(eq(backupFile), any(Options.class))).thenReturn(CHESS_CACHED);

        this.sut.execute("backups/*");

        verify(tabletopSimulator).restoreBackup(eq(backupFile), any(Options.class));
        verifyNoMoreInteractions(tabletopSimulator);
    }

    @Test
    void should_print_warning_when_restored_mod_is_incomplete() {
        Path backupFile = givenTestBackupFile("backups", "Chess (12345678).zip");

        when(this.tabletopSimulator.restoreBackup(eq(backupFile), any(Options.class))).thenReturn(CHESS);

        this.sut.execute("backups/Chess (12345678).zip");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] restored from '%s' (incomplete)
                
                1/1 mods restored, 0 restores failed
                """.formatted(backupFile)
        );
    }

    @Test
    void should_print_error_when_backup_restore_fails() {
        Path backupFile = givenTestBackupFile("backups", "Chess (12345678).zip");

        when(this.tabletopSimulator.restoreBackup(eq(backupFile), any(Options.class)))
                .thenThrow(new TtsException("No mods found"));

        this.sut.execute("backups/*.zip");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                '%s' restore failed: No mods found
                
                0/1 mods restored, 1 restores failed
                """.formatted(backupFile)
        );
    }

    @Test
    void should_restore_backups_when_there_are_multiple_backup_files() {
        Path chessFile = givenTestBackupFile("backups", "Chess (12345678).zip");
        Path checkersFile = givenTestBackupFile("backups", "Checkers (23456789).zip");

        when(this.tabletopSimulator.restoreBackup(eq(chessFile), any(Options.class))).thenReturn(CHESS_CACHED);
        when(this.tabletopSimulator.restoreBackup(eq(checkersFile), any(Options.class)))
                .thenThrow(new TtsException("No mods found"));

        this.sut.execute("backups/*.zip");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                '%s' restore failed: No mods found
                Chess [12345678] restored from '%s'
                
                1/2 mods restored, 1 restores failed
                """.formatted(checkersFile, chessFile)
        );
    }

    @Test
    void should_restore_backups_when_given_multiple_file_patterns() {
        Path chessFile = givenTestBackupFile("Backups", "Chess (12345678).zip");
        Path checkersFile = givenTestBackupFile("Backups", "Checkers (23456789).zip");

        when(this.tabletopSimulator.restoreBackup(eq(chessFile), any(Options.class))).thenReturn(CHESS_CACHED);
        when(this.tabletopSimulator.restoreBackup(eq(checkersFile), any(Options.class))).thenReturn(CHECKERS);

        this.sut.execute("Backups/Chess*.zip", "Backups/Checkers*.zip");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Checkers [23456789] restored from '%s'
                Chess [12345678] restored from '%s'
                
                2/2 mods restored, 0 restores failed
                """.formatted(checkersFile, chessFile)
        );
    }

    @Test
    void should_print_error_when_backup_file_is_not_found() {
        this.sut.execute("backup/does-not-exist/*.zip");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Backup file not found
                """
        );
    }

    private Path givenTestBackupFile(String first, String... more) {
        try {
            Path backupFile = this.testFs.getPath(first, more);
            return PathUtils.touch(backupFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
