/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.update;

import com.google.gson.JsonSyntaxException;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import picocli.CommandLine.IVersionProvider;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VersionUpdateCheckerTest {

    private final IVersionProvider versionProvider = mock(IVersionProvider.class);

    private final HttpClient httpClient = mock(HttpClient.class);

    private final VersionUpdateChecker sut = new VersionUpdateChecker(this.versionProvider, this.httpClient);

    @BeforeEach
    void before() throws Exception {
        when(this.versionProvider.getVersion()).thenReturn(new String[]{"0.1.0"});
    }

    @Test
    void should_return_result_when_there_is_a_newer_version() {
        whenHttpClientSendThenReturn(getReleasesJson("v0.0.1", "v1.0.0", "v1.0.1"));

        Optional<VersionUpdate> result = assertDoesNotThrow(this.sut::checkForUpdate);

        assertThat(result).isNotEmpty();
        assertThat(result.get().getVersion()).isEqualTo("v1.0.1");
    }

    @ParameterizedTest
    @ValueSource(strings = {"v0.0.1", "v0.1.0"})
    void should_return_no_result_when_there_are_no_updates(String tagName) {
        whenHttpClientSendThenReturn(getReleasesJson(tagName));

        Optional<VersionUpdate> result = assertDoesNotThrow(this.sut::checkForUpdate);

        assertThat(result).isEmpty();
    }

    @Test
    void should_throw_exception_when_update_check_returns_no_releases() {
        whenHttpClientSendThenReturn(getReleasesJson());

        assertThatThrownBy(this.sut::checkForUpdate).isInstanceOf(IllegalStateException.class)
                .hasMessage("No releases returned by GitLab API");
    }

    @Test
    void should_throw_exception_when_update_check_returns_invalid_json() {
        whenHttpClientSendThenReturn("invalid response");

        assertThatThrownBy(this.sut::checkForUpdate).isInstanceOf(JsonSyntaxException.class);
    }

    @Test
    void should_throw_exception_when_update_check_returns_bad_http_status() {
        whenHttpClientSendThenReturn(HTTP_NOT_FOUND);

        assertThatThrownBy(this.sut::checkForUpdate).isInstanceOf(UncheckedIOException.class)
                .hasRootCauseMessage("HTTP 404 error");
    }

    @Test
    void should_throw_exception_when_update_check_returns_error() throws InterruptedException, IOException {
        when(this.httpClient.send(any(), any())).thenThrow(new IOException("Connection error"));

        assertThatThrownBy(this.sut::checkForUpdate).isInstanceOf(UncheckedIOException.class)
                .hasRootCauseMessage("Connection error");
    }

    private void whenHttpClientSendThenReturn(String response) {
        whenHttpClientSendThenReturn(HTTP_OK, response);
    }

    private void whenHttpClientSendThenReturn(int statusCode) {
        whenHttpClientSendThenReturn(statusCode, null);
    }

    private void whenHttpClientSendThenReturn(int statusCode, @Nullable String response) {
        HttpResponse<?> mockResponse = getMockResponse(statusCode, response);

        try {
            doReturn(mockResponse).when(this.httpClient).send(any(), any());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private HttpResponse<?> getMockResponse(int statusCode, @Nullable String response) {
        HttpResponse<?> mockResponse = mock(HttpResponse.class);
        when(mockResponse.statusCode()).thenReturn(statusCode);

        if (response != null) {
            doReturn(new ByteArrayInputStream(response.getBytes())).when(mockResponse).body();
        }

        return mockResponse;
    }

    private String getReleasesJson(String... tagNames) {
        return Stream.of(tagNames)
                .map("{\"tag_name\": \"%s\"}"::formatted)
                .collect(Collectors.joining(",", "[", "]"));
    }

}
