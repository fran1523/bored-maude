/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.cache;

import hr.fran.bored.maude.tts.UnitTestBase;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.nio.file.Path;
import java.time.Instant;

import static hr.fran.bored.maude.tts.mod.AssetType.PDF;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

@ExtendWith(MockitoExtension.class)
class DownloadManagerTest extends UnitTestBase {

    @Mock
    private DownloadListener listener;

    @Mock
    private ModManager modManager;

    @Captor
    ArgumentCaptor<HttpRequest> httpRequestCaptor;

    private DownloadManager sut;

    @BeforeEach
    @Override
    protected void setup() {
        super.setup();
        this.sut = new DownloadManager(this.modManager);
    }

    @Test
    void downloadAssets_should_download_uncached_assets_when_there_are_no_errors() {
        Asset image1cached = cached(IMAGE1);
        Mod mod = CHESS_MOD.withAssets(image1cached, PDF1);

        whenSendAssetUrlThenReturnFile(PDF1);

        Mod result = this.sut.downloadAssets(mod, testOptions);

        Path pdf1CacheFile = getExpectedCacheFile(PDF, PDF1_CACHE_FILENAME);
        assertThat(result).isEqualTo(CHESS_MOD.withAssets(
                image1cached,
                PDF1.withCacheFile(pdf1CacheFile)
        ));
        assertThat(pdf1CacheFile).isNotEmptyFile();
        assertThat(getTtsFiles()).containsExactly(image1cached.getCacheFile(), pdf1CacheFile);
    }

    @Test
    void downloadAssets_should_set_user_agent_for_http_requests() throws IOException, InterruptedException {
        Mod mod = CHESS_MOD.withAssets(PDF1);

        this.sut.downloadAssets(mod, testOptions);

        verify(mockHttpClient).send(this.httpRequestCaptor.capture(), any());
        assertThat(this.httpRequestCaptor.getValue().headers().map())
                .extractingByKey("User-Agent").asInstanceOf(InstanceOfAssertFactories.LIST)
                .singleElement().asInstanceOf(InstanceOfAssertFactories.STRING).isNotBlank();
    }

    @Test
    void downloadAssets_should_download_uncached_assets_when_some_downloads_fail() {
        whenSendUrlThenThrow(IMAGE1.getUrl(), new IOException("Network error"));
        whenSendAssetUrlThenReturnFile(PDF1);

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(IMAGE1, PDF1), testOptions);

        Path pdf1CacheFile = getExpectedCacheFile(PDF, PDF1_CACHE_FILENAME);
        assertThat(result.getAssets()).containsExactlyInAnyOrder(
                IMAGE1.withError("Network error"),
                PDF1.withCacheFile(pdf1CacheFile)
        );
        assertThat(getTtsFiles()).singleElement().isEqualTo(pdf1CacheFile);
    }

    @Test
    void downloadAssets_should_overwrite_cached_assets_when_overwriteCacheFiles_is_enabled() {
        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Instant oneMinuteAgo = setLastModifiedTimeOneMinuteAgo(image1CacheFile);
        Asset image1Cached = IMAGE1.withCacheFile(image1CacheFile);

        whenSendAssetUrlThenReturnFile(IMAGE1);

        this.sut.downloadAssets(CHESS_MOD.withAssets(image1Cached), this.testOptions.withOverwriteCacheFiles(true));

        assertThat(getLastModifiedTime(image1CacheFile)).isAfter(oneMinuteAgo);
    }

    @Test
    void downloadAssets_should_not_overwrite_cached_assets_when_overwriteCacheFiles_is_disabled() {
        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Instant oneMinuteAgo = setLastModifiedTimeOneMinuteAgo(image1CacheFile);
        Asset image1Cached = IMAGE1.withCacheFile(image1CacheFile);

        whenSendAssetUrlThenReturnFile(IMAGE1);

        this.sut.downloadAssets(CHESS_MOD.withAssets(image1Cached), testOptions);

        assertThat(getLastModifiedTime(image1CacheFile)).isEqualTo(oneMinuteAgo);
    }

    @Test
    void downloadAssets_should_not_remove_cache_file_when_overwriteCacheFiles_is_enabled_and_download_fails() {
        Asset image1Cached = cached(IMAGE1);

        whenSendUrlThenReturn404NotFound(IMAGE1.getUrl());

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(image1Cached),
                this.testOptions.withOverwriteCacheFiles(true));

        assertThat(result.getAssets()).singleElement()
                .isEqualTo(image1Cached.withError("HTTP 404 error"));
        assertThat(image1Cached.getCacheFile()).exists();
    }

    @Test
    void downloadAssets_should_add_error_when_http_status_is_not_ok() {
        whenSendUrlThenReturn404NotFound(IMAGE1.getUrl());

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(IMAGE1), testOptions);

        assertThat(result.getAssets()).singleElement()
                .isEqualTo(IMAGE1.withError("HTTP 404 error"));
    }

    @Test
    void downloadAssets_should_add_error_when_url_is_invalid() {
        Asset image1Asset = IMAGE1.withJsonUrl("bad url");

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(image1Asset), testOptions);

        assertThat(result.getAssets()).singleElement()
                .isEqualTo(image1Asset.withError("Illegal character in path at index 3: bad url"));
    }

    @Test
    void downloadAssets_should_add_error_when_file_content_type_is_not_valid_for_asset_type() {
        whenSendAssetUrlThenReturnFile(PDF1, "IMAGE.jpg");

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(PDF1), testOptions);

        assertThat(result.getAssets()).singleElement()
                .isEqualTo(PDF1.withError("image/jpeg is not a valid content type for PDF"));
    }

    @Test
    void downloadAssets_should_add_error_when_file_is_empty() {
        whenSendAssetUrlThenReturnFile(ASSETBUNDLE1, "empty-file");

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(ASSETBUNDLE1), testOptions);

        assertThat(result.getAssets()).singleElement()
                .isEqualTo(ASSETBUNDLE1.withError("File is empty"));
    }

    @Test
    void downloadAssets_should_trigger_download_listener_when_asset_download_completes() {
        whenSendAssetUrlThenReturnFile(PDF1);

        this.sut.addDownloadListener(this.listener);
        this.sut.downloadAssets(CHESS_MOD.withAssets(PDF1), testOptions);

        Path pdf1CacheFile = getExpectedCacheFile(PDF, PDF1_CACHE_FILENAME);
        verify(this.listener).downloadCompleted(PDF1.withCacheFile(pdf1CacheFile));
    }

    @Test
    void downloadAssets_should_trigger_download_listener_when_asset_download_fails() {
        whenSendUrlThenThrow(IMAGE1.getUrl(), new IOException("Network error"));

        this.sut.addDownloadListener(this.listener);
        this.sut.downloadAssets(CHESS_MOD.withAssets(IMAGE1), testOptions);

        verify(this.listener).downloadFailed(IMAGE1.withError("Network error"));
    }

    @Test
    void downloadAssets_should_not_trigger_removed_download_listener() {
        whenSendAssetUrlThenReturnFile(IMAGE1);

        this.sut.addDownloadListener(this.listener);
        this.sut.removeDownloadListener(this.listener);
        this.sut.downloadAssets(CHESS_MOD.withAssets(IMAGE1), testOptions);

        verifyNoInteractions(this.listener);
    }

}
