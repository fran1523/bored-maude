/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.cache.Cleanup;
import org.apache.commons.io.FileUtils;

import java.nio.file.Path;
import java.util.Map;

import static picocli.CommandLine.Command;

@Command(name = "cleanup",
        description = "Deletes orphan assets not used by any installed mod from Tabletop Simulator cache",
        mixinStandardHelpOptions = true)
public class CleanupCommand extends AbstractCommand {

    public CleanupCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    public void run() {
        Options options = getOptions();
        Cleanup cleanup = this.tabletopSimulator.cleanup(options);

        printRenamedFiles(cleanup.renamedFiles());
        getOut().println();
        printDeletedFiles(cleanup.deletedFiles());

        printSummary(cleanup.renamedFiles().size(), cleanup.deletedFiles());
    }

    private void printRenamedFiles(Map<Path, Path> renamedFiles) {
        if (renamedFiles.isEmpty()) {
            getOut().println("No files renamed");
            return;
        }

        printNamedValue("Files renamed", 1, renamedFiles.size());
        renamedFiles.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(
                renamedFile -> getOut().printf("%s%s -> %s%n", INDENT, renamedFile.getKey(), renamedFile.getValue())
        );
    }

    private void printDeletedFiles(Map<Path, Long> deletedFiles) {
        if (deletedFiles.isEmpty()) {
            getOut().println("No orphan files found");
            return;
        }

        printNamedValue("Orphans found", 1, deletedFiles.size());
        deletedFiles.keySet().stream().sorted().forEach(orphan -> getOut().printf("%s%s%n", INDENT, orphan));
    }

    private void printSummary(int renamedCount, Map<Path, Long> deletedFiles) {
        if (renamedCount == 0 && deletedFiles.isEmpty()) {
            return;
        }

        long totalSize = getTotalSize(deletedFiles);
        getOut().println();
        getOut().printf("%s files renamed. %s orphans deleted, %s total%n",
                renamedCount, deletedFiles.size(), FileUtils.byteCountToDisplaySize(totalSize));
    }

    private long getTotalSize(Map<Path, Long> orphansToSizes) {
        return orphansToSizes.values().stream()
                .mapToLong(size -> size)
                .sum();
    }

}
