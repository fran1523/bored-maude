plugins {
    id("jacoco")
    id("java")
}

dependencies {
    annotationProcessor(libs.immutables.value)
    annotationProcessor(libs.picocli.codegen)
    compileOnly(libs.assertj.core)
    compileOnly(libs.bundles.immutables)
    implementation(project(":bored-maude-tts"))
    implementation(libs.commons.io)
    implementation(libs.gson)
    implementation(libs.picocli)
    implementation(libs.semver4j)
    implementation(libs.slf4j.api)
    runtimeOnly(libs.bundles.tinylog)

    testImplementation(libs.bundles.system.stubs)
    testImplementation(libs.bundles.test)
    testImplementation(libs.immutables.value)
    testImplementation(libs.jimfs)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

checkerFramework {
    extraJavacArgs = listOf("-AsuppressWarnings=initialization.fields.uninitialized")
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
    // disable SLF4J initialisation logs
    jvmArgs("-Dslf4j.internal.verbosity=ERROR")
}

tasks.jacocoTestReport {
    reports.xml.required = true
    dependsOn(tasks.test)
}
