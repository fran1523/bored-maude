/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import hr.fran.bored.maude.tts.common.ZipUtils;
import org.apache.commons.io.FilenameUtils;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.within;

public final class ZipAssert extends AbstractAssert<ZipAssert, Collection<ZipEntry>> {

    private final Path zipFile;

    private ZipAssert(Collection<ZipEntry> actual, Path zipFile) {
        super(actual, ZipAssert.class);
        this.zipFile = zipFile;
    }

    public static ZipAssert assertThatZip(Path zipFile) {
        Collection<ZipEntry> actual = ZipUtils.applyToZipEntries(zipFile,
                (zipEntry, zipInputStream) -> Optional.of(zipEntry)
        ).toList();
        return new ZipAssert(actual, zipFile);
    }

    public ZipAssert isEqualTo(Path path) {
        Assertions.assertThat(this.zipFile).isEqualTo(path);
        return this;
    }

    public ZipAssert containsOnlyFiles(Path... files) {
        Assertions.assertThat(actual).allSatisfy(entry -> assertThatFilesContainZipEntry(entry, files));
        Assertions.assertThat(files).allSatisfy(this::assertThatZipEntriesContainFile);
        return this;
    }

    private void assertThatFilesContainZipEntry(ZipEntry entry, Path... files) {
        Assertions.assertThat(files).anySatisfy(file -> assertThatZipEntryMatchesFile(entry, file));
    }

    private void assertThatZipEntriesContainFile(Path file) {
        String normalisedFile = FilenameUtils.separatorsToUnix(file.toString());
        Assertions.assertThat(actual)
                .filteredOn(entry -> normalisedFile.endsWith(entry.getName()))
                .hasSize(1)
                .allSatisfy(entry -> assertThatZipEntryMatchesFile(entry, file));
    }

    private void assertThatZipEntryMatchesFile(ZipEntry zipEntry, Path file) {
        try {
            String normalisedFile = FilenameUtils.separatorsToUnix(file.toString());
            Assertions.assertThat(normalisedFile).endsWith(zipEntry.getName());
            Assertions.assertThat(zipEntry.getSize()).isEqualTo(Files.size(file));
            Assertions.assertThat(zipEntry.getCrc()).isEqualTo(getCrc(file));
            Assertions.assertThat(zipEntry.getLastModifiedTime().toInstant())
                    .isCloseTo(Files.getLastModifiedTime(file).toInstant(), within(1, SECONDS));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private long getCrc(Path file) {
        try (CheckedOutputStream checkOut = new CheckedOutputStream(OutputStream.nullOutputStream(), new CRC32())) {
            Files.copy(file, checkOut);
            return checkOut.getChecksum().getValue();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void wasExtractedTo(Path dir) {
        Assertions.assertThat(actual).allSatisfy(entry -> assertZipEntryWasExtracted(entry, dir));
    }

    private void assertZipEntryWasExtracted(ZipEntry zipEntry, Path dir) {
        Path file = dir.resolve(zipEntry.getName());
        Assertions.assertThat(file).isNotEmptyFile();
        assertThatZipEntryMatchesFile(zipEntry, file);
    }

}