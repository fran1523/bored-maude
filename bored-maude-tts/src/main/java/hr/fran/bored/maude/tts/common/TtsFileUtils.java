/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.common;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.file.PathUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public final class TtsFileUtils {

    private static final Logger log = LoggerFactory.getLogger(TtsFileUtils.class);

    private TtsFileUtils() {
    }

    public static String getBaseName(Path file) {
        return FilenameUtils.getBaseName(file.toString());
    }

    public static String getCacheFileBaseName(String url) {
        return url.replaceAll("[^a-zA-Z0-9]", "");
    }

    public static Path createTempFile(Path dir) {
        try {
            Files.createDirectories(dir);
            @SuppressWarnings("argument")
            Path tempFile = Files.createTempFile(dir, null, null);
            return tempFile;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void move(Path source, Path target) {
        try {
            PathUtils.createParentDirectories(target);
            Files.move(source, target, REPLACE_EXISTING);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void deleteIfExists(@Nullable Path file) {
        try {
            if (file != null) {
                Files.deleteIfExists(file);
            }
        } catch (IOException e) {
            log.warn("Failed to delete file {}", file, e);
        }
    }
}
