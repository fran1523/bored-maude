/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.UnitTestBase;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.Collection;

import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ModInfoManagerTest extends UnitTestBase {

    private final ModInfoManager sut = new ModInfoManager();

    @Test
    void getModInfo_should_return_result_when_input_json_is_valid() {
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);

        ModInfo result = this.sut.getModInfo(modFile, testOptions);

        assertThat(result).isEqualTo(CHESS_MOD_INFO.withFile(modFile));
    }

    @Test
    void getModInfo_should_throw_exception_when_required_json_properties_are_missing() {
        Path modFile = givenWorkshopFile("12345678-missing-properties.json");

        assertThatThrownBy(() -> this.sut.getModInfo(modFile, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("Cannot read mod JSON '%s'", modFile);
    }

    @Test
    void getValidModInfos_should_return_results_when_there_are_mods_matching_mod_filter() {
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);
        givenWorkshopFile("23456789.json");

        ModFilter filter = new ModFilter(CHESS_MOD_INFO.getId());
        Collection<ModInfo> result = this.sut.getValidModInfos(filter, testOptions);

        assertThat(result).singleElement().isEqualTo(CHESS_MOD_INFO.withFile(modFile));
    }

    @Test
    void getValidModInfos_should_return_no_results_when_no_mods_match_mod_filter() {
        givenWorkshopFile(CHESS_MOD_FILE);

        ModFilter filter = new ModFilter("invalid-name");
        Collection<ModInfo> result = this.sut.getValidModInfos(filter, testOptions);

        assertThat(result).isEmpty();
    }

    @Test
    void getValidModInfos_should_ignore_WorkshopFileInfos_and_unsupported_file_types() {
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);
        givenWorkshopFile(CHESS_THUMBNAIL);
        givenWorkshopFile("WorkshopFileInfos.json");

        Collection<ModInfo> result = this.sut.getValidModInfos(ModFilter.MATCH_ALL, testOptions);

        assertThat(result).singleElement().isEqualTo(CHESS_MOD_INFO.withFile(modFile));
    }

    @Test
    void getValidModInfos_should_ignore_SaveFileInfos() {
        Path modFile = givenSavesFile(CHESS_SAVE_FILE);
        givenSavesFile("SaveFileInfos.json");

        Collection<ModInfo> result = this.sut.getValidModInfos(ModFilter.MATCH_ALL, this.testOptions.withModType(SAVE));

        assertThat(result).singleElement().isEqualTo(CHESS_SAVE_INFO.withFile(modFile));
    }

    @Test
    void getValidModInfos_should_skip_invalid_files() {
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);
        givenWorkshopFile("invalid.json");

        Collection<ModInfo> result = this.sut.getValidModInfos(ModFilter.MATCH_ALL, testOptions);

        assertThat(result).singleElement().isEqualTo(CHESS_MOD_INFO.withFile(modFile));
    }

    @Test
    void getWorkshopAndSavesModInfos_should_return_all_mods() {
        Path workshopFile = givenWorkshopFile(CHESS_MOD_FILE);
        Path saveFile = givenSavesFile(CHESS_SAVE_FILE);

        Collection<ModInfo> result = this.sut.getWorkshopAndSavesModInfos(testOptions);

        assertThat(result).containsExactly(CHESS_MOD_INFO.withFile(workshopFile), CHESS_SAVE_INFO.withFile(saveFile));
    }

}
