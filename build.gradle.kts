plugins {
    id("application")
    id("checkstyle")
    alias(libs.plugins.axion.release)
    alias(libs.plugins.ben.manes.versions)
    alias(libs.plugins.checkerframework.plugin)
    alias(libs.plugins.dependency.analysis)
    alias(libs.plugins.jacocolog)
    alias(libs.plugins.licensee)
    alias(libs.plugins.owasp.dependencycheck)
    alias(libs.plugins.sonarqube)
    alias(libs.plugins.version.catalog.update)
}

allprojects {
    group = "hr.fran"
    version = rootProject.scmVersion.version

    repositories {
        mavenCentral()
    }

    apply(plugin = "checkstyle")
    checkstyle {
        toolVersion = rootProject.libs.versions.checkstyle.get()
        maxWarnings = 0
    }

    apply(plugin = "org.checkerframework")
    checkerFramework {
        checkers = listOf("org.checkerframework.checker.nullness.NullnessChecker")
        excludeTests = true
    }

    // Workaround for Checkstyle google-collections dependency conflict
    // https://github.com/checkstyle/checkstyle/issues/14211#issuecomment-1905356177
    configurations.checkstyle {
        resolutionStrategy.capabilitiesResolution.withCapability("com.google.collections:google-collections") {
            select("com.google.guava:guava:0")
        }
    }
}

dependencies {
    runtimeOnly(project(":bored-maude-cli"))
}

application {
    executableDir = ""
    mainClass = "hr.fran.bored.maude.cli.BoredMaude"
    // disable SLF4J initialisation logs
    applicationDefaultJvmArgs = listOf("-Dslf4j.internal.verbosity=ERROR")
}

licensee {
    allow("Apache-2.0")
    allow("MIT")
}

sonar {
    properties {
        property("sonar.projectKey", "fran1523_bored-maude")
        property("sonar.organization", "fran1523")
    }
}

tasks.sonar {
    dependsOn("check", ":bored-maude-cli:check", ":bored-maude-tts:check")
}

versionCatalogUpdate {
    keep {
        versions = listOf("checkstyle")
    }
}
