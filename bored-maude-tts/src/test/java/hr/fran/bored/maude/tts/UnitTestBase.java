/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import com.google.common.jimfs.Jimfs;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.AssetType;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModInfo;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.file.PathUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static hr.fran.bored.maude.tts.mod.AssetType.ASSETBUNDLE;
import static hr.fran.bored.maude.tts.mod.AssetType.AUDIO;
import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static hr.fran.bored.maude.tts.mod.AssetType.MODEL;
import static hr.fran.bored.maude.tts.mod.AssetType.PDF;
import static hr.fran.bored.maude.tts.mod.ModType.MOD;
import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UnitTestBase {

    protected static final String CHESS_MOD_FILE = "12345678.json";
    protected static final String CHESS_THUMBNAIL = "12345678.png";
    protected static final String CHESS_SAVE_FILE = "TS_Save_1.json";
    protected static final String CHESS_BACKUP_FILE = "Chess (12345678).zip";

    protected static final ModInfo CHESS_MOD_INFO = new ModInfo.Builder()
            .date("1/31/2020 1:00:00 AM")
            .name("Chess")
            .version("v12.3.4")
            .file(Path.of("Tabletop Simulator", "Mods", "Workshop", CHESS_MOD_FILE))
            .type(MOD)
            .build();

    protected static final ModInfo CHESS_SAVE_INFO = new ModInfo.Builder()
            .date("2/28/2020 1:00:00 AM")
            .name("Chess")
            .version("v12.3.4")
            .file(Path.of("Tabletop Simulator", "Saves", CHESS_SAVE_FILE))
            .type(SAVE)
            .build();

    protected static final Asset ASSETBUNDLE1 = givenAsset(ASSETBUNDLE, "http://www.test.com/assetbundle/1");
    protected static final Asset AUDIO1 = givenAsset(AUDIO, "http://www.test.com/audio/1");
    protected static final Asset IMAGE1 = givenAsset(IMAGE, "http://www.test.com/image/1");
    protected static final Asset IMAGE2 = givenAsset(IMAGE, "http://www.test.com/image/2");
    protected static final Asset MODEL1 = givenAsset(MODEL, "http://www.test.com/model/1");
    protected static final Asset PDF1 = givenAsset(PDF, "https://www.test.com/pdf/1");

    protected static final String OLD_STEAM_CLOUD_URL =
            "http://cloud-3.steamusercontent.com/ugc/4502007633708726063/A1B6398B2F72BA68F7FDE7D0F1A7D24D5714282B/";
    protected static final Asset OLD_STEAM_CLOUD_ASSET = givenAsset(IMAGE, OLD_STEAM_CLOUD_URL);
    protected static final String OLD_STEAM_CLOUD_CACHE_FILENAME =
            "httpcloud3steamusercontentcomugc4502007633708726063A1B6398B2F72BA68F7FDE7D0F1A7D24D5714282B.jpg";
    protected static final String NEW_STEAM_CLOUD_URL =
            "https://steamusercontent-a.akamaihd.net/ugc/4502007633708726063/A1B6398B2F72BA68F7FDE7D0F1A7D24D5714282B/";
    protected static final Asset NEW_STEAM_CLOUD_ASSET = givenAsset(IMAGE, NEW_STEAM_CLOUD_URL);
    protected static final String NEW_STEAM_CLOUD_CACHE_FILENAME =
            "httpssteamusercontentaakamaihdnetugc4502007633708726063A1B6398B2F72BA68F7FDE7D0F1A7D24D5714282B.jpg";

    protected static final String IMAGE1_CACHE_FILENAME = "httpwwwtestcomimage1.jpg";
    protected static final String IMAGE1_SHA1 = "b1351a99d7a34f3cf6568799dcf0ef8a421b9973";
    protected static final String PDF1_CACHE_FILENAME = "httpswwwtestcompdf1.PDF";
    protected static final String STEAM_CLOUD_SHA1 = "a1b6398b2f72ba68f7fde7d0f1a7d24d5714282b";

    protected static final Mod CHESS_MOD = new Mod.Builder()
            .modInfo(CHESS_MOD_INFO)
            .assets(Set.of(ASSETBUNDLE1, AUDIO1, IMAGE1, MODEL1, PDF1))
            .build();

    protected static final Mod CHESS_SAVE = new Mod.Builder()
            .modInfo(CHESS_SAVE_INFO)
            .assets(Set.of(ASSETBUNDLE1, AUDIO1, IMAGE1, MODEL1, PDF1))
            .build();

    protected final HttpClient mockHttpClient = mock(HttpClient.class);

    private FileSystem testFs;

    protected Options testOptions;

    protected UnitTestBase() {
    }

    @BeforeEach
    protected void setup() {
        try {
            this.testFs = Jimfs.newFileSystem();
            Path backupDir = this.testFs.getPath("backup");
            Path ttsDir = Files.createDirectories(
                    this.testFs.getPath("home", "fran", ".local", "share", "Tabletop Simulator")
            );

            this.testOptions = Options.builder()
                    .backupDir(backupDir)
                    .httpClient(this.mockHttpClient)
                    .ttsDir(ttsDir)
                    .build();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @AfterEach
    protected void tearDown() {
        try {
            this.testFs.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected Path givenWorkshopFile(String filename) {
        String sourceResource = "/mods/%s".formatted(filename);
        Path targetFile = this.testOptions.getWorkshopDir().resolve(filename);
        return copyResourceToFile(sourceResource, targetFile);
    }

    protected Path givenSavesFile(String filename) {
        String sourceResource = "/mods/%s".formatted(filename);
        Path targetFile = this.testOptions.getSavesDir().resolve(filename);
        return copyResourceToFile(sourceResource, targetFile);
    }

    protected ModInfo givenModInfoWithFile(ModInfo modInfo) {
        String filename = modInfo.getFile().getFileName().toString();
        Path modFile = modInfo.getType() == MOD
                ? givenWorkshopFile(filename)
                : givenSavesFile(filename);
        return modInfo.withFile(modFile);
    }

    protected Mod givenModWithFileAndAssets(Mod mod, Asset... assets) {
        ModInfo modInfoWithFile = givenModInfoWithFile(mod.getModInfo());
        return mod.withModInfo(modInfoWithFile).withAssets(assets);
    }

    protected Path givenCacheFileForAsset(Asset asset) {
        String sourceResource = getCacheResource(asset.getType());
        return givenCacheFileForAsset(asset, sourceResource);
    }

    protected Path givenCacheFileForAsset(Asset asset, String sourceResource) {
        String targetFilename = "%s.%s".formatted(
                TtsFileUtils.getCacheFileBaseName(asset.getUrl()), FilenameUtils.getExtension(sourceResource));
        Path targetFile = this.testOptions.getCacheDir(asset.getType()).resolve(targetFilename);

        return copyCacheResourceToFile(sourceResource, targetFile);
    }

    protected Path givenCacheFile(AssetType type, String filename) {
        String sourceResource = getCacheResource(type);
        Path targetFile = this.testOptions.getCacheDir(type).resolve(filename);
        return copyCacheResourceToFile(sourceResource, targetFile);
    }

    protected Asset cached(Asset asset) {
        Path cacheFile = givenCacheFileForAsset(asset);
        return asset.withCacheFile(cacheFile);
    }

    private String getCacheResource(AssetType type) {
        return switch (type) {
            case ASSETBUNDLE -> "ASSETBUNDLE.unity3d";
            case AUDIO -> "AUDIO.mp3";
            case IMAGE -> "IMAGE.jpg";
            case MODEL -> "MODEL.obj";
            case PDF -> "PDF.PDF";
        };
    }

    private Path copyResourceToFile(String sourceResource, Path targetFile) {
        try (InputStream in = this.getClass().getResourceAsStream(sourceResource)) {
            Objects.requireNonNull(in, "Cannot read resource '%s'".formatted(sourceResource));
            PathUtils.createParentDirectories(targetFile);
            Files.copy(in, targetFile, REPLACE_EXISTING);
            return targetFile;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Path copyCacheResourceToFile(String sourceResource, Path targetFile) {
        return copyResourceToFile("/cache/%s".formatted(sourceResource), targetFile);
    }

    protected void whenSendAssetUrlThenReturnFile(Asset asset) {
        String sourceResource = getCacheResource(asset.getType());
        whenSendAssetUrlThenReturnFile(asset, sourceResource);
    }

    protected void whenSendAssetUrlThenReturnFile(Asset asset, String sourceResource) {
        try {
            doAnswer(invocation -> {
                HttpResponse<?> response = mock(HttpResponse.class);
                Path responseFile = getBodyHandlerFile(invocation.getArgument(1));
                copyCacheResourceToFile(sourceResource, responseFile);
                doReturn(responseFile).when(response).body();
                when(response.statusCode()).thenReturn(HTTP_OK);
                return response;
            }).when(this.mockHttpClient).send(
                    argThat(request -> request.uri().toString().equals(asset.getUrl())), any()
            );
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Path getBodyHandlerFile(BodyHandler<Path> bodyHandler) {
        try {
            Field fileField = bodyHandler.getClass().getDeclaredField("file");
            fileField.setAccessible(true);
            return (Path) fileField.get(bodyHandler);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    protected void whenSendUrlThenThrow(String url, Exception ex) {
        try {
            when(this.mockHttpClient.send(
                    argThat(request -> request.uri().toString().equals(url)), any()
            )).thenThrow(ex);
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected void whenSendUrlThenReturn404NotFound(String url) {
        HttpResponse<?> response = mock(HttpResponse.class);
        when(response.statusCode()).thenReturn(HTTP_NOT_FOUND);
        try {
            doReturn(response).when(this.mockHttpClient)
                    .send(argThat(request -> request.uri().toString().equals(url)), any());
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected Path getExpectedCacheFile(AssetType type, String filename) {
        return this.testOptions.getCacheDir(type).resolve(filename);
    }

    protected Collection<Path> getTtsFiles() {
        if (!Files.isDirectory(this.testOptions.getTtsDir())) {
            return List.of();
        }

        try (Stream<Path> ttsFiles = Files.find(this.testOptions.getTtsDir(), 3,
                (path, basicFileAttributes) -> basicFileAttributes.isRegularFile()
        )) {
            return ttsFiles.toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected Path givenBackupFile(String filename) {
        String sourceResource = "/backup/%s".formatted(filename);
        Path targetFile = this.testOptions.getBackupDir().resolve(filename);
        return copyResourceToFile(sourceResource, targetFile);
    }

    protected Path getBackupFile(String filename) {
        String sourceResource = "/backup/%s".formatted(filename);
        URL url = this.getClass().getResource(sourceResource);
        Objects.requireNonNull(url);
        try {
            return Path.of(url.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    protected Instant setLastModifiedTimeOneMinuteAgo(Path file) {
        Instant oneMinuteAgo = Instant.now().minusSeconds(60);
        try {
            Files.setLastModifiedTime(file, FileTime.from(oneMinuteAgo));
            return oneMinuteAgo;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected Instant getLastModifiedTime(Path file) {
        try {
            return Files.getLastModifiedTime(file).toInstant();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static Asset givenAsset(AssetType type, String url) {
        return new Asset.Builder().type(type).jsonUrl(url).build();
    }

}
