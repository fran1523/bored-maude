/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.AssetType;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModInfo;
import hr.fran.bored.maude.tts.mod.ModType;
import org.junit.jupiter.api.extension.ExtendWith;
import picocli.CommandLine;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;
import uk.org.webcompere.systemstubs.stream.SystemErrAndOut;
import uk.org.webcompere.systemstubs.stream.output.MultiplexOutput;
import uk.org.webcompere.systemstubs.stream.output.Output;
import uk.org.webcompere.systemstubs.stream.output.TapStream;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static hr.fran.bored.maude.tts.mod.AssetType.ASSETBUNDLE;
import static hr.fran.bored.maude.tts.mod.AssetType.AUDIO;
import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static hr.fran.bored.maude.tts.mod.AssetType.PDF;
import static hr.fran.bored.maude.tts.mod.ModType.MOD;
import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static org.mockito.Mockito.mock;

@ExtendWith(SystemStubsExtension.class)
class CommandTestBase {

    private static final Options OPTIONS = Options.builder().build();

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("M/d/yyyy K:mm:ss a");
    private static final LocalDateTime DEFAULT_DATE = LocalDateTime.of(2020, 12, 31, 1, 2, 3);
    private static final long YEAR_IN_SECONDS = 40_000_000;

    protected static final ModInfo CHESS_INFO = modInfoOf("Chess", "12345678", MOD);
    protected static final ModInfo CHECKERS_INFO = modInfoOf("Checkers", "23456789", MOD);
    protected static final ModInfo CHESS_SAVE_INFO = modInfoOf("Chess", "TS_Save_1", SAVE);

    protected static final Asset ASSETBUNDLE1 = assetOf(ASSETBUNDLE, "http://www.test.com/asset/1");
    protected static final Asset ASSETBUNDLE1_CACHED = cached(ASSETBUNDLE1);
    protected static final Asset ASSETBUNDLE1_ERROR = ASSETBUNDLE1.withError("404 Not Found");
    protected static final Asset AUDIO1 = assetOf(AUDIO, "http://www.test.com/audio/1");
    protected static final Asset AUDIO1_CACHED = cached(AUDIO1);
    protected static final Asset AUDIO1_ERROR = AUDIO1.withError("403 Forbidden");
    protected static final Asset IMAGE1_CACHED = cached(assetOf(IMAGE, "http://www.test.com/image/1"));
    protected static final Asset PDF1_CACHED_ERROR = cached(assetOf(PDF, "https://www.test.com/notapdf/1"))
            .withError("Content type image/jpeg is not valid for PDF");

    protected static final Path AUDIO1_CACHE_FILE = AUDIO1_CACHED.getCacheFile();
    protected static final Path IMAGE1_CACHE_FILE = IMAGE1_CACHED.getCacheFile();

    protected static final Mod CHESS = modOf(CHESS_INFO,
            AUDIO1, ASSETBUNDLE1_ERROR, IMAGE1_CACHED, PDF1_CACHED_ERROR);
    protected static final Mod CHESS_CACHED = modOf(CHESS_INFO,
            AUDIO1_CACHED, ASSETBUNDLE1_CACHED, IMAGE1_CACHED, PDF1_CACHED_ERROR);
    protected static final Mod CHECKERS = modOf(CHECKERS_INFO);
    protected static final Mod CHESS_SAVE = modOf(CHESS_SAVE_INFO);

    protected static final String OLD_STEAM_CLOUD_CACHE_FILENAME =
            "httpcloud3steamusercontentcomugc4502007633708726063A1B6398B2F72BA68F7FDE7D0F1A7D24D5714282B";
    protected static final Path OLD_STEAM_CLOUD_CACHE_FILE = OPTIONS.getCacheDir(IMAGE)
            .resolve(OLD_STEAM_CLOUD_CACHE_FILENAME + "1.jpg");
    protected static final Path OLD_STEAM_CLOUD_CACHE_FILE_2 = OPTIONS.getCacheDir(IMAGE)
            .resolve(OLD_STEAM_CLOUD_CACHE_FILENAME + "2.jpg");
    protected static final String NEW_STEAM_CLOUD_CACHE_FILENAME =
            "httpssteamusercontentaakamaihdnetugc4502007633708726063A1B6398B2F72BA68F7FDE7D0F1A7D24D5714282B";
    protected static final Path NEW_STEAM_CLOUD_CACHE_FILE = OPTIONS.getCacheDir(IMAGE)
            .resolve(NEW_STEAM_CLOUD_CACHE_FILENAME + "1.jpg");
    protected static final Path NEW_STEAM_CLOUD_CACHE_FILE_2 = OPTIONS.getCacheDir(IMAGE)
            .resolve(NEW_STEAM_CLOUD_CACHE_FILENAME + "2.jpg");

    @SystemStub
    protected final SystemErrAndOut systemErrAndOut = new SystemErrAndOut(
            new MultiplexOutput(new TapStream(), Output.fromStream(System.out))
    );

    protected final TabletopSimulator tabletopSimulator = mock(TabletopSimulator.class);

    protected CommandTestBase() {
    }

    protected CommandLine getTestCommandLine(Class<? extends AbstractCommand> cls) {
        try {
            AbstractCommand command = cls.getDeclaredConstructor(TabletopSimulator.class)
                    .newInstance(this.tabletopSimulator);
            return new CommandLine(command);
        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static ModInfo modInfoOf(String name, String id, ModType type) {
        String filename = "%s.json".formatted(id);
        Path file = switch (type) {
            case MOD -> OPTIONS.getWorkshopDir().resolve(filename);
            case SAVE -> OPTIONS.getSavesDir().resolve(filename);
        };

        return new ModInfo.Builder()
                .date(generateDate(id))
                .name(name)
                .version("v12.3.4")
                .file(file)
                .type(type)
                .build();
    }

    private static String generateDate(String id) {
        long shift = id.hashCode() % YEAR_IN_SECONDS;
        LocalDateTime date = DEFAULT_DATE.minusSeconds(shift);
        return DATE_TIME_FORMATTER.format(date);
    }

    private static Asset assetOf(AssetType type, String url) {
        return new Asset.Builder().type(type).jsonUrl(url).build();
    }

    private static Mod modOf(ModInfo modInfo, Asset... assets) {
        return new Mod.Builder().modInfo(modInfo).assets(List.of(assets)).build();
    }

    private static Asset cached(Asset asset) {
        Path cacheDir = OPTIONS.getCacheDir(asset.getType());
        String baseName = getRandomString(asset.getUrl());
        String fileExtension = getFileExtension(asset.getType());
        String filename = "%s.%s".formatted(baseName, fileExtension);
        Path cacheFile = cacheDir.resolve(filename);
        return asset.withCacheFile(cacheFile);
    }

    private static String getRandomString(String s) {
        return Integer.toHexString(s.hashCode());
    }

    private static String getFileExtension(AssetType type) {
        return switch (type) {
            case ASSETBUNDLE -> "unity3d";
            case AUDIO -> "mp3";
            case IMAGE -> "jpg";
            case MODEL -> "obj";
            case PDF -> "PDF";
        };
    }

}
