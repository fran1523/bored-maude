/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.backup;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.UnitTestBase;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Path;
import java.time.Instant;

import static hr.fran.bored.maude.tts.ZipAssert.assertThatZip;
import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestoreManagerTest extends UnitTestBase {

    @Mock
    private ModInfoManager modInfoManager;

    @Mock
    private ModManager modManager;

    private RestoreManager sut;

    @BeforeEach
    @Override
    protected void setup() {
        super.setup();
        this.sut = new RestoreManager(this.modInfoManager, this.modManager);
    }

    @Test
    void restoreBackup_should_restore_backup_from_archive_when_zip_file_is_valid() {
        Path backupFile = getBackupFile(CHESS_BACKUP_FILE);
        Path modFile = this.testOptions.getWorkshopDir().resolve(CHESS_MOD_FILE);

        when(this.modInfoManager.getModInfo(eq(modFile), any(Options.class))).thenReturn(CHESS_MOD_INFO);
        when(this.modManager.getMod(eq(CHESS_MOD_INFO), any(Options.class))).thenReturn(CHESS_MOD);

        Mod result = this.sut.restoreBackup(backupFile, testOptions);

        assertThat(result).isEqualTo(CHESS_MOD);
        assertThatZip(backupFile).wasExtractedTo(this.testOptions.getTtsDir());
    }

    @Test
    void restoreBackup_should_overwrite_existing_files_when_restoring_backup() {
        Path backupFile = getBackupFile(CHESS_BACKUP_FILE);
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);
        final Instant oneMinuteAgo = setLastModifiedTimeOneMinuteAgo(modFile);

        when(this.modInfoManager.getModInfo(eq(modFile), any(Options.class))).thenReturn(CHESS_MOD_INFO);
        when(this.modManager.getMod(eq(CHESS_MOD_INFO), any(Options.class))).thenReturn(CHESS_MOD);

        this.sut.restoreBackup(backupFile, testOptions);

        assertThat(getLastModifiedTime(modFile)).isNotEqualTo(oneMinuteAgo);
    }

    @Test
    void restoreBackup_should_rename_old_steam_cloud_files() {
        Path backupFile = getBackupFile("Chess (12345678)-with-old-steam-cloud.zip");
        Path modFile = this.testOptions.getWorkshopDir().resolve(CHESS_MOD_FILE);

        when(this.modInfoManager.getModInfo(eq(modFile), any(Options.class))).thenReturn(CHESS_MOD_INFO);
        when(this.modManager.getMod(eq(CHESS_MOD_INFO), any(Options.class))).thenReturn(CHESS_MOD);

        this.sut.restoreBackup(backupFile, testOptions);

        String oldSteamCloudFilename = TtsFileUtils.getCacheFileBaseName(OLD_STEAM_CLOUD_URL) + ".jpg";
        String newSteamCloudFilename = TtsFileUtils.getCacheFileBaseName(NEW_STEAM_CLOUD_URL) + ".jpg";
        assertThat(getExpectedCacheFile(IMAGE, oldSteamCloudFilename)).doesNotExist();
        assertThat(getExpectedCacheFile(IMAGE, newSteamCloudFilename)).exists();
    }

    @Test
    void restoreBackup_should_throw_exception_when_zip_file_contains_multiple_mods() {
        Path backupFile = getBackupFile("Chess (12345678)-with-multiple-mods.zip");

        assertThatThrownBy(() -> this.sut.restoreBackup(backupFile, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("One mod file expected, 2 mods found: "
                        + "[Mods/Workshop/12345678.json, Mods/Workshop/23456789.json]");
    }

    @Test
    void restoreBackup_should_throw_exception_when_zip_file_does_not_contain_mod() {
        Path backupFile = getBackupFile("Chess (12345678)-without-mod.zip");

        assertThatThrownBy(() -> this.sut.restoreBackup(backupFile, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("One mod file expected, 0 mods found: []");
    }

    @Test
    void restoreBackup_should_throw_exception_when_zip_file_contains_illegal_path() {
        Path backupFile = getBackupFile("Chess (12345678)-with-illegal-path.zip");

        assertThatThrownBy(() -> this.sut.restoreBackup(backupFile, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("Files cannot be extracted outside Tabletop Simulator directories: "
                        + "[Mods/Images/../../../../httpwwwtestcomimage1.jpg]");
    }

    @Test
    void restoreBackup_should_throw_exception_when_zip_file_contains_illegal_file_extension() {
        Path backupFile = getBackupFile("Chess (12345678)-with-illegal-ext.zip");

        assertThatThrownBy(() -> this.sut.restoreBackup(backupFile, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("Archive contains illegal files: Mods/Images/httpwwwtestcomimage1.exe");
    }

    @Test
    void restoreBackup_should_restore_save_backup_from_archive_when_zip_file_is_valid() {
        Path backupFile = getBackupFile("Chess (TS_Save_1).zip");
        Path saveFile = this.testOptions.getSavesDir().resolve(CHESS_SAVE_FILE);

        when(this.modInfoManager.getModInfo(eq(saveFile), any(Options.class))).thenReturn(CHESS_SAVE_INFO);
        when(this.modManager.getMod(eq(CHESS_SAVE_INFO), any(Options.class))).thenReturn(CHESS_SAVE);

        Mod result = this.sut.restoreBackup(backupFile, testOptions);

        assertThat(result).isEqualTo(CHESS_SAVE);
        assertThatZip(backupFile).wasExtractedTo(this.testOptions.getTtsDir());
    }

    @Test
    void restoreBackup_should_restore_mod_thumbnail_when_backup_is_ttsmod() {
        Path backupFile = getBackupFile("Chess (12345678).ttsmod");

        when(this.modInfoManager.getModInfo(any(), any(Options.class))).thenReturn(CHESS_MOD_INFO);
        when(this.modManager.getMod(eq(CHESS_MOD_INFO), any(Options.class))).thenReturn(CHESS_MOD);

        this.sut.restoreBackup(backupFile, testOptions);

        Path modThumbnail = this.testOptions.getWorkshopDir().resolve(CHESS_THUMBNAIL);
        assertThat(modThumbnail).exists();
    }

}
