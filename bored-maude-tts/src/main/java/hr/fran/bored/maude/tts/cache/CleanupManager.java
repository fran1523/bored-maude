/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.cache;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.common.SteamCloud;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.mod.AssetType;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CleanupManager {

    private static final Logger log = LoggerFactory.getLogger(CleanupManager.class);

    private final ModInfoManager modInfoManager;

    private final ModManager modManager;

    public CleanupManager(ModInfoManager modInfoManager, ModManager modManager) {
        this.modInfoManager = modInfoManager;
        this.modManager = modManager;
    }

    public Cleanup cleanup(Options options) {
        Map<Path, Path> renamedFileToNewFile = updateSteamCloudFilenames(options);
        this.modManager.invalidateTabletopSimulatorCache();
        Map<Path, Long> deletedFileToSizeInBytes = deleteOrphans(options);
        return new Cleanup(renamedFileToNewFile, deletedFileToSizeInBytes);
    }

    public Map<Path, Path> updateSteamCloudFilenames(Options options) {
        Map<Path, Path> fileToNewFile = new HashMap<>();

        Collection<Path> filesFromCache = getFilesFromCache(options);
        for (Path file : filesFromCache) {
            String filename = FilenameUtils.getName(file.toString());
            String newFilename = SteamCloud.toNewFilename(filename);
            if (!filename.equals(newFilename)) {
                Optional<Path> newFile = renameFileIfNotExists(file, newFilename);
                newFile.ifPresent(
                        nf -> {
                            log.info("Moved Steam Cloud file '{}' to '{}'", file, nf);
                            fileToNewFile.put(file, nf);
                        }
                );
            }
        }

        log.info("Moved {} Steam Cloud files", fileToNewFile.size());
        return fileToNewFile;
    }

    private Optional<Path> renameFileIfNotExists(Path file, String newFilename) {
        try {
            Path parent = file.getParent();
            Path newFile = parent == null ? Path.of(newFilename) : parent.resolve(newFilename);

            if (Files.exists(newFile)) {
                return Optional.empty();
            }

            return Optional.of(Files.move(file, newFile));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Map<Path, Long> deleteOrphans(Options options) {
        Collection<Path> filesFromAssets = getFilesFromAssets(options);
        Collection<Path> filesFromCache = getFilesFromCache(options);
        Collection<Path> orphanRawFiles = getOrphanRawFiles(filesFromAssets, options);

        Set<Path> orphanFiles = new HashSet<>(filesFromCache);
        orphanFiles.removeAll(filesFromAssets);
        orphanFiles.addAll(orphanRawFiles);

        Map<Path, Long> orphanFileToSizeInBytes = deleteOrphanFiles(orphanFiles);

        log.info("Deleted {} orphan files", orphanFileToSizeInBytes.size());
        return orphanFileToSizeInBytes;
    }

    private Collection<Path> getFilesFromAssets(Options options) {
        return this.modInfoManager.getWorkshopAndSavesModInfos(options).parallelStream()
                .map(info -> this.modManager.getMod(info, options))
                .flatMap(mod -> mod.getAssets().stream())
                .flatMap(asset -> Stream.ofNullable(asset.getCacheFile()))
                .collect(Collectors.toUnmodifiableSet());
    }

    private Collection<Path> getFilesFromCache(Options options) {
        return Stream.of(AssetType.values())
                .flatMap(type -> getFilesFromCache(type, options).stream())
                .collect(Collectors.toUnmodifiableSet());
    }

    private Collection<Path> getFilesFromCache(AssetType type, Options options) {
        Path cacheDir = options.getCacheDir(type);
        if (!Files.isDirectory(cacheDir)) {
            return List.of();
        }

        try (Stream<Path> cacheFiles = Files.find(cacheDir, 1,
                (path, basicFileAttributes) -> basicFileAttributes.isRegularFile()
        )) {
            return cacheFiles.toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Map<Path, Long> deleteOrphanFiles(Collection<Path> orphans) {
        return orphans.stream().collect(Collectors.toUnmodifiableMap(
                file -> file,
                this::deleteOrphanFile
        ));
    }

    private long deleteOrphanFile(Path orphan) {
        try {
            long sizeInBytes = Files.size(orphan);
            Files.delete(orphan);
            log.debug("Deleted orphan file '{}'", orphan);
            return sizeInBytes;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Collection<Path> getOrphanRawFiles(Collection<Path> assetFiles, Options options) {
        Collection<String> assetBaseNames = assetFiles.stream()
                .map(TtsFileUtils::getBaseName)
                .collect(Collectors.toUnmodifiableSet());

        return Stream.of(AssetType.values())
                .flatMap(type -> type.getRawCacheDir().stream())
                .flatMap(rawCacheDir -> getOrphanRawFiles(rawCacheDir, assetBaseNames, options).stream())
                .toList();
    }

    private Collection<Path> getOrphanRawFiles(String rawCacheDir, Collection<String> assetBaseNames, Options options) {
        Path rawCachePath = options.getModsDir().resolve(rawCacheDir);
        if (!Files.isDirectory(rawCachePath)) {
            return List.of();
        }

        try (Stream<Path> rawCacheFiles = Files.find(rawCachePath, 1,
                (file, basicFileAttributes) -> basicFileAttributes.isRegularFile()
                        && !assetBaseNames.contains(TtsFileUtils.getBaseName(file))
        )) {
            return rawCacheFiles.toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
