/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.Mod;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Parameters;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static picocli.CommandLine.Command;

@Command(name = "restore",
        description = "Restores mod and its assets from a backup archive",
        mixinStandardHelpOptions = true)
public class RestoreCommand extends AbstractCommand {

    private static final Collection<String> BACKUP_FILE_EXTENSIONS = Set.of("zip", "ttsmod");

    private static final Pattern GLOB_CHARACTERS = Pattern.compile("[*?\\[\\]{}]");

    private static final Logger log = LoggerFactory.getLogger(RestoreCommand.class);

    // glob pattern syntax:
    // https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/nio/file/FileSystem.html#getPathMatcher(java.lang.String)
    @Parameters(
            paramLabel = "backups",
            description = "Backup archive or wildcard patterns",
            arity = "1..*"
    )
    protected Collection<String> backupFilePatterns;

    public RestoreCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    public void run() {
        Options options = getOptions();
        Collection<Path> backupFiles = getBackupFiles();

        restoreBackups(backupFiles, options);
    }

    private Collection<Path> getBackupFiles() {
        return this.backupFilePatterns.stream()
                .flatMap(pattern -> findBackupFiles(pattern).stream())
                .sorted()
                .toList();
    }

    private Collection<Path> findBackupFiles(String backupFilePattern) {
        Path baseDir = getBaseDir(FilenameUtils.getFullPath(backupFilePattern));
        if (!Files.isDirectory(baseDir)) {
            return List.of();
        }

        PathMatcher matcher = getFileSystem().getPathMatcher("glob:" + backupFilePattern);

        try (Stream<Path> backupFiles = Files.find(baseDir, Integer.MAX_VALUE,
                (file, basicFileAttributes) -> basicFileAttributes.isRegularFile()
                        && isBackupArchive(file)
                        && matcher.matches(file)
        )) {
            return backupFiles.toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Path getBaseDir(String pattern) {
        if (containsGlob(pattern)) {
            String parent = getParent(pattern);
            return getBaseDir(parent);
        }

        return getPath(pattern);
    }

    private boolean containsGlob(String pattern) {
        return GLOB_CHARACTERS.matcher(pattern).find();
    }

    private String getParent(String pattern) {
        int index = FilenameUtils.indexOfLastSeparator(pattern);
        return index >= 0
                ? pattern.substring(0, index)
                : "";
    }

    private Path getPath(String path) {
        return getFileSystem().getPath(path);
    }

    protected FileSystem getFileSystem() {
        return FileSystems.getDefault();
    }

    private boolean isBackupArchive(Path file) {
        return BACKUP_FILE_EXTENSIONS.contains(
                FilenameUtils.getExtension(file.toString()).toLowerCase(Locale.ROOT)
        );
    }

    private void restoreBackups(Collection<Path> backupFiles, Options options) {
        if (backupFiles.isEmpty()) {
            getOut().println("Backup file not found");
            return;
        }

        long successCount = backupFiles.stream()
                .map(file -> restoreBackup(file, options))
                .filter(isSuccess -> isSuccess)
                .count();

        printSummary(successCount, backupFiles.size());
    }

    private boolean restoreBackup(Path backupFile, Options options) {
        try {
            Mod mod = this.tabletopSimulator.restoreBackup(backupFile, options);
            printSuccess(mod, backupFile);
            return true;
        } catch (Exception e) {
            log.warn("'{}' restore failed", backupFile, e);
            printFailure(backupFile, e);
            return false;
        }
    }

    private void printSuccess(Mod mod, Path backupFile) {
        getOut().printf("%s restored from '%s'%s%n", getDisplayName(mod), backupFile,
                getModIncompleteSuffix(mod.hasUncachedNonAudioAssets()));
    }

    private void printFailure(Path backupFile, Exception e) {
        getOut().printf("'%s' restore failed: %s%n", backupFile, e.getMessage());
    }

    private void printSummary(long successCount, int backupCount) {
        long failureCount = backupCount - successCount;
        getOut().printf("%n%s/%s mods restored, %s restores failed%n",
                successCount, backupCount, failureCount);
    }

}
