/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.backup;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModInfo;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class BackupManager {

    private static final String BACKUP_SUFFIX = "zip";

    // Reference: https://en.wikipedia.org/wiki/Filename#Comparison_of_filename_limitations
    private static final Pattern ILLEGAL_FILENAME_CHARACTERS = Pattern.compile("[\"*/:<>?\\\\|]");

    private static final int MAX_FILENAME_LENGTH = 255;

    private static final Logger log = LoggerFactory.getLogger(BackupManager.class);

    public BackupFile createBackup(Mod mod, Options options) {
        if (mod.hasUncachedNonAudioAssets() && !options.isBackupIncompleteMods()) {
            throw new TtsException("Some assets are not downloaded");
        }

        log.info("Backing up {}", mod);

        Path tempFile = null;
        try {
            tempFile = TtsFileUtils.createTempFile(options.getBackupDir());
            createTempBackupArchive(mod, tempFile, options.getTtsDir());
            Path backupFile = getBackupFile(mod, options.getBackupDir());
            TtsFileUtils.move(tempFile, backupFile);

            log.info("Backup for {} saved to '{}'", mod, backupFile);
            return new BackupFile.Builder()
                    .mod(mod)
                    .file(backupFile)
                    .build();
        } finally {
            TtsFileUtils.deleteIfExists(tempFile);
        }
    }

    private void createTempBackupArchive(Mod mod, Path tempFile, Path ttsDir) {
        try (OutputStream out = Files.newOutputStream(tempFile);
             BufferedOutputStream buffOut = new BufferedOutputStream(out);
             ZipOutputStream zipOut = new ZipOutputStream(buffOut)
        ) {
            ModInfo modInfo = mod.getModInfo();
            copyIntoZipArchive(mod.getAssets(), zipOut, ttsDir);
            copyIntoZipArchive(modInfo.getFile(), zipOut, ttsDir);
            modInfo.getThumbnail().ifPresent(
                    thumbnail -> copyIntoZipArchive(thumbnail, zipOut, ttsDir)
            );
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void copyIntoZipArchive(Collection<Asset> assets, ZipOutputStream zipOut, Path ttsDir) {
        assets.stream()
                .flatMap(asset -> Stream.ofNullable(asset.getCacheFile()))
                .distinct()
                .forEach(cacheFile -> copyIntoZipArchive(cacheFile, zipOut, ttsDir));
    }

    private void copyIntoZipArchive(Path file, ZipOutputStream zipOut, Path ttsDir) {
        Path relativePath = ttsDir.relativize(file);
        String zipPath = FilenameUtils.separatorsToUnix(relativePath.toString());

        try {
            ZipEntry zipEntry = new ZipEntry(zipPath)
                    .setLastModifiedTime(Files.getLastModifiedTime(file));
            zipOut.putNextEntry(zipEntry);
            Files.copy(file, zipOut);
            zipOut.closeEntry();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        log.debug("Added '{}' to backup archive", zipPath);
    }

    private Path getBackupFile(Mod mod, Path backupDir) {
        String backupFilename = getBackupFilename(mod.getModInfo());
        Path backupFile = backupDir.resolve(backupFilename);

        if (Files.isRegularFile(backupFile)) {
            log.info("Backup file '{}' already exists, will be overwritten", backupFile);
        }
        return backupFile;
    }

    private String getBackupFilename(ModInfo modInfo) {
        String name = ILLEGAL_FILENAME_CHARACTERS.matcher(modInfo.getName()).replaceAll("_").trim();
        String id = modInfo.getId().trim();
        int maxNameLength = MAX_FILENAME_LENGTH - id.length() - BACKUP_SUFFIX.length() - 4;
        String shortName = name.substring(0, Math.min(name.length(), maxNameLength));

        return "%s (%s).%s".formatted(shortName, id, BACKUP_SUFFIX);
    }

}
