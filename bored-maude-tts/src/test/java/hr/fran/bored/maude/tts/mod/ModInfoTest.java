/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.UnitTestBase;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

class ModInfoTest extends UnitTestBase {

    @Test
    void getId_should_return_id_from_filename() {
        assertThat(CHESS_MOD_INFO.getId()).isEqualTo("12345678");
    }

    @Test
    void getThumbnail_should_return_empty_when_mod_has_no_thumbnail() {
        assertThat(CHESS_MOD_INFO.getThumbnail()).isEmpty();
    }

    @Test
    void getThumbnail_should_return_thumbnail_when_mod_has_thumbnail() {
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);
        Path modThumbnail = givenWorkshopFile(CHESS_THUMBNAIL);

        ModInfo modInfo = CHESS_MOD_INFO.withFile(modFile);

        assertThat(modInfo.getThumbnail()).contains(modThumbnail);
    }

    @Test
    void toString_should_handle_spaces_in_name() {
        ModInfo modInfo = CHESS_MOD_INFO.withName("Chess 2");

        assertThat(modInfo).hasToString("'Chess 2' [12345678]");
    }

}
