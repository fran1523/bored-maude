/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.UnitTestBase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ModManagerTest extends UnitTestBase {

    private final ModManager sut = new ModManager();

    @ParameterizedTest
    @ValueSource(strings = {
            "12345678.json",
            "12345678-blacklisted-field.json",
            "12345678-duplicate-url.json",
            "12345678-metadata-url.json",
            "12345678-unknown-field.json"
    })
    void getMod_should_return_result_when_input_json_is_valid(String filename) {
        Path modFile = givenWorkshopFile(filename);
        ModInfo modInfo = CHESS_MOD_INFO.withFile(modFile);

        Mod result = this.sut.getMod(modInfo, testOptions);

        assertThat(result).isEqualTo(CHESS_MOD.withModInfo(modInfo));
    }

    @Test
    void getMod_should_return_result_when_input_json_contains_localised_urls() {
        Path modFile = givenWorkshopFile("12345678-localised-url.json");
        ModInfo modInfo = CHESS_MOD_INFO.withFile(modFile);

        Mod result = this.sut.getMod(modInfo, testOptions);

        assertThat(result.getAssets()).containsExactlyInAnyOrder(ASSETBUNDLE1, AUDIO1, IMAGE1, IMAGE2, MODEL1, PDF1);
    }

    @Test
    void getMod_should_match_cache_files_to_assets() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_MOD_INFO);

        Path assetbundle1CacheFile = givenCacheFileForAsset(ASSETBUNDLE1);
        Path audio1CacheFile = givenCacheFileForAsset(AUDIO1);
        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Path model1CacheFile = givenCacheFileForAsset(MODEL1);
        Path pdf1CacheFile = givenCacheFileForAsset(PDF1);

        Mod result = this.sut.getMod(modInfo, testOptions);

        assertThat(result.getAssets()).containsExactlyInAnyOrder(
                ASSETBUNDLE1.withCacheFile(assetbundle1CacheFile),
                AUDIO1.withCacheFile(audio1CacheFile),
                IMAGE1.withCacheFile(image1CacheFile),
                MODEL1.withCacheFile(model1CacheFile),
                PDF1.withCacheFile(pdf1CacheFile)
        );
    }

    @Test
    void getMod_should_match_new_steam_cloud_files_to_assets_with_old_steam_cloud_urls() {
        Path modFile = givenWorkshopFile("12345678-old-steam-cloud-url.json");
        ModInfo modInfo = CHESS_MOD_INFO.withFile(modFile);

        Path newSteamCloudCacheFile = givenCacheFileForAsset(NEW_STEAM_CLOUD_ASSET);

        Mod result = this.sut.getMod(modInfo, testOptions);

        assertThat(result.getAssets()).singleElement()
                .isEqualTo(OLD_STEAM_CLOUD_ASSET.withCacheFile(newSteamCloudCacheFile));
    }

    @Test
    void getMod_should_match_last_modified_cache_file_when_there_are_multiple_candidates() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_MOD_INFO);

        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Path oldImage1CacheFile = givenCacheFileForAsset(IMAGE1, "IMAGE.png");
        setLastModifiedTimeOneMinuteAgo(oldImage1CacheFile);

        Mod result = this.sut.getMod(modInfo, testOptions);

        Asset expectedAsset = assetWithCacheError(IMAGE1, image1CacheFile,
                "Multiple cache files found: %s, %s".formatted(image1CacheFile, oldImage1CacheFile));
        assertThat(result.getAssets()).contains(expectedAsset);
    }

    @Test
    void getMod_should_add_error_to_asset_with_unexpected_content() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_MOD_INFO);
        Path pdf1CacheFile = givenCacheFileForAsset(PDF1, "IMAGE.jpg");

        Mod result = this.sut.getMod(modInfo, testOptions);

        Asset expectedAsset = assetWithCacheError(PDF1, pdf1CacheFile, "Content type image/jpeg is not valid for PDF");
        assertThat(result.getAssets()).contains(expectedAsset);
    }

    @Test
    void getMod_should_add_error_to_asset_with_empty_cache_file() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_MOD_INFO);
        Path pdf1CacheFile = givenCacheFileForAsset(PDF1, "empty-file");

        Mod result = this.sut.getMod(modInfo, testOptions);

        Asset expectedAsset = assetWithCacheError(PDF1, pdf1CacheFile, "File is empty");
        assertThat(result.getAssets()).contains(expectedAsset);
    }

    @Test
    void getMod_should_throw_exception_when_input_json_cannot_be_parsed() {
        Path modFile = givenWorkshopFile("invalid.json");
        ModInfo modInfo = CHESS_MOD_INFO.withFile(modFile);

        assertThatThrownBy(() -> this.sut.getMod(modInfo, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("Cannot read mod JSON '%s'", modFile);
    }

    @Test
    void getMod_should_return_result_when_save_file_is_valid() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_SAVE_INFO);

        Mod result = this.sut.getMod(modInfo, testOptions);

        assertThat(result).isEqualTo(CHESS_SAVE.withModInfo(modInfo));
    }

    @Test
    void getMod_should_match_cache_files_to_assets_when_loading_saved_mod() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_SAVE_INFO);

        Path assetbundle1CacheFile = givenCacheFileForAsset(ASSETBUNDLE1);
        Path audio1CacheFile = givenCacheFileForAsset(AUDIO1);
        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Path model1CacheFile = givenCacheFileForAsset(MODEL1);
        Path pdf1CacheFile = givenCacheFileForAsset(PDF1);

        Mod result = this.sut.getMod(modInfo, testOptions);

        assertThat(result.getAssets()).containsExactlyInAnyOrder(
                ASSETBUNDLE1.withCacheFile(assetbundle1CacheFile),
                AUDIO1.withCacheFile(audio1CacheFile),
                IMAGE1.withCacheFile(image1CacheFile),
                MODEL1.withCacheFile(model1CacheFile),
                PDF1.withCacheFile(pdf1CacheFile)
        );
    }

    private static Asset assetWithCacheError(Asset asset, Path cacheFile, String error) {
        return new Asset.Builder().from(asset)
                .cacheFile(cacheFile)
                .error(error)
                .build();
    }

}
