plugins {
    id("jacoco")
    id("java-library")
}

dependencies {
    annotationProcessor(libs.immutables.value)
    api(libs.gson)
    compileOnly(libs.bundles.immutables)
    implementation(libs.commons.codec)
    implementation(libs.commons.io)
    implementation(libs.slf4j.api)
    implementation(libs.tika.core)

    testImplementation(libs.bundles.test)
    testImplementation(libs.jimfs)
    testImplementation(libs.system.stubs.core)
    testRuntimeOnly(libs.bundles.tinylog)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.register("versionText") {
    group = "Classes"
    description = "Writes version to a text file in project resources"
    dependsOn(tasks.processResources)
    doLast {
        layout.buildDirectory.file("resources/main/version")
        layout.buildDirectory.file("resources/main/version").get().asFile.apply {
            createNewFile()
            writeText(version.toString())
        }
    }
}

tasks.classes {
    dependsOn(tasks.named("versionText"))
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
    // required for hr.fran.bored.maude.tts.UnitTestBase.getBodyHandlerFile
    jvmArgs("--add-opens=java.net.http/jdk.internal.net.http=ALL-UNNAMED")
}

tasks.jacocoTestReport {
    reports.xml.required = true
    dependsOn(tasks.test)
}

testing.suites {
    register<JvmTestSuite>("integrationTest") {
        dependencies {
            implementation(project())
            implementation(libs.assertj.core)
            implementation(libs.junit.jupiter.api)
            implementation(libs.junit.jupiter.params)
        }
    }
}
