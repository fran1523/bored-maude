/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.cache.DownloadListener;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

class DownloadCommandTest extends CommandTestBase {

    private final Collection<DownloadListener> listeners = new HashSet<>();

    private CommandLine sut;

    @BeforeEach
    protected void setup() {
        doAnswer(i -> this.listeners.add(i.getArgument(0)))
                .when(tabletopSimulator).addDownloadListener(any(DownloadListener.class));
        this.sut = getTestCommandLine(DownloadCommand.class);
    }

    @Test
    void should_download_mod_assets_when_using_default_parameters() {
        Mod mod = CHESS.withAssets(IMAGE1_CACHED, AUDIO1, ASSETBUNDLE1);

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(mod);
        when(this.tabletopSimulator.downloadAssets(eq(mod), any(Options.class)))
                .thenAnswer(i -> notifyListeners(
                        mod, CHESS.withAssets(IMAGE1_CACHED, AUDIO1_CACHED, ASSETBUNDLE1_CACHED)
                ));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678]
                ================
                Downloading http://www.test.com/asset/1 ... saved as '%s'
                Downloading http://www.test.com/audio/1 ... saved as '%s'
                Download finished, 2 new assets downloaded, 0 errors
                
                1 mods downloaded
                """.formatted(ASSETBUNDLE1_CACHED.getCacheFile(), AUDIO1_CACHED.getCacheFile())
        );
    }

    @Test
    void should_download_mod_assets_when_http_timeout_is_provided() {
        Mod mod = CHESS.withAssets(AUDIO1);

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(mod);
        when(this.tabletopSimulator.downloadAssets(eq(mod), argThat(
                options -> options.getConnectTimeoutInSeconds().equals(Optional.of(60))
        ))).thenAnswer(i -> notifyListeners(mod, CHESS.withAssets(AUDIO1_CACHED)));

        this.sut.execute("--connect-timeout", "60");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678]
                ================
                Downloading http://www.test.com/audio/1 ... saved as '%s'
                Download finished, 1 new assets downloaded, 0 errors
                
                1 mods downloaded
                """.formatted(AUDIO1_CACHED.getCacheFile())
        );
    }

    @Test
    void should_download_mod_assets_when_overwrite_is_enabled() {
        Mod mod = CHESS.withAssets();

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(mod);
        when(this.tabletopSimulator.downloadAssets(eq(mod), argThat(Options::isOverwriteCacheFiles))).thenReturn(mod);

        this.sut.execute("--overwrite");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678]
                ================
                Download finished, 0 new assets downloaded, 0 errors
                
                1 mods downloaded
                """
        );
    }

    @Test
    void should_download_mod_assets_when_downloading_saves() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_SAVE_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_SAVE_INFO), any(Options.class))).thenReturn(CHESS_SAVE);
        when(this.tabletopSimulator.downloadAssets(eq(CHESS_SAVE), argThat(options -> options.getModType() == SAVE)))
                .thenReturn(CHESS_SAVE);

        this.sut.execute("--saves");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [TS_Save_1]
                =================
                Download finished, 0 new assets downloaded, 0 errors
                
                1 mods downloaded
                """
        );
    }

    @Test
    void should_download_mod_assets_when_filter_is_provided() {
        Mod mod = CHESS.withAssets(AUDIO1);

        when(this.tabletopSimulator.getModInfos(eq(new ModFilter("12345678")), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(mod);
        when(this.tabletopSimulator.downloadAssets(eq(mod), any(Options.class)))
                .thenAnswer(i -> notifyListeners(mod, CHESS.withAssets(AUDIO1_CACHED)));

        this.sut.execute("12345678");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678]
                ================
                Downloading http://www.test.com/audio/1 ... saved as '%s'
                Download finished, 1 new assets downloaded, 0 errors
                
                1 mods downloaded
                """.formatted(AUDIO1_CACHED.getCacheFile())
        );
    }

    @Test
    void should_download_mod_assets_when_there_are_download_errors() {
        Mod mod = CHESS.withAssets(IMAGE1_CACHED, AUDIO1, ASSETBUNDLE1);

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(mod);
        when(this.tabletopSimulator.downloadAssets(eq(mod), any(Options.class)))
                .thenAnswer(i -> notifyListeners(
                        mod, CHESS.withAssets(IMAGE1_CACHED, AUDIO1_ERROR, ASSETBUNDLE1_ERROR)
                ));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678]
                ================
                Downloading http://www.test.com/asset/1 ... ERROR: 404 Not Found
                Downloading http://www.test.com/audio/1 ... ERROR: 403 Forbidden
                Download finished, 0 new assets downloaded, 2 errors
                
                1 mods downloaded, 1 mods have errors
                """
        );
    }

    @Test
    void should_download_mod_assets_when_there_are_multiple_mods() {
        Mod chess = CHESS.withAssets(List.of(AUDIO1));
        Mod checkers = CHECKERS.withAssets(List.of(ASSETBUNDLE1));

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO, CHECKERS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(chess);
        when(this.tabletopSimulator.getMod(eq(CHECKERS_INFO), any(Options.class))).thenReturn(checkers);
        when(this.tabletopSimulator.downloadAssets(eq(chess), any(Options.class)))
                .thenAnswer(i -> notifyListeners(chess, chess.withAssets(List.of(AUDIO1_CACHED))));
        when(this.tabletopSimulator.downloadAssets(eq(checkers), any(Options.class)))
                .thenAnswer(i -> notifyListeners(checkers, checkers.withAssets(List.of(ASSETBUNDLE1_ERROR))));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Checkers [23456789]
                ===================
                Downloading http://www.test.com/asset/1 ... ERROR: 404 Not Found
                Download finished, 0 new assets downloaded, 1 errors
                
                Chess [12345678]
                ================
                Downloading http://www.test.com/audio/1 ... saved as '%s'
                Download finished, 1 new assets downloaded, 0 errors
                
                2 mods downloaded, 1 mods have errors
                """.formatted(AUDIO1_CACHED.getCacheFile())
        );
    }

    @Test
    void should_print_warning_when_no_mods_match_filter() {
        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                No mods found
                """);
    }

    private Mod notifyListeners(Mod beforeDownload, Mod afterDownload) {
        afterDownload.getAssets().stream()
                .filter(asset -> !beforeDownload.getAssets().contains(asset))
                .sorted()
                .forEach(asset -> this.listeners.forEach(
                        listener -> {
                            if (asset.hasError()) {
                                listener.downloadFailed(asset);
                            } else {
                                listener.downloadCompleted(asset);
                            }
                        }
                ));

        return afterDownload;
    }

}
