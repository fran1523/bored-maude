/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.List;

import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ListCommandTest extends CommandTestBase {

    private CommandLine sut;

    @BeforeEach
    protected void setup() {
        this.sut = getTestCommandLine(ListCommand.class);
    }

    @Test
    void should_list_all_mods_when_using_default_parameters() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO, CHECKERS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.getMod(eq(CHECKERS_INFO), any(Options.class)))
                .thenReturn(CHECKERS.withModInfo(
                        CHECKERS_INFO.withName("Checkers with a really really really really really long name")
                ));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                      Id Name                                              Updated
                --------------------------------------------------------------------------------
                23456789 Checkers with a really really really really re 10/11/2019 10:19:51 PM
                12345678 Chess                                             9/4/2021 4:31:03 AM *
                
                2 mods found
                """
        );
    }

    @Test
    void should_list_saved_mods_when_listing_saves() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL),
                argThat(options -> options.getModType() == SAVE))).thenReturn(List.of(CHESS_SAVE_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_SAVE_INFO), any(Options.class))).thenReturn(CHESS_SAVE);

        this.sut.execute("--saves");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                Id        Name                                             Updated
                --------------------------------------------------------------------------------
                TS_Save_1 Chess                                            2/9/2022 11:27:56 AM
                
                1 mods found
                """
        );
    }

    @Test
    void should_list_matching_mods_when_filter_is_provided() {
        when(this.tabletopSimulator.getModInfos(eq(new ModFilter(CHESS_INFO.getId())), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);

        this.sut.execute("12345678");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                      Id Name                                              Updated
                --------------------------------------------------------------------------------
                12345678 Chess                                             9/4/2021 4:31:03 AM *
                
                1 mods found
                """
        );
    }

    @Test
    void should_apply_column_limit_when_update_column_width_exceeds_terminal_width() {
        String longDate = "10/11/2022 11:23:45 PM" + "x".repeat(80);

        when(this.tabletopSimulator.getModInfos(any(ModFilter.class), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO.withDate(longDate)));
        when(this.tabletopSimulator.getMod(any(ModInfo.class), any(Options.class)))
                .thenReturn(CHESS.withModInfo(CHESS_INFO.withDate(longDate)));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                      Id Name                                        Updated
                --------------------------------------------------------------------------------
                12345678 Chess                                       10/11/2022 11:23:45 PMxxx *
                
                1 mods found
                """
        );
    }

    @Test
    void should_list_matching_mods_when_multiple_filters_are_provided() {
        ModFilter modFilter = new ModFilter("12345678", "chess");

        when(this.tabletopSimulator.getModInfos(eq(modFilter), any(Options.class))).thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);

        this.sut.execute("12345678", "chess");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                      Id Name                                              Updated
                --------------------------------------------------------------------------------
                12345678 Chess                                             9/4/2021 4:31:03 AM *
                
                1 mods found
                """
        );
    }

    @Test
    void should_list_matching_mods_when_custom_tts_dir_is_provided() {
        ModFilter modFilter = new ModFilter("12345678", "chess");
        Path ttsPath = Path.of("tts");

        when(this.tabletopSimulator.getModInfos(eq(modFilter), argThat(options -> options.getTtsDir().equals(ttsPath))))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), argThat(options -> options.getTtsDir().equals(ttsPath))))
                .thenReturn(CHESS);

        this.sut.execute("--path", "tts", "12345678", "chess");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                      Id Name                                              Updated
                --------------------------------------------------------------------------------
                12345678 Chess                                             9/4/2021 4:31:03 AM *
                
                1 mods found
                """
        );
    }

    @Test
    void should_print_warning_when_no_mods_are_found() {
        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                No mods found
                """);
    }

    @Test
    void should_print_warning_when_no_mods_match_filter() {
        this.sut.execute("12345678", "chess");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                No mods found matching [12345678, chess]
                """);
    }

}
