/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import com.google.gson.stream.JsonReader;
import hr.fran.bored.maude.tts.Configuration;
import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.common.SteamCloud;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import org.apache.tika.Tika;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.gson.stream.JsonToken.END_DOCUMENT;
import static com.google.gson.stream.JsonToken.STRING;

public class ModManager {

    private static final Pattern URL_PATTERN = Pattern.compile("(?:\\{\\w+?})?https?://.*");

    private static final Pattern URL_METADATA_PATTERN = Pattern.compile("\\{\\w+?}");

    private static final Configuration CONFIG = Configuration.instance();

    private static final Tika TIKA = new Tika();

    private static final LastModifiedTimeComparator LAST_MODIFIED_TIME = new LastModifiedTimeComparator();

    private static final Logger log = LoggerFactory.getLogger(ModManager.class);

    private final TabletopSimulatorCache cache = new TabletopSimulatorCache();

    public Mod getMod(ModInfo modInfo, Options options) {
        Collection<Asset> assets = getAssets(modInfo.getFile(), options);
        Mod mod = new Mod.Builder()
                .modInfo(modInfo)
                .assets(assets)
                .build();

        log.debug("Loaded {} with {}/{} cached assets from '{}'",
                mod,
                mod.getCachedAssetsCount(),
                assets.size(),
                modInfo.getFile()
        );

        return mod;
    }

    private Collection<Asset> getAssets(Path modFile, Options options) {
        Map<String, String> urlToFieldName = getUrlsFromJson(modFile);

        return urlToFieldName.entrySet().parallelStream()
                .map(this::mapFieldNameToAssetType)
                .flatMap(Optional::stream)
                .map(urlToAssetType -> createAsset(
                        urlToAssetType.getKey(),
                        urlToAssetType.getValue(),
                        options
                ))
                .collect(Collectors.toUnmodifiableSet());
    }

    private Map<String, String> getUrlsFromJson(Path modFile) {
        Map<String, String> urlToFieldName = new ConcurrentHashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(modFile);
             JsonReader jsonReader = new JsonReader(reader)
        ) {
            while (jsonReader.peek() != END_DOCUMENT) {
                switch (jsonReader.peek()) {
                    case BEGIN_ARRAY -> jsonReader.beginArray();
                    case END_ARRAY -> jsonReader.endArray();
                    case BEGIN_OBJECT -> jsonReader.beginObject();
                    case END_OBJECT -> jsonReader.endObject();
                    case NAME -> addNextValueIfUrl(jsonReader, urlToFieldName);
                    default -> jsonReader.skipValue();
                }
            }

            return urlToFieldName;
        } catch (IOException e) {
            throw new TtsException("Cannot read mod JSON '%s'".formatted(modFile), e);
        }
    }

    private void addNextValueIfUrl(JsonReader jsonReader, Map<String, String> urlToFieldName) throws IOException {
        String fieldName = jsonReader.nextName();
        getStringValue(jsonReader).ifPresent(value ->
                addValueIfUrl(fieldName, value, urlToFieldName)
        );
    }

    private Optional<String> getStringValue(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == STRING) {
            return Optional.of(jsonReader.nextString());
        }
        return Optional.empty();
    }

    private void addValueIfUrl(String fieldName, String value, Map<String, String> urlToFieldName) {
        if (isUrl(value)) {
            Collection<String> urls = stripTtsMetadata(value);
            urls.forEach(url -> urlToFieldName.put(url, fieldName));
        }
    }

    private boolean isUrl(String s) {
        return URL_PATTERN.matcher(s).matches();
    }

    // BackURL values may have "{Unique}" appended to the URL
    // URLs can be localised, for example: {en}url1{de}url2
    private Collection<String> stripTtsMetadata(String url) {
        return Stream.of(URL_METADATA_PATTERN.split(url))
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .toList();
    }

    private Optional<Map.Entry<String, AssetType>> mapFieldNameToAssetType(
            Map.Entry<String, String> urlToFieldName
    ) {
        String url = urlToFieldName.getKey();
        String fieldName = urlToFieldName.getValue();
        if (CONFIG.isFieldNameBlacklisted(fieldName)) {
            return Optional.empty();
        }

        Optional<AssetType> assetType = CONFIG.getAssetType(fieldName);
        if (assetType.isEmpty()) {
            log.warn("Cannot find asset type for '{}': {}", fieldName, url);
        }

        return assetType.map(type -> Map.entry(url, type));
    }

    private Asset createAsset(String url, AssetType type, Options options) {
        Asset.Builder assetBuilder = new Asset.Builder().type(type).jsonUrl(url);

        Collection<Path> cacheFiles = getCacheFiles(type, SteamCloud.toNewUrl(url), options);
        @Nullable Path cacheFile = cacheFiles.stream().max(LAST_MODIFIED_TIME).orElse(null);
        assetBuilder.cacheFile(cacheFile);

        try {
            validateSingleFile(cacheFiles);
            validateNotEmptyFile(cacheFile);
            validateContentType(cacheFile, type);
        } catch (TtsException e) {
            assetBuilder.error(Objects.requireNonNullElse(e.getMessage(), e.toString()));
        }

        return assetBuilder.build();
    }

    private Collection<Path> getCacheFiles(AssetType type, String url, Options options) {
        String baseName = TtsFileUtils.getCacheFileBaseName(url);
        return this.cache.get(type, baseName, options);
    }

    private void validateSingleFile(Collection<Path> cacheFiles) {
        if (cacheFiles.size() > 1) {
            throw new TtsException("Multiple cache files found: %s".formatted(
                    cacheFiles.stream().map(Objects::toString).sorted().collect(Collectors.joining(", "))
            ));
        }
    }

    private void validateNotEmptyFile(@Nullable Path cacheFile) {
        try {
            if (cacheFile != null && Files.size(cacheFile) == 0L) {
                throw new TtsException("File is empty");
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void validateContentType(@Nullable Path cacheFile, AssetType type) {
        if (cacheFile == null) {
            return;
        }

        try {
            String mimeType = TIKA.detect(cacheFile);
            if (!CONFIG.isMimeTypeValidForAssetType(mimeType, type)) {
                throw new TtsException("Content type %s is not valid for %s".formatted(mimeType, type));
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void invalidateTabletopSimulatorCache() {
        this.cache.invalidate();
    }

    private static class LastModifiedTimeComparator implements Comparator<Path> {

        @Override
        public int compare(Path o1, Path o2) {
            try {
                return Files.getLastModifiedTime(o1)
                        .compareTo(Files.getLastModifiedTime(o2));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

    }

    private static class TabletopSimulatorCache {

        @Nullable Map<AssetType, Map<String, List<Path>>> typeToBasenameToCacheFileMap;

        public List<Path> get(AssetType type, String baseName, Options options) {
            return getTypeToBasenameToCacheFileMap(options)
                    .getOrDefault(type, Map.of())
                    .getOrDefault(baseName, List.of());
        }

        public void invalidate() {
            this.typeToBasenameToCacheFileMap = null;
        }

        private synchronized Map<AssetType, Map<String, List<Path>>> getTypeToBasenameToCacheFileMap(Options options) {
            if (this.typeToBasenameToCacheFileMap == null) {
                this.typeToBasenameToCacheFileMap = createTypeToBasenameToCacheFileMap(options);
            }
            return this.typeToBasenameToCacheFileMap;
        }

        private Map<AssetType, Map<String, List<Path>>> createTypeToBasenameToCacheFileMap(Options options) {
            log.debug("Generating Tabletop Simulator cache");
            return Stream.of(AssetType.values()).collect(
                    Collectors.toUnmodifiableMap(
                            type -> type,
                            type -> getBasenameToCacheFileMap(options.getCacheDir(type))
                    ));
        }

        private Map<String, List<Path>> getBasenameToCacheFileMap(Path cacheDir) {
            if (!Files.isDirectory(cacheDir)) {
                return Map.of();
            }

            try (Stream<Path> cacheFiles = Files.find(cacheDir, 1,
                    (path, basicFileAttributes) -> basicFileAttributes.isRegularFile()
            )) {
                return cacheFiles.collect(
                        Collectors.groupingBy(TtsFileUtils::getBaseName)
                );
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

    }

}
