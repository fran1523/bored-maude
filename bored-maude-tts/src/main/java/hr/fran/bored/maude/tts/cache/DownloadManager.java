/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.cache;

import hr.fran.bored.maude.tts.Configuration;
import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.AssetType;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.net.HttpURLConnection.HTTP_OK;

public class DownloadManager {

    private static final Configuration CONFIG = Configuration.instance();

    private static final Tika TIKA = new Tika();

    private static final Logger log = LoggerFactory.getLogger(DownloadManager.class);

    private final ExecutorService executorService = Executors.newFixedThreadPool(CONFIG.getDownloadThreadPoolSize());

    private final Collection<DownloadListener> downloadListeners = new ArrayList<>();

    private final ModManager modManager;

    public DownloadManager(ModManager modManager) {
        this.modManager = modManager;
    }

    public void addDownloadListener(DownloadListener listener) {
        this.downloadListeners.add(listener);
    }

    public void removeDownloadListener(DownloadListener listener) {
        this.downloadListeners.remove(listener);
    }

    public Mod downloadAssets(Mod mod, Options options) {
        log.info("Downloading assets for {}", mod);

        List<Callable<Asset>> assetDownloadTasks = mod.getAssets().stream()
                .map(asset -> (Callable<Asset>) () -> downloadIfRequired(asset, options))
                .toList();
        try {
            List<Asset> assetsAfterDownload = new ArrayList<>(mod.getAssets().size());
            for (Future<Asset> assetFuture : this.executorService.invokeAll(assetDownloadTasks)) {
                Asset asset = assetFuture.get();
                assetsAfterDownload.add(asset);
            }
            assetsAfterDownload.sort(Asset::compareTo);
            Mod modAfterDownload = mod.withAssets(assetsAfterDownload);

            long cachedCount = modAfterDownload.getCachedAssetsCount();
            long downloadedCount = cachedCount - mod.getCachedAssetsCount();
            long notCachedCount = mod.getAssets().size() - cachedCount;
            log.info("Download for {} completed, {} new files downloaded, {} downloads failed",
                    mod, downloadedCount, notCachedCount);

            return modAfterDownload;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.warn("Download for {} interrupted", mod);
            return this.modManager.getMod(mod.getModInfo(), options);
        } catch (ExecutionException e) {
            throw new TtsException("Download task error", e);
        }
    }

    private Asset downloadIfRequired(Asset asset, Options options) {
        if (asset.isCached() && !options.isOverwriteCacheFiles()) {
            return asset;
        }

        try {
            return downloadAndNotifyListeners(asset, options);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.warn("Download interrupted for {}", asset, e);
            return failDownloadAndNotifyListeners(asset, e);
        } catch (Exception e) {
            log.warn("Download failed for {}", asset, e);
            return failDownloadAndNotifyListeners(asset, e);
        }
    }

    private Asset downloadAndNotifyListeners(Asset asset, Options options) throws InterruptedException, IOException {
        Asset assetAfterDownload = download(asset, options);
        this.downloadListeners.forEach(listener -> listener.downloadCompleted(assetAfterDownload));
        return assetAfterDownload;
    }

    private Asset download(Asset asset, Options options) throws InterruptedException, IOException {
        Path tempFile = null;
        try {
            Path cacheDir = options.getCacheDir(asset.getType());
            tempFile = TtsFileUtils.createTempFile(cacheDir);
            downloadUrlContentToFile(options.getHttpClient(), asset.getUrl(), tempFile);

            Path cacheFile = getCacheFile(asset, tempFile, options);
            TtsFileUtils.move(tempFile, cacheFile);

            log.debug("{} saved to '{}'", asset, cacheFile);
            return asset.withCacheFile(cacheFile);
        } finally {
            TtsFileUtils.deleteIfExists(tempFile);
        }
    }

    private void downloadUrlContentToFile(HttpClient httpClient, String url, Path file)
            throws InterruptedException, IOException {
        URI uri = URI.create(url);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(uri)
                .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0")
                .build();
        HttpResponse<Path> response = httpClient.send(request, BodyHandlers.ofFile(file));

        if (response.statusCode() != HTTP_OK) {
            throw new TtsException("HTTP %s error".formatted(response.statusCode()));
        }
        if (Files.size(response.body()) == 0) {
            throw new TtsException("File is empty");
        }
    }

    private Path getCacheFile(Asset asset, Path tempFile, Options options) throws IOException {
        String baseName = TtsFileUtils.getCacheFileBaseName(asset.getUrl());
        String extension = getFileExtension(asset.getType(), tempFile);
        String cacheFilename = baseName + "." + extension;

        return options.getCacheDir(asset.getType())
                .resolve(cacheFilename);
    }

    private String getFileExtension(AssetType assetType, Path tempFile) throws IOException {
        String mimeType = TIKA.detect(tempFile);
        return CONFIG.getFileExtensionForMimeType(assetType, mimeType).orElseThrow(() ->
                new TtsException("%s is not a valid content type for %s".formatted(mimeType, assetType)));
    }

    private Asset failDownloadAndNotifyListeners(Asset asset, Exception downloadError) {
        String errorMessage = Objects.requireNonNullElse(downloadError.getMessage(), "Download error");
        Asset failedAsset = asset.withError(errorMessage);
        this.downloadListeners.forEach(listener -> listener.downloadFailed(failedAsset));
        return failedAsset;
    }

}
