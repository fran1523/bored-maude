/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.UnitTestBase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.UncheckedIOException;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class AssetTest extends UnitTestBase {

    @Test
    void getUrl_should_update_json_url_when_asset_has_old_steam_cloud_url() {
        assertThat(OLD_STEAM_CLOUD_ASSET.getUrl()).isEqualTo(NEW_STEAM_CLOUD_URL);
    }

    @Test
    void getUrl_should_return_json_url_when_asset_does_not_have_old_steam_cloud_url() {
        assertThat(IMAGE1.getUrl()).isEqualTo(IMAGE1.getJsonUrl());
    }

    @Test
    void hasError_should_return_false_when_asset_has_no_error() {
        assertThat(IMAGE1.hasError()).isFalse();
    }

    @Test
    void hasError_should_return_true_when_asset_has_error() {
        Asset assetWithError = IMAGE1.withError("Error");

        assertThat(assetWithError.hasError()).isTrue();
    }

    @Test
    void getChecksum_should_return_null_when_asset_is_not_on_steam_cloud_and_is_not_cached() {
        assertThat(IMAGE1.getChecksum()).isNull();
    }

    @ParameterizedTest
    @ValueSource(strings = {OLD_STEAM_CLOUD_URL, NEW_STEAM_CLOUD_URL})
    void getChecksum_should_return_checksum_from_url_when_asset_is_on_steam_cloud_and_is_cached(String url) {
        Asset steamCloudAsset = cached(IMAGE1.withJsonUrl(url));

        assertThat(steamCloudAsset.getChecksum()).isEqualTo(STEAM_CLOUD_SHA1);
    }

    @Test
    void getChecksum_should_return_checksum_from_file_when_asset_is_not_on_steam_cloud_and_is_cached() {
        Asset cachedAsset = cached(IMAGE1);

        assertThat(cachedAsset.getChecksum()).isEqualTo(IMAGE1_SHA1);
    }

    @Test
    void getChecksum_should_throw_exception_when_cache_file_does_not_exist() {
        Asset asset = IMAGE1.withCacheFile(Path.of("file-does-not-exist"));

        assertThatThrownBy(asset::getChecksum).isExactlyInstanceOf(UncheckedIOException.class);
    }

}
