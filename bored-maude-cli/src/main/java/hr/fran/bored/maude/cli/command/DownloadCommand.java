/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static picocli.CommandLine.Command;

@Command(name = "download",
        description = "Downloads mod assets into Tabletop Simulator cache",
        mixinStandardHelpOptions = true)
public class DownloadCommand extends AbstractDownloadCommand {

    public DownloadCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    public void run() {
        Options options = getOptions();
        ModFilter modFilter = getModFilter();

        Collection<ModInfo> modInfos = getSortedModInfos(modFilter, options);
        downloadMods(modInfos, options);
    }

    private void downloadMods(Collection<ModInfo> modInfos, Options options) {
        if (modInfos.isEmpty()) {
            getOut().printf("No mods found%s%n", getFilterSuffix());
            return;
        }

        long modsWithErrors = modInfos.stream()
                .map(modInfo -> downloadMod(modInfo, options))
                .filter(hasErrors -> hasErrors)
                .count();

        printSummary(modInfos.size(), modsWithErrors);
    }

    private boolean downloadMod(ModInfo modInfo, Options options) {
        Mod mod = this.tabletopSimulator.getMod(modInfo, options);
        Mod afterDownload = downloadAssets(mod, options);

        boolean modHasErrors = printModSummary(mod, afterDownload);
        getOut().println();

        return modHasErrors;
    }

    private Mod downloadAssets(Mod mod, Options options) {
        printTitle(getDisplayName(mod));

        this.tabletopSimulator.addDownloadListener(listener);
        Mod afterDownload = this.tabletopSimulator.downloadAssets(mod, options);
        this.tabletopSimulator.removeDownloadListener(listener);

        return afterDownload;
    }

    private boolean printModSummary(Mod beforeDownload, Mod afterDownload) {
        Collection<Asset> beforeDownloadAssets = Set.copyOf(beforeDownload.getAssets());
        Map<Boolean, Long> hasErrorsToAssetCount = afterDownload.getAssets().stream()
                .filter(asset -> !beforeDownloadAssets.contains(asset))
                .collect(Collectors.partitioningBy(
                        Asset::hasError,
                        Collectors.counting())
                );

        long downloads = hasErrorsToAssetCount.getOrDefault(false, 0L);
        long errors = hasErrorsToAssetCount.getOrDefault(true, 0L);
        getOut().printf("Download finished, %s new assets downloaded, %s errors%n", downloads, errors);

        return errors > 0;
    }

    private void printSummary(int modCount, long modsWithErrors) {
        String errorsText = modsWithErrors > 0
                ? ", %s mods have errors".formatted(modsWithErrors)
                : "";
        getOut().printf("%s mods downloaded%s%n", modCount, errorsText);
    }

}
