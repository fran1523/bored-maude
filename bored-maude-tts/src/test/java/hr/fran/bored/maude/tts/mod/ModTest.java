/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.UnitTestBase;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;

import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static hr.fran.bored.maude.tts.mod.AssetType.PDF;
import static org.assertj.core.api.Assertions.assertThat;

class ModTest extends UnitTestBase {

    private static final Asset IMAGE1_CACHED = IMAGE1.withCacheFile(Path.of("image1.jpg"));

    private static final Asset IMAGE1_ERROR = IMAGE1.withError("Error");

    private final Mod sut = CHESS_MOD.withAssets(AUDIO1, IMAGE1);

    @Test
    void getAssets_should_return_empty_when_mod_contains_no_matching_assets() {
        assertThat(this.sut.getAssets(PDF)).isEmpty();
    }

    @Test
    void getAssets_should_return_assets_of_type_when_mod_contains_matching_assets() {
        assertThat(this.sut.getAssets(IMAGE)).singleElement().isEqualTo(IMAGE1);
    }

    @Test
    void getAssetsWithError_should_return_empty_when_mod_contains_no_errors() {
        assertThat(this.sut.getAssetsWithError()).isEmpty();
    }

    @Test
    void getAssetsWithError_should_return_assets_when_mod_contains_errors() {
        Mod mod = CHESS_MOD.withAssets(AUDIO1, IMAGE1_ERROR);

        assertThat(mod.getAssetsWithError()).singleElement().isEqualTo(IMAGE1_ERROR);
    }

    @Test
    void getCachedAssetsCount_should_return_number_of_cached_assets() {
        Mod mod = CHESS_MOD.withAssets(AUDIO1, IMAGE1_CACHED);

        assertThat(mod.getCachedAssetsCount()).isOne();
    }

    @Test
    void hasUncachedNonAudioAssets_should_return_true_when_mod_contains_matching_assets() {
        assertThat(this.sut.hasUncachedNonAudioAssets()).isTrue();
    }

    @Test
    void hasUncachedNonAudioAssets_should_return_false_when_mod_contains_no_matching_assets() {
        Mod mod = CHESS_MOD.withAssets(List.of(AUDIO1, IMAGE1_CACHED));

        assertThat(mod.hasUncachedNonAudioAssets()).isFalse();
    }

}
