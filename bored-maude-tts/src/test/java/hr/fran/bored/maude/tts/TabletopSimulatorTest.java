/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import hr.fran.bored.maude.tts.backup.BackupFile;
import hr.fran.bored.maude.tts.backup.BackupManager;
import hr.fran.bored.maude.tts.backup.RestoreManager;
import hr.fran.bored.maude.tts.cache.Cleanup;
import hr.fran.bored.maude.tts.cache.CleanupManager;
import hr.fran.bored.maude.tts.cache.DownloadListener;
import hr.fran.bored.maude.tts.cache.DownloadManager;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Path;
import java.util.Collection;

import static hr.fran.bored.maude.tts.ZipAssert.assertThatZip;
import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

@ExtendWith(MockitoExtension.class)
class TabletopSimulatorTest extends UnitTestBase {

    @Mock
    private DownloadListener listener;

    private ModManager modManager;

    private DownloadManager downloadManager;

    private final TabletopSimulator sut = getTabletopSimulator();

    private TabletopSimulator getTabletopSimulator() {
        ModInfoManager modInfoManager = new ModInfoManager();
        this.modManager = spy(new ModManager());
        CleanupManager cleanupManager = new CleanupManager(modInfoManager, this.modManager);
        this.downloadManager = spy(new DownloadManager(this.modManager));
        BackupManager backupManager = new BackupManager();
        RestoreManager restoreManager = new RestoreManager(modInfoManager, this.modManager);

        return new TabletopSimulator(
                modInfoManager,
                this.modManager,
                cleanupManager,
                this.downloadManager,
                backupManager,
                restoreManager
        );
    }

    @Test
    void getModInfos_should_return_results_when_there_are_workshop_mods() {
        Path modFile = givenWorkshopFile(CHESS_MOD_FILE);

        Collection<ModInfo> result = this.sut.getModInfos(ModFilter.MATCH_ALL, testOptions);

        assertThat(result).singleElement().isEqualTo(CHESS_MOD_INFO.withFile(modFile));
        verify(this.modManager, never()).invalidateTabletopSimulatorCache();
    }

    @Test
    void getMod_should_return_result_when_mod_file_is_valid() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_MOD_INFO);

        Mod mod = this.sut.getMod(modInfo, testOptions);

        assertThat(mod).isEqualTo(CHESS_MOD.withModInfo(modInfo));
        verify(this.modManager, never()).invalidateTabletopSimulatorCache();
    }

    @Test
    void cleanup() {
        Path orphanFile = givenCacheFileForAsset(PDF1);

        Cleanup result = this.sut.cleanup(testOptions);

        assertThat(result.deletedFiles()).containsOnlyKeys(orphanFile);
        verify(this.modManager, times(2)).invalidateTabletopSimulatorCache();
    }

    @Test
    void downloadAssets_should_download_uncached_assets() {
        whenSendAssetUrlThenReturnFile(IMAGE1);

        Mod result = this.sut.downloadAssets(CHESS_MOD.withAssets(IMAGE1), testOptions);

        Path image1CacheFile = getExpectedCacheFile(IMAGE, IMAGE1_CACHE_FILENAME);
        assertThat(result.getAssets()).singleElement().isEqualTo(IMAGE1.withCacheFile(image1CacheFile));
        verify(this.modManager, times(1)).invalidateTabletopSimulatorCache();
    }

    @Test
    void downloadAssets_should_propagate_download_listeners() {
        whenSendAssetUrlThenReturnFile(IMAGE1);

        this.sut.addDownloadListener(this.listener);
        this.sut.downloadAssets(CHESS_MOD.withAssets(IMAGE1), testOptions);

        Path image1CacheFile = getExpectedCacheFile(IMAGE, IMAGE1_CACHE_FILENAME);
        verify(this.listener).downloadCompleted(IMAGE1.withCacheFile(image1CacheFile));
    }

    @Test
    void downloadAssets_should_not_propagate_removed_download_listeners() {
        whenSendAssetUrlThenReturnFile(PDF1);

        this.sut.addDownloadListener(this.listener);
        this.sut.removeDownloadListener(this.listener);
        this.sut.downloadAssets(CHESS_MOD.withAssets(PDF1), testOptions);

        verifyNoInteractions(this.listener);
    }

    @Test
    void downloadAndCreateBackup_should_download_assets_before_creating_backup() {
        Mod mod = givenModWithFileAndAssets(CHESS_MOD, PDF1);

        whenSendAssetUrlThenReturnFile(PDF1);

        this.sut.downloadAndCreateBackup(mod, testOptions);

        verify(this.downloadManager).downloadAssets(mod, testOptions);
        verify(this.modManager, times(1)).invalidateTabletopSimulatorCache();
    }

    @Test
    void downloadAndCreateBackup_should_create_backup_archive() {
        Mod mod = givenModWithFileAndAssets(CHESS_MOD);

        BackupFile result = this.sut.downloadAndCreateBackup(mod, testOptions);

        assertThat(result.getFile()).isNotEmptyFile();
        verify(this.modManager, times(1)).invalidateTabletopSimulatorCache();
    }

    @Test
    void restoreBackup_should_restore_mod_from_backup_archive() {
        Path backupFile = getBackupFile(CHESS_BACKUP_FILE);

        this.sut.restoreBackup(backupFile, testOptions);

        assertThatZip(backupFile).wasExtractedTo(this.testOptions.getTtsDir());
        verify(this.modManager, times(1)).invalidateTabletopSimulatorCache();
    }

}
