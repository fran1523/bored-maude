# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.6.0]
### Added
- Convert old Steam Cloud URLs (http://cloud-3.steamusercontent.com/) to new URLs (https://steamusercontent-a.akamaihd.net/) when loading a mod
- Restore old Steam Cloud files from backup archives to new filenames
- Rename old Steam Cloud files on cleanup

## [1.5.0]
### Added
- Auto-detect Tabletop Simulator directory for Ubuntu Steam snap
- FUTURE: Add thumbnail to mods, for GUI use.
- FUTURE: Add type (**Workshop Mod** or **Save**) to mods, for GUI use.

### Changed
- Cache local Tabletop Simulator file list to improve performance.
- Do not add multiple errors to asset, fail validation on first error.
- Increase the number of parallel downloads to 10

### Removed
- Remove **dry run** option from all commands.
- Remove **audit** command.

### Fixed
- Fix tests failing on Windows because of newline mismatch.
- Orphan cleanup skips files used in mod saves.
- Fix failing downloads because of missing `User-Agent` header.

## [1.4.0] - 2024-05-02
### Added
- Add HTTP connection timeout option to backup.
- Print warning when backing up an incomplete mod.
- Print warning when restoring an incomplete mod.
- FUTURE: Calculate SHA-1 checksum for asset files, for upcoming download optimisation.

### Changed
- Upgrade project to Java 17.

### Fixed
- Verify HTTP status code is OK when checking for new version.
- Fix version update message.
- Check for empty files when downloading.
- Delete temp file on download error.

## [1.3.0] - 2023-04-14
### Added
- Support multiple wildcard patterns when restoring backups.

### Changed
- Create temporary files in the same directory as target files.
- Remove some dependencies to reduce application size.

## [1.2.0] - 2022-11-22
### Added
- Check for new version at startup.

### Changed
- Find orphan raw files unrelated to orphan assets.

### Fixed
- Match only `zip` and `ttsmod` files when restoring backups using wildcard pattern.
- Resolve backup file correctly when not using wildcard pattern.
- Check if table column widths exceed total table width. Fixes error _"Conversion = '-'"_.

## [1.1.0] - 2022-07-28
### Changed
- Remove 6 line limit for exception stacktrace logs.

### Fixed
- Skip mods that cannot be parsed when listing installed mods. Fixes error _"Cannot read mod JSON"_.
- Ignore unmapped fields when searching for asset URLs. Fixes error _"Cannot find asset type for Description"_.
- Read mod updated time as `String` instead of converting to `Date`, because date format varies.

## [1.0.1] - 2022-07-23
### Fixed
- Ignore `Description` field when searching for asset URLs. Fixes error _"Cannot find asset type for Description"_.

## [1.0.0] - 2022-07-23
### Added
- Support multiple mod filter parameters.

## [0.4.0] - 2022-05-13
### Changed
- Print only error message for unexpected errors, log full stacktrace.

## [0.3.1] - 2022-04-08
### Fixed
- Return empty list instead of error if mod directory does not exist.

### Security

- Update `jackson-databind` to `2.13.2.2` because of [CVE-2020-36518](https://nvd.nist.gov/vuln/detail/CVE-2020-36518).

## [0.3.0] - 2022-03-20
### Added
- Support localised asset URL format.

### Changed
- Generate mod list from available mod JSON files, instead of mod infos.

## [0.2.0] - 2022-03-10
### Added
- Print total size of deleted files after cleanup.
- Check assets for empty files.

### Removed
- Do not update mod infos when restoring backup, because new mods are auto-detected by Tabletop Simulator on startup.

### Fixed
- Update readme link.
- Read `json` mods only, ignore unsupported `cjc` files.

## [0.1.0] - 2022-02-22
### Added
- Mod operations:
  - **audit**: Inspect mod for invalid or unavailable assets.
  - **backup**: Create an archive containing all mod assets.
  - **cleanup**: Remove assets not used by any installed mod.
  - **download**: Download all mod assets.
  - **info**: Print detailed mod information.
  - **list**: List installed mods.
  - **restore**: Restore mod from a backup archive.
- Support workshop mods and saves for all operations.
- Restore [TTS Mod Backup](https://steamcommunity.com/app/286160/discussions/0/496880503078046116/) archives on Linux,
  macOS and Windows.

[Unreleased]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.6.0...master
[1.6.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.4.0...v1.5.0
[1.5.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.4.0...v1.5.0
[1.4.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.3.0...v1.4.0
[1.3.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.1.0...v1.2.0
[1.1.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/fran1523/bored-maude/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v0.4.0...v1.0.0
[0.4.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/fran1523/bored-maude/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/fran1523/bored-maude/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/fran1523/bored-maude/-/releases/v0.1.0
