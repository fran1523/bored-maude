/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.backup;

import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.UnitTestBase;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModInfo;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.time.Instant;

import static hr.fran.bored.maude.tts.ZipAssert.assertThatZip;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class BackupManagerTest extends UnitTestBase {

    private final BackupManager sut = new BackupManager();

    @Test
    void createBackup_should_create_backup_archive_when_all_non_audio_assets_are_cached() {
        Asset image1Cached = cached(IMAGE1);
        Mod mod = givenModWithFileAndAssets(CHESS_MOD, AUDIO1, image1Cached);

        BackupFile result = this.sut.createBackup(mod, testOptions);

        Path expectedFile = this.testOptions.getBackupDir().resolve(CHESS_BACKUP_FILE);
        assertThat(result.getMod()).isEqualTo(mod);
        assertThat(result.isModIncomplete()).isFalse();
        assertThatZip(result.getFile()).isEqualTo(expectedFile)
                .containsOnlyFiles(mod.getModInfo().getFile(), image1Cached.getCacheFile());
    }

    @Test
    void createBackup_should_include_thumbnail_when_there_is_a_mod_thumbnail() {
        Mod mod = givenModWithFileAndAssets(CHESS_MOD);
        Path modThumbnail = givenWorkshopFile(CHESS_THUMBNAIL);

        BackupFile result = this.sut.createBackup(mod, testOptions);

        assertThatZip(result.getFile()).containsOnlyFiles(mod.getModInfo().getFile(), modThumbnail);
    }

    @Test
    void createBackup_should_create_backup_archive_when_cache_file_is_used_multiple_times() {
        Path imageCacheFile = givenCacheFileForAsset(IMAGE1);
        Asset image1Cached = IMAGE1.withCacheFile(imageCacheFile);
        Asset image2cached = IMAGE2.withCacheFile(imageCacheFile);
        Mod mod = givenModWithFileAndAssets(CHESS_MOD, image1Cached, image2cached);

        BackupFile result = this.sut.createBackup(mod, testOptions);

        assertThatZip(result.getFile()).containsOnlyFiles(mod.getModInfo().getFile(), imageCacheFile);
    }

    @Test
    void createBackup_should_throw_exception_when_some_non_audio_assets_are_not_cached() {
        Mod mod = givenModWithFileAndAssets(CHESS_MOD, IMAGE1);

        assertThatThrownBy(() -> this.sut.createBackup(mod, testOptions))
                .isExactlyInstanceOf(TtsException.class)
                .hasMessage("Some assets are not downloaded");
    }

    @Test
    void createBackup_should_backup_mod_with_uncached_assets_when_backup_incomplete_mods_is_enabled() {
        Mod mod = givenModWithFileAndAssets(CHESS_MOD, IMAGE1);

        BackupFile result = this.sut.createBackup(mod, this.testOptions.withBackupIncompleteMods(true));

        assertThat(result.isModIncomplete()).isTrue();
        assertThatZip(result.getFile()).containsOnlyFiles(mod.getModInfo().getFile());
    }

    @Test
    void createBackup_should_overwrite_backup_archive_when_file_exists() {
        Path backupFile = givenBackupFile(CHESS_BACKUP_FILE);
        Instant oneMinuteAgo = setLastModifiedTimeOneMinuteAgo(backupFile);
        Mod mod = givenModWithFileAndAssets(CHESS_MOD);

        BackupFile result = this.sut.createBackup(mod, testOptions);

        assertThat(result.getFile()).isEqualTo(backupFile);
        assertThat(getLastModifiedTime(backupFile)).isAfter(oneMinuteAgo);
    }

    @Test
    void createBackup_should_remove_illegal_characters_when_generating_backup_filename() {
        ModInfo modInfo = givenModInfoWithFile(CHESS_MOD_INFO).withName("a:b/c*d" + "x".repeat(1000));
        Mod mod = CHESS_MOD.withModInfo(modInfo).withAssets();

        BackupFile result = this.sut.createBackup(mod, testOptions);

        String filename = result.getFile().getFileName().toString();
        assertThat(filename).matches("a_b_c_dx+ \\(12345678\\)\\.zip").hasSize(255);
    }

    @Test
    void createBackup_should_backup_mod_save_when_all_assets_are_cached() {
        Asset image1Cached = cached(IMAGE1);
        Mod mod = givenModWithFileAndAssets(CHESS_SAVE, AUDIO1, image1Cached);
        Path modThumbnail = givenSavesFile("TS_Save_1.png");

        BackupFile result = this.sut.createBackup(mod, testOptions);

        Path expected = this.testOptions.getBackupDir().resolve("Chess (TS_Save_1).zip");
        assertThatZip(result.getFile()).isEqualTo(expected)
                .containsOnlyFiles(mod.getModInfo().getFile(), modThumbnail, image1Cached.getCacheFile());
    }

}
