/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import com.google.gson.annotations.SerializedName;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import org.apache.commons.io.FilenameUtils;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.immutables.gson.Gson;
import org.immutables.value.Value.Default;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Optional;

import static org.immutables.value.Value.Immutable;
import static org.immutables.value.Value.Lazy;

@Immutable
@Gson.TypeAdapters
public abstract class ModInfo implements WithModInfo, Comparable<ModInfo> {

    private static final Comparator<ModInfo> COMPARATOR =
            Comparator.comparing(ModInfo::getName).thenComparing(ModInfo::getId);

    @SerializedName("Date")
    public abstract String getDate();

    @SerializedName("SaveName")
    public abstract String getName();

    @SerializedName("VersionNumber")
    public abstract String getVersion();

    @Default
    @Gson.Ignore
    public Path getFile() {
        return Path.of("");
    }

    @Default
    @Gson.Ignore
    public ModType getType() {
        return ModType.MOD;
    }

    @Lazy
    @Gson.Ignore
    public String getId() {
        return FilenameUtils.getBaseName(getFile().toString());
    }

    @Lazy
    public Optional<Path> getThumbnail() {
        Path file = getFile();
        String baseName = TtsFileUtils.getBaseName(file);
        String thumbnailFilename = baseName + ".png";
        Path thumbnailFile = getFile().resolveSibling(thumbnailFilename);

        if (Files.isRegularFile(thumbnailFile)) {
            return Optional.of(thumbnailFile);
        }
        return Optional.empty();
    }

    @Override
    public int compareTo(@NonNull ModInfo o) {
        return COMPARATOR.compare(this, o);
    }

    @Override
    public String toString() {
        String name = getName();
        return name.contains(" ")
                ? "'%s' [%s]".formatted(name, getId())
                : "%s [%s]".formatted(name, getId());
    }

    public static class Builder extends ImmutableModInfo.Builder {
    }

}
