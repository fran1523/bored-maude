/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import hr.fran.bored.maude.tts.backup.BackupFile;
import hr.fran.bored.maude.tts.backup.BackupManager;
import hr.fran.bored.maude.tts.backup.RestoreManager;
import hr.fran.bored.maude.tts.cache.Cleanup;
import hr.fran.bored.maude.tts.cache.CleanupManager;
import hr.fran.bored.maude.tts.cache.DownloadListener;
import hr.fran.bored.maude.tts.cache.DownloadManager;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

public class TabletopSimulator {

    private final ModInfoManager modInfoManager;
    private final ModManager modManager;
    private final CleanupManager cleanupManager;
    private final DownloadManager downloadManager;
    private final BackupManager backupManager;
    private final RestoreManager restoreManager;

    private final Collection<DownloadListener> downloadListeners = new ArrayList<>();

    public TabletopSimulator(
            ModInfoManager modInfoManager,
            ModManager modManager,
            CleanupManager cleanupManager,
            DownloadManager downloadManager,
            BackupManager backupManager,
            RestoreManager restoreManager
    ) {
        this.modInfoManager = modInfoManager;
        this.modManager = modManager;
        this.cleanupManager = cleanupManager;
        this.downloadManager = downloadManager;
        this.backupManager = backupManager;
        this.restoreManager = restoreManager;
    }

    public void addDownloadListener(DownloadListener downloadListener) {
        this.downloadListeners.add(downloadListener);
    }

    public void removeDownloadListener(DownloadListener downloadListener) {
        this.downloadListeners.remove(downloadListener);
    }

    public Collection<ModInfo> getModInfos(ModFilter filter, Options options) {
        return this.modInfoManager.getValidModInfos(filter, options);
    }

    public Mod getMod(ModInfo modInfo, Options options) {
        return this.modManager.getMod(modInfo, options);
    }

    public Cleanup cleanup(Options options) {
        return withCacheInvalidation(() ->
                this.cleanupManager.cleanup(options)
        );
    }

    public Mod downloadAssets(Mod mod, Options options) {
        this.downloadListeners.forEach(this.downloadManager::addDownloadListener);
        Mod afterDownload = withCacheInvalidation(() ->
                this.downloadManager.downloadAssets(mod, options)
        );
        this.downloadListeners.forEach(this.downloadManager::removeDownloadListener);

        return afterDownload;
    }

    public BackupFile downloadAndCreateBackup(Mod mod, Options options) {
        Mod afterDownload = downloadAssets(mod, options);
        return this.backupManager.createBackup(afterDownload, options);
    }

    public Mod restoreBackup(Path backupFile, Options options) {
        return withCacheInvalidation(() ->
                this.restoreManager.restoreBackup(backupFile, options)
        );
    }

    private <T> T withCacheInvalidation(Supplier<T> supplier) {
        T result = supplier.get();
        this.modManager.invalidateTabletopSimulatorCache();
        return result;
    }

}
