/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.backup.BackupFile;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

@Command(name = "backup",
        description = "Downloads missing assets and creates a mod backup archive",
        mixinStandardHelpOptions = true)
public class BackupCommand extends AbstractDownloadCommand {

    private static final Logger log = LoggerFactory.getLogger(BackupCommand.class);

    @Option(
            names = {"-b", "--backup-dir"},
            description = "Backup files will be saved to this directory. Default is current directory",
            defaultValue = ""
    )
    protected Path backupDir;

    @Option(
            names = {"-i", "--backup-incomplete"},
            description = """
                    Create backup even if some non-audio assets are not downloaded. \
                    Missing audio assets are always ignored"""
    )
    protected boolean backupIncompleteMods;

    public BackupCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    public void run() {
        Options options = getOptions();
        ModFilter modFilter = getModFilter();

        List<ModInfo> modInfos = getSortedModInfos(modFilter, options);
        backupMods(modInfos, options);
    }

    @Override
    protected Options.Builder getOptionsBuilder() {
        return super.getOptionsBuilder()
                .backupDir(this.backupDir)
                .backupIncompleteMods(this.backupIncompleteMods);
    }

    private void backupMods(Collection<ModInfo> modInfos, Options options) {
        if (modInfos.isEmpty()) {
            getOut().printf("No mods found%s%n", getFilterSuffix());
            return;
        }

        Collection<BackupFile> backupSuccesses = modInfos.stream()
                .map(modInfo -> this.tabletopSimulator.getMod(modInfo, options))
                .flatMap(mod -> backupMod(mod, options).stream())
                .toList();

        long successCount = backupSuccesses.size();
        long incompleteWarningCount = backupSuccesses.stream()
                .filter(BackupFile::isModIncomplete)
                .count();

        printSummary(successCount, modInfos.size(), incompleteWarningCount);
    }

    private Optional<BackupFile> backupMod(Mod mod, Options options) {
        try {
            printStart(mod);
            this.tabletopSimulator.addDownloadListener(listener);
            BackupFile backupFile = this.tabletopSimulator.downloadAndCreateBackup(mod, options);
            this.tabletopSimulator.removeDownloadListener(listener);
            printSuccess(backupFile);
            return Optional.of(backupFile);
        } catch (Exception e) {
            log.warn("{} backup failed", getDisplayName(mod), e);
            printFailure(e);
            return Optional.empty();
        }
    }

    private void printStart(Mod mod) {
        getOut().printf("%s backup started ... ", getDisplayName(mod));
    }

    private void printSuccess(BackupFile backupFile) {
        getOut().printf("saved to '%s'%s%n",
                backupFile.getFile(),
                getModIncompleteSuffix(backupFile.isModIncomplete())
        );
    }

    private void printFailure(Exception e) {
        getOut().printf("failed: %s%n", e.getMessage());
    }

    private void printSummary(long successCount, int modCount, long incompleteWarningCount) {
        long failureCount = modCount - successCount;
        getOut().printf("%n%s/%s mods backed up", successCount, modCount);
        if (incompleteWarningCount > 0) {
            getOut().printf(", %s backups incomplete", incompleteWarningCount);
        }
        if (failureCount > 0) {
            getOut().printf(", %s backups failed", failureCount);
        }
        getOut().println();
    }

}
