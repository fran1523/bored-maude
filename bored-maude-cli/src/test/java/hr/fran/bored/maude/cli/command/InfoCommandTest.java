/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.mod.ModFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.util.List;

import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class InfoCommandTest extends CommandTestBase {

    private CommandLine sut;

    @BeforeEach
    protected void setup() {
        this.sut = getTestCommandLine(InfoCommand.class);
    }

    @Test
    void should_print_mod_info_when_using_default_parameters() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHECKERS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHECKERS_INFO), any(Options.class))).thenReturn(CHECKERS);

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Checkers
                ========
                File   : %s
                Id     : 23456789
                Updated: 10/11/2019 10:19:51 PM
                Version: v12.3.4
                Assets : 0
                  ASSETBUNDLE: 0
                  AUDIO      : 0
                  IMAGE      : 0
                  MODEL      : 0
                  PDF        : 0
                Errors : 0
                
                1 mods found
                """.formatted(CHECKERS_INFO.getFile())
        );
    }

    @Test
    void should_print_mod_info_when_displaying_saves() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_SAVE_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_SAVE_INFO), argThat(options -> options.getModType() == SAVE)))
                .thenReturn(CHESS_SAVE);

        this.sut.execute("--saves");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess
                =====
                File   : %s
                Id     : TS_Save_1
                Updated: 2/9/2022 11:27:56 AM
                Version: v12.3.4
                Assets : 0
                  ASSETBUNDLE: 0
                  AUDIO      : 0
                  IMAGE      : 0
                  MODEL      : 0
                  PDF        : 0
                Errors : 0
                
                1 mods found
                """.formatted(CHESS_SAVE_INFO.getFile())
        );
    }

    @Test
    void should_print_matching_mod_info_when_filter_is_provided() {
        when(this.tabletopSimulator.getModInfos(eq(new ModFilter(CHECKERS_INFO.getId())), any(Options.class)))
                .thenReturn(List.of(CHECKERS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHECKERS_INFO), any(Options.class))).thenReturn(CHECKERS);

        this.sut.execute("23456789");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Checkers
                ========
                File   : %s
                Id     : 23456789
                Updated: 10/11/2019 10:19:51 PM
                Version: v12.3.4
                Assets : 0
                  ASSETBUNDLE: 0
                  AUDIO      : 0
                  IMAGE      : 0
                  MODEL      : 0
                  PDF        : 0
                Errors : 0
                
                1 mods found
                """.formatted(CHECKERS_INFO.getFile())
        );
    }

    @Test
    void should_print_mod_info_when_mod_has_assets_with_errors() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess
                =====
                File   : %s
                Id     : 12345678
                Updated: 9/4/2021 4:31:03 AM
                Version: v12.3.4
                Assets : 2/4 cached
                  ASSETBUNDLE: 0/1 cached
                  AUDIO      : 0/1 cached
                  IMAGE      : 1/1 cached
                  MODEL      : 0
                  PDF        : 1/1 cached
                Errors : 2
                  http://www.test.com/asset/1
                    Type : ASSETBUNDLE
                    Error: 404 Not Found
                  https://www.test.com/notapdf/1
                    Type : PDF
                    File : %s
                    Error: Content type image/jpeg is not valid for PDF
                
                1 mods found
                """.formatted(CHESS_INFO.getFile(), PDF1_CACHED_ERROR.getCacheFile())
        );
    }

    @Test
    void should_print_mod_infos_when_multiple_results_are_returned() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO, CHECKERS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.getMod(eq(CHECKERS_INFO), any(Options.class))).thenReturn(CHECKERS);

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Checkers
                ========
                File   : %s
                Id     : 23456789
                Updated: 10/11/2019 10:19:51 PM
                Version: v12.3.4
                Assets : 0
                  ASSETBUNDLE: 0
                  AUDIO      : 0
                  IMAGE      : 0
                  MODEL      : 0
                  PDF        : 0
                Errors : 0
                
                Chess
                =====
                File   : %s
                Id     : 12345678
                Updated: 9/4/2021 4:31:03 AM
                Version: v12.3.4
                Assets : 2/4 cached
                  ASSETBUNDLE: 0/1 cached
                  AUDIO      : 0/1 cached
                  IMAGE      : 1/1 cached
                  MODEL      : 0
                  PDF        : 1/1 cached
                Errors : 2
                  http://www.test.com/asset/1
                    Type : ASSETBUNDLE
                    Error: 404 Not Found
                  https://www.test.com/notapdf/1
                    Type : PDF
                    File : %s
                    Error: Content type image/jpeg is not valid for PDF
                
                2 mods found
                """.formatted(CHECKERS_INFO.getFile(), CHESS_INFO.getFile(), PDF1_CACHED_ERROR.getCacheFile())
        );
    }

    @Test
    void should_print_warning_when_no_mods_are_found() {
        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                No mods found
                """);
    }

}
