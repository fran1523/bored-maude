/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.AssetType;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;

import java.util.Collection;
import java.util.List;

import static picocli.CommandLine.Command;

@Command(name = "info",
        description = "Displays detailed info for installed mods",
        mixinStandardHelpOptions = true)
public class InfoCommand extends AbstractListCommand {

    public InfoCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    public void run() {
        Options options = getOptions();
        ModFilter modFilter = getModFilter();
        List<ModInfo> modInfos = getSortedModInfos(modFilter, options);

        printInfos(modInfos, options);
    }

    private void printInfos(List<ModInfo> modInfos, Options options) {
        if (modInfos.isEmpty()) {
            getOut().printf("No mods found%s%n", getFilterSuffix());
            return;
        }

        for (ModInfo modInfo : modInfos) {
            Mod mod = getMod(modInfo, options);
            printInfo(mod);
            getOut().println();
        }

        printSummary(modInfos.size());
    }

    protected Mod getMod(ModInfo modInfo, Options options) {
        return this.tabletopSimulator.getMod(modInfo, options);
    }

    private void printInfo(Mod mod) {
        printDetails(mod.getModInfo());
        printAssets(mod);
        printErrors(mod);
    }

    private void printDetails(ModInfo modInfo) {
        printTitle(modInfo.getName());
        printNamedValue("File", 7, modInfo.getFile());
        printNamedValue("Id", 7, modInfo.getId());
        printNamedValue("Updated", 7, modInfo.getDate());
        printNamedValue("Version", 7, modInfo.getVersion());
    }

    private void printAssets(Mod mod) {
        String assetCounts = getAssetCounts(mod.getAssets());
        printNamedValue("Assets", 7, assetCounts);
        for (AssetType type : AssetType.values()) {
            printAssetsCountsForType(type, mod.getAssets(type));
        }
    }

    private String getAssetCounts(Collection<Asset> assets) {
        if (assets.isEmpty()) {
            return "0";
        }

        int count = assets.size();
        long cachedCount = cachedCount(assets);
        return "%s/%s cached".formatted(cachedCount, count);
    }

    private long cachedCount(Collection<Asset> assets) {
        return assets.stream()
                .filter(Asset::isCached)
                .count();
    }

    private void printAssetsCountsForType(AssetType type, Collection<Asset> assets) {
        String assetCounts = getAssetCounts(assets);
        printNamedValue(1, type, 11, assetCounts);
    }

    private void printErrors(Mod mod) {
        Collection<Asset> assetsWithErrors = getSortedAssetsWithErrors(mod);
        printNamedValue("Errors", 7, assetsWithErrors.size());

        if (assetsWithErrors.isEmpty()) {
            return;
        }

        for (Asset asset : assetsWithErrors) {
            printAsset(1, asset);
        }
    }

    private Collection<Asset> getSortedAssetsWithErrors(Mod mod) {
        return mod.getAssetsWithError().stream()
                .sorted()
                .toList();
    }

    private void printSummary(int modCount) {
        getOut().printf("%s mods found%n", modCount);
    }

}
