/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.update;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vdurmont.semver4j.Semver;
import hr.fran.bored.maude.tts.Configuration;
import picocli.CommandLine.IVersionProvider;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.vdurmont.semver4j.Semver.SemverType.NPM;
import static java.net.HttpURLConnection.HTTP_OK;

public class VersionUpdateChecker {

    private static final Configuration CONFIG = Configuration.instance();

    private static final Gson GSON = new GsonBuilder()
            .disableJdkUnsafe()
            .registerTypeAdapterFactory(new GsonAdaptersGitLabRelease())
            .create();

    private final IVersionProvider versionProvider;

    private final HttpClient httpClient;

    public VersionUpdateChecker(IVersionProvider versionProvider, HttpClient httpClient) {
        this.versionProvider = versionProvider;
        this.httpClient = httpClient;
    }

    public Optional<VersionUpdate> checkForUpdate() throws Exception {
        Semver currentVersion = getCurrentVersion();
        Semver latestVersion = getLatestVersion();

        if (latestVersion.isGreaterThan(currentVersion)) {
            return versionUpdateOf(latestVersion);
        }

        return Optional.empty();
    }

    private Semver getCurrentVersion() throws Exception {
        return Stream.of(this.versionProvider.getVersion())
                .findFirst()
                .map(Semver::new)
                .orElseThrow(() -> new IllegalStateException("No version found"));
    }

    private Semver getLatestVersion() {
        List<GitLabRelease> releases = getGitLabReleases();
        return releases.stream()
                .map(GitLabRelease::getTagName)
                .map(tag -> new Semver(tag, NPM))
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("No releases returned by GitLab API"));
    }

    private List<GitLabRelease> getGitLabReleases() {
        try {
            HttpRequest request = getGitLabReleasesRequest();
            HttpResponse<InputStream> response = this.httpClient.send(request,
                    BodyHandlers.buffering(BodyHandlers.ofInputStream(), 8192)
            );

            if (response.statusCode() != HTTP_OK) {
                throw new IOException("HTTP %s error".formatted(response.statusCode()));
            }

            try (InputStream in = response.body();
                 InputStreamReader reader = new InputStreamReader(in)
            ) {
                return GSON.fromJson(reader, new TypeToken<>() {
                });
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private HttpRequest getGitLabReleasesRequest() {
        String gitlabProjectId = CONFIG.getGitlabProjectId();
        return HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://gitlab.com/api/v4/projects/%s/releases".formatted(gitlabProjectId)))
                .build();
    }

    private Optional<VersionUpdate> versionUpdateOf(Semver version) {
        String gitlabReleasesUrl = CONFIG.getGitlabReleasesUrl();
        return Optional.of(
                new VersionUpdate.Builder()
                        .version(version.getOriginalValue())
                        .url(gitlabReleasesUrl)
                        .build()
        );
    }

}
