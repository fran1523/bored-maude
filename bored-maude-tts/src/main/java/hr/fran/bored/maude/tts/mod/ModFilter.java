/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collection;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

public class ModFilter {

    public static final ModFilter MATCH_ALL = new ModFilter() {
        @Override
        public boolean matches(ModInfo modInfo) {
            return true;
        }
    };

    private final Set<String> idOrNameFilters;

    public ModFilter(Collection<String> idOrNameFilters) {
        this.idOrNameFilters = Set.copyOf(idOrNameFilters);
    }

    public ModFilter(String... idOrNameFilters) {
        this.idOrNameFilters = Set.of(idOrNameFilters);
    }

    public boolean matches(ModInfo modInfo) {
        return this.idOrNameFilters.stream().anyMatch(
                filter -> modInfo.getId().equalsIgnoreCase(filter)
                        || modInfo.getName().toLowerCase(Locale.ROOT).contains(filter.toLowerCase(Locale.ROOT)));
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModFilter modFilter = (ModFilter) o;
        return Objects.equals(this.idOrNameFilters, modFilter.idOrNameFilters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.idOrNameFilters);
    }
}
