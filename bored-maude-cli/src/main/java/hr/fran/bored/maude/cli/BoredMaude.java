/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli;

import hr.fran.bored.maude.cli.command.AbstractCommand;
import hr.fran.bored.maude.cli.command.BackupCommand;
import hr.fran.bored.maude.cli.command.CleanupCommand;
import hr.fran.bored.maude.cli.command.DownloadCommand;
import hr.fran.bored.maude.cli.command.InfoCommand;
import hr.fran.bored.maude.cli.command.ListCommand;
import hr.fran.bored.maude.cli.command.RestoreCommand;
import hr.fran.bored.maude.cli.update.VersionUpdate;
import hr.fran.bored.maude.cli.update.VersionUpdateChecker;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.backup.BackupManager;
import hr.fran.bored.maude.tts.backup.RestoreManager;
import hr.fran.bored.maude.tts.cache.CleanupManager;
import hr.fran.bored.maude.tts.cache.DownloadManager;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.assertj.core.util.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.io.PrintWriter;
import java.net.http.HttpClient;
import java.util.Optional;

import static java.net.http.HttpClient.Redirect.NORMAL;
import static picocli.CommandLine.Command;
import static picocli.CommandLine.IFactory;

@Command(
        name = "bored-maude",
        description = "Tabletop Simulator mod manager",
        versionProvider = VersionProvider.class,
        mixinStandardHelpOptions = true,
        subcommands = {
                BackupCommand.class,
                CleanupCommand.class,
                DownloadCommand.class,
                InfoCommand.class,
                ListCommand.class,
                RestoreCommand.class,
        }
)
public class BoredMaude {

    private static final Logger log = LoggerFactory.getLogger(BoredMaude.class);

    public static void main(String[] args) {
        BoredMaude boredMaude = new BoredMaude();
        int exitCode = boredMaude.execute(args);
        System.exit(exitCode);
    }

    protected int execute(String[] args) {
        CommandLine commandLine = createCommandLine();
        checkForUpdates(commandLine.getOut());
        return commandLine.execute(args);
    }

    private CommandLine createCommandLine() {
        TabletopSimulator tabletopSimulator = createTabletopSimulator();
        IFactory commandFactory = new CommandFactory(tabletopSimulator);
        return new CommandLine(this, commandFactory)
                .setExecutionExceptionHandler(new PrintMessageExceptionHandler());
    }

    @VisibleForTesting
    protected TabletopSimulator createTabletopSimulator() {
        ModManager modManager = new ModManager();
        ModInfoManager modInfoManager = new ModInfoManager();
        CleanupManager cleanupManager = new CleanupManager(modInfoManager, modManager);
        DownloadManager downloadManager = new DownloadManager(modManager);
        BackupManager backupManager = new BackupManager();
        RestoreManager restoreManager = new RestoreManager(modInfoManager, modManager);

        return new TabletopSimulator(modInfoManager, modManager, cleanupManager, downloadManager, backupManager,
                restoreManager);
    }

    private void checkForUpdates(PrintWriter out) {
        try {
            VersionUpdateChecker versionUpdateChecker = createVersionUpdateChecker();
            Optional<VersionUpdate> versionUpdate = versionUpdateChecker.checkForUpdate();
            versionUpdate.ifPresent(
                    update -> out.printf("New version %s is available at %s%n%n", update.getVersion(), update.getUrl())
            );
        } catch (Exception e) {
            log.warn("Error checking for version update", e);
            out.printf("Error checking for version update: %s%n%n", e.getMessage());
        }
    }

    @VisibleForTesting
    protected VersionUpdateChecker createVersionUpdateChecker() {
        VersionProvider versionProvider = new VersionProvider();
        HttpClient httpClient = HttpClient.newBuilder().followRedirects(NORMAL).build();
        return new VersionUpdateChecker(versionProvider, httpClient);
    }

    private record CommandFactory(TabletopSimulator tabletopSimulator) implements IFactory {

        @Override
        public <K> K create(Class<K> cls) throws Exception {
            if (AbstractCommand.class.isAssignableFrom(cls)) {
                return cls.getDeclaredConstructor(TabletopSimulator.class)
                        .newInstance(this.tabletopSimulator);
            }

            return CommandLine.defaultFactory().create(cls);
        }
    }
}
