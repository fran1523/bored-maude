/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.cache.Cleanup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CleanupCommandTest extends CommandTestBase {

    private CommandLine sut;

    @BeforeEach
    protected void setup() {
        this.sut = getTestCommandLine(CleanupCommand.class);
    }

    @Test
    void should_delete_orphans_when_orphans_are_found() {
        when(this.tabletopSimulator.cleanup(any(Options.class))).thenReturn(new Cleanup(Map.of(),
                Map.of(
                        IMAGE1_CACHE_FILE, 42027L,
                        AUDIO1_CACHE_FILE, 33430L
                )));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                No files renamed
                
                Orphans found: 2
                  %s
                  %s
                
                0 files renamed. 2 orphans deleted, 73 KB total
                """.formatted(AUDIO1_CACHE_FILE, IMAGE1_CACHE_FILE)
        );
    }

    @Test
    void should_rename_files_when_old_steam_cloud_files_are_found() {
        when(this.tabletopSimulator.cleanup(any(Options.class))).thenReturn(
                new Cleanup(Map.of(
                        OLD_STEAM_CLOUD_CACHE_FILE, NEW_STEAM_CLOUD_CACHE_FILE,
                        OLD_STEAM_CLOUD_CACHE_FILE_2, NEW_STEAM_CLOUD_CACHE_FILE_2
                ), Map.of()));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingWhitespace("""
                Files renamed: 2
                  %s -> %s
                  %s -> %s
                
                No orphan files found
                
                2 files renamed. 0 orphans deleted, 0 bytes total
                """.formatted(OLD_STEAM_CLOUD_CACHE_FILE, NEW_STEAM_CLOUD_CACHE_FILE,
                OLD_STEAM_CLOUD_CACHE_FILE_2, NEW_STEAM_CLOUD_CACHE_FILE_2)
        );
    }

    @Test
    void should_print_warning_when_no_orphans_are_found() {
        when(this.tabletopSimulator.cleanup(any(Options.class))).thenReturn(new Cleanup(Map.of(), Map.of()));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                No files renamed
                
                No orphan files found
                """);
    }

}
