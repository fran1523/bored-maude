/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.cache;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.UnitTestBase;
import hr.fran.bored.maude.tts.common.TtsFileUtils;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.ModInfoManager;
import hr.fran.bored.maude.tts.mod.ModManager;
import org.apache.commons.io.file.PathUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static hr.fran.bored.maude.tts.mod.AssetType.IMAGE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CleanupManagerTest extends UnitTestBase {

    @Mock
    private ModInfoManager modInfoManager;

    @Mock
    private ModManager modManager;

    private CleanupManager sut;

    @BeforeEach
    @Override
    protected void setup() {
        super.setup();
        this.sut = new CleanupManager(this.modInfoManager, this.modManager);
    }

    @Test
    void cleanup_should_return_no_results_when_all_files_belong_to_assets() {
        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Asset image1Asset = IMAGE1.withCacheFile(image1CacheFile);
        final Path image1RawCacheFile = givenRawCacheFile(IMAGE1);

        whenGetAssetsThenReturn(image1Asset);

        Cleanup result = this.sut.cleanup(testOptions);

        assertThat(result.deletedFiles()).isEmpty();
        assertThat(result.renamedFiles()).isEmpty();
        assertThat(image1CacheFile).exists();
        assertThat(image1RawCacheFile).exists();
    }

    @Test
    void cleanup_should_rename_old_steam_cloud_file() {
        Path oldSteamCloudCacheFile = givenCacheFile(IMAGE, OLD_STEAM_CLOUD_CACHE_FILENAME);
        Path newSteamCloudCacheFile = getExpectedCacheFile(IMAGE, NEW_STEAM_CLOUD_CACHE_FILENAME);

        whenGetAssetsThenReturn(NEW_STEAM_CLOUD_ASSET.withCacheFile(newSteamCloudCacheFile));

        Cleanup result = this.sut.cleanup(testOptions);

        assertThat(result.renamedFiles()).containsExactly(Map.entry(oldSteamCloudCacheFile, newSteamCloudCacheFile));
        assertThat(result.deletedFiles()).isEmpty();
        assertThat(oldSteamCloudCacheFile).doesNotExist();
        assertThat(newSteamCloudCacheFile).exists();

        verify(this.modManager, times(1)).invalidateTabletopSimulatorCache();
    }

    @Test
    void cleanup_should_delete_old_steam_cloud_file_if_new_file_exists() {
        Path oldSteamCloudCacheFile = givenCacheFile(IMAGE, OLD_STEAM_CLOUD_CACHE_FILENAME);
        Path newSteamCloudCacheFile = givenCacheFile(IMAGE, NEW_STEAM_CLOUD_CACHE_FILENAME);

        whenGetAssetsThenReturn(NEW_STEAM_CLOUD_ASSET.withCacheFile(newSteamCloudCacheFile));

        Cleanup result = this.sut.cleanup(testOptions);

        assertThat(result.renamedFiles()).isEmpty();
        assertThat(result.deletedFiles()).containsOnlyKeys(oldSteamCloudCacheFile);
        assertThat(oldSteamCloudCacheFile).doesNotExist();
        assertThat(newSteamCloudCacheFile).exists();

        verify(this.modManager, times(1)).invalidateTabletopSimulatorCache();
    }

    @Test
    void cleanup_should_delete_orphan_files() {
        Path image1CacheFile = givenCacheFileForAsset(IMAGE1);
        Path image1RawCacheFile = givenRawCacheFile(IMAGE1);
        Path model1RawCacheFile = givenRawCacheFile(MODEL1);
        Path pdf1CacheFile = givenCacheFileForAsset(PDF1);
        Asset pdf1Asset = PDF1.withCacheFile(pdf1CacheFile);

        whenGetAssetsThenReturn(pdf1Asset);

        Cleanup result = this.sut.cleanup(testOptions);

        assertThat(result.deletedFiles()).containsExactlyInAnyOrderEntriesOf(Map.of(
                image1CacheFile, 42027L,
                image1RawCacheFile, 0L,
                model1RawCacheFile, 0L
        ));
        assertThat(result.deletedFiles().keySet()).allSatisfy(file -> assertThat(file).doesNotExist());
        assertThat(pdf1CacheFile).exists();
    }

    private Path givenRawCacheFile(Asset asset) {
        try {
            String rawCacheDir = asset.getType().getRawCacheDir().orElseThrow();
            String filename = getRawCacheFilename(asset);
            Path rawCacheFile = this.testOptions.getModsDir().resolve(rawCacheDir).resolve(filename);
            return PathUtils.touch(rawCacheFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private String getRawCacheFilename(Asset asset) {
        String baseName = TtsFileUtils.getCacheFileBaseName(asset.getUrl());
        String extension = switch (asset.getType()) {
            case IMAGE -> "rawt";
            case MODEL -> "rawm";
            default -> throw new IllegalArgumentException(asset.getType().toString());
        };
        return "%s.%s".formatted(baseName, extension);
    }

    private void whenGetAssetsThenReturn(Asset... assets) {
        when(this.modInfoManager.getWorkshopAndSavesModInfos(any(Options.class)))
                .thenReturn(List.of(CHESS_MOD_INFO, CHESS_SAVE_INFO));
        when(this.modManager.getMod(eq(CHESS_MOD_INFO), any(Options.class)))
                .thenReturn(CHESS_MOD.withAssets(assets));
        when(this.modManager.getMod(eq(CHESS_SAVE_INFO), any(Options.class)))
                .thenReturn(CHESS_SAVE.withAssets(assets));
    }

}
