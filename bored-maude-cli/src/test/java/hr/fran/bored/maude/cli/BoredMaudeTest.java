/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli;

import hr.fran.bored.maude.cli.update.VersionUpdate;
import hr.fran.bored.maude.cli.update.VersionUpdateChecker;
import hr.fran.bored.maude.tts.TabletopSimulator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;
import uk.org.webcompere.systemstubs.stream.SystemErrAndOut;
import uk.org.webcompere.systemstubs.stream.output.MultiplexOutput;
import uk.org.webcompere.systemstubs.stream.output.Output;
import uk.org.webcompere.systemstubs.stream.output.TapStream;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SystemStubsExtension.class})
class BoredMaudeTest {

    @SystemStub
    protected final SystemErrAndOut systemErrAndOut = new SystemErrAndOut(
            new MultiplexOutput(new TapStream(), Output.fromStream(System.out))
    );

    @Mock
    private final VersionUpdateChecker versionUpdateChecker = mock(VersionUpdateChecker.class);

    @Mock
    private final TabletopSimulator tabletopSimulator = mock(TabletopSimulator.class);

    private BoredMaude sut;

    @BeforeEach
    protected void setup() {
        this.sut = spy(new BoredMaude());
        lenient().when(this.sut.createVersionUpdateChecker()).thenReturn(this.versionUpdateChecker);
        lenient().when(this.sut.createTabletopSimulator()).thenReturn(this.tabletopSimulator);
    }

    @Test
    void should_return_zero_when_there_are_no_errors() {
        int result = this.sut.execute(new String[]{"list"});

        assertThat(result).isZero();
    }

    @Test
    void should_return_non_zero_when_there_are_execution_errors() {
        when(this.tabletopSimulator.getModInfos(any(), any())).thenThrow(new RuntimeException("Runtime error"));

        int result = this.sut.execute(new String[]{"list"});

        assertThat(result).isNotZero();
        assertThat(this.systemErrAndOut.getText()).isEqualToIgnoringWhitespace(("Runtime error"));
    }

    @Test
    void should_print_message_when_there_is_a_version_update() throws Exception {
        String version = "v9999.0.0";
        String url = "https://gitlab.com/fran1523/bored-maude/-/releases";
        when(this.versionUpdateChecker.checkForUpdate()).thenReturn(Optional.of(
                new VersionUpdate.Builder().version(version).url(url).build()
        ));

        this.sut.execute(new String[]{"list"});

        assertThat(this.systemErrAndOut.getText())
                .startsWith("New version %s is available at %s".formatted(version, url));
    }

    @Test
    void should_print_no_message_when_there_is_no_version_update() {
        this.sut.execute(new String[]{"list"});

        assertThat(this.systemErrAndOut.getText()).doesNotContain("New version");
    }

    @Test
    void should_print_error_message_when_version_update_check_fails() throws Exception {
        when(this.versionUpdateChecker.checkForUpdate()).thenThrow(new RuntimeException("Update check failed"));

        this.sut.execute(new String[]{"list"});

        assertThat(this.systemErrAndOut.getText()).startsWith("Error checking for version update: Update check failed");
    }

}
