/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts;

import hr.fran.bored.maude.tts.mod.AssetType;
import hr.fran.bored.maude.tts.mod.ModType;

import java.net.http.HttpClient;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static hr.fran.bored.maude.tts.mod.ModType.MOD;
import static java.net.http.HttpClient.Redirect.NORMAL;
import static org.immutables.value.Value.Default;
import static org.immutables.value.Value.Immutable;
import static org.immutables.value.Value.Lazy;
import static org.immutables.value.Value.Style;

@Immutable
@Style(get = {"get*", "is*"}, typeImmutable = "*")
public abstract class AbstractOptions {

    private static final String TABLETOP_SIMULATOR_DIR = "Tabletop Simulator";

    private static final String[] LINUX_DIR = new String[]{".local", "share", TABLETOP_SIMULATOR_DIR};

    private static final String[] LINUX_SNAP_DIR = new String[]{"snap", "steam", "common", ".local", "share",
            TABLETOP_SIMULATOR_DIR};

    private static final String[] MACOS_DIR = new String[]{"Library", TABLETOP_SIMULATOR_DIR};

    private static final String[] WINDOWS_DIR = new String[]{"Documents", "My Games", TABLETOP_SIMULATOR_DIR};

    public abstract Optional<Integer> getConnectTimeoutInSeconds();

    @Default
    public Path getBackupDir() {
        return getFileSystem().getPath("");
    }

    @Default
    public Path getTtsDir() {
        // TODO verify Tabletop Simulator directory exists
        return getDefaultTtsDir();
    }

    private Path getDefaultTtsDir() {
        String osName = System.getProperty("os.name").toLowerCase(Locale.ROOT);
        String userHome = System.getProperty("user.home");

        if (osName.startsWith("windows")) {
            return getFileSystem().getPath(userHome, WINDOWS_DIR);
        }
        if (osName.startsWith("mac")) {
            return getFileSystem().getPath(userHome, MACOS_DIR);
        }
        // default to Linux
        Path snapDir = getFileSystem().getPath(userHome, LINUX_SNAP_DIR);
        if (Files.isDirectory(snapDir)) {
            return snapDir;
        }
        return getFileSystem().getPath(userHome, LINUX_DIR);
    }

    @Default
    protected FileSystem getFileSystem() {
        return FileSystems.getDefault();
    }

    @Default
    public boolean isBackupIncompleteMods() {
        return false;
    }

    @Default
    public boolean isOverwriteCacheFiles() {
        return false;
    }

    @Default
    public ModType getModType() {
        return MOD;
    }

    @Default
    public HttpClient getHttpClient() {
        HttpClient.Builder builder = HttpClient.newBuilder()
                .followRedirects(NORMAL);
        getConnectTimeoutInSeconds().ifPresent(timeout ->
                builder.connectTimeout(Duration.ofSeconds(timeout))
        );
        return builder.build();
    }

    @Lazy
    public Path getModFilesDir() {
        return switch (getModType()) {
            case MOD -> getWorkshopDir();
            case SAVE -> getSavesDir();
        };
    }

    @Lazy
    public Path getSavesDir() {
        return getTtsDir().resolve("Saves");
    }

    @Lazy
    public Path getWorkshopDir() {
        return getModsDir().resolve("Workshop");
    }

    @Lazy
    public Path getModsDir() {
        return getTtsDir().resolve("Mods");
    }

    @Lazy
    public Path getCacheDir(AssetType type) {
        return getModsDir().resolve(type.getCacheDir());
    }

    @Lazy
    public Collection<Path> getTtsBackupRestoreDirs() {
        Stream<Path> cacheDirs = Stream.of(AssetType.values())
                .map(this::getCacheDir);
        Stream<Path> modDirs = Stream.of(
                getSavesDir(),
                getWorkshopDir()
        );
        return Stream.concat(
                cacheDirs,
                modDirs
        ).collect(Collectors.toUnmodifiableSet());
    }

}
