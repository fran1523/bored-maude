/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.backup;

import hr.fran.bored.maude.tts.mod.Mod;
import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Lazy;

import java.nio.file.Path;

@Immutable
public interface BackupFile extends WithBackupFile {

    Path getFile();

    Mod getMod();

    @Lazy
    default boolean isModIncomplete() {
        return getMod().hasUncachedNonAudioAssets();
    }

    class Builder extends ImmutableBackupFile.Builder {
    }

}
