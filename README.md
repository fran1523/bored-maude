[[_TOC_]]

# Bored Maude

Bored Maude is a command line tool for backing up, restoring and
auditing [Tabletop Simulator](https://www.tabletopsimulator.com/) mods.

You can use it to create a local backup of a game in case its online assets become broken, or to audit your games to see
if any have issues and should be replaced with a different mod.

It supports only JSON format mods, so backup old binary mod create a save first, which will convert it to JSON format.

Workshop mods and saves are both supported, and backups created
by [TTS Mod Backup](https://www.nexusmods.com/tabletopsimulator/mods/263) can be restored.

## Installation

### Requirements

Bored Maude requires [Java 17](https://java.com/en/download/help/download_options.html) or later.

It runs on all operating systems Tabletop Simulator supports: Linux, macOS and Windows.

### Setup

Download the [latest release](https://gitlab.com/fran1523/bored-maude/-/releases). Unpack the zip archive containing
application executables, `bored-maude` for Linux and macOS and `bored-maude.bat` for Windows.

## Usage

Bored Maude is a command line tool. To use it, open a terminal (Linux and macOS) or a command prompt (Windows), and
run `./bored-maude` with command parameters.

To see all available commands and parameters, check help first.

```
./bored-maude --help
```

This lists available commands with brief descriptions.

```
Usage: bored-maude [-hV] [COMMAND]
Tabletop Simulator mod manager
  -h, --help      Show this help message and exit.
  -V, --version   Print version information and exit.
Commands:
  audit     Audits mod assets' availability and file content
  backup    Downloads missing assets and creates a mod backup archive
  cleanup   Deletes orphan assets not used by any installed mod from Tabletop
              Simulator cache
  download  Downloads mod assets into Tabletop Simulator cache
  info      Displays detailed info for installed mods
  list      Lists installed mods
  restore   Restores mod and its assets from a backup archive
```

For more info about a specific command, see help for that command.

```
./bored-maude list --help
```

This will show detailed information and available parameters.

### List

Shows a list of installed mods. List command parameters are common, and used in some other commands as well.

- _path_: Optional, path to Tabletop Simulator data directory (e.g. _C:\Users\\\<user>\Documents\My Games\Tabletop
  Simulator_). The correct Tabletop Simulator path should be automatically detected on all supported platforms, but this
  provides a way to override it
- _saves_: If this flag is enabled, run the command on saves instead of workshop mods
- _filters_: Filter mods to run the command on by id or partial name, case-insensitive. Multiple filters can be
  provided, or none to run the command on all mods

If a mod is marked with "*" in the list, it means some of its assets are not cached. If a download was run for that mod,
it might indicate asset issues.

#### Examples

List all installed mods.

```
./bored-maude list
```

```
      Id Name                                              Updated              
--------------------------------------------------------------------------------
23456789 Checkers                                          1/1/2021 2:01:02 AM  
12345678 Chess                                             1/1/2021 2:01:01 AM *

2 mods found
```

List mods from a custom Tabletop Simulator directory.

```
./bored-maude list --path C:\Music
or
./bored-maude list -p C:\Music
```

```
No mods found
```

List all mods having id "12345678", or containing the word "check" in the title, ignoring case.

```
./bored-maude list 12345678 check
```

```
      Id Name                                              Updated              
--------------------------------------------------------------------------------
23456789 Checkers                                          1/1/2021 2:01:02 AM  
12345678 Chess                                             1/1/2021 2:01:01 AM *

1 mods found
```

### Info

Shows detailed mod info. Parameters are the same as for [List](#list).

#### Examples

Show info for mods with id 12345678.

```
./bored-maude info 12345678
```

```
Chess
=====
File   : /home/fran/Tabletop Simulator/Mods/Workshop/12345678.json
Id     : 12345678
Updated: 1/1/2021 2:01:01 AM
Version: v12.3.4
Assets : 2/4 cached
  ASSETBUNDLE: 0/1 cached
  AUDIO      : 0/1 cached
  IMAGE      : 1/1 cached
  MODEL      : 0
  PDF        : 1/1 cached
Errors : 2
  http://www.test.com/asset/1
    Type : ASSETBUNDLE
    Error: 404 Not Found
  https://www.test.com/notapdf/1
    Type : PDF
    File : /home/Tabletop Simulator/Mods/PDF/httpswwwtestcomnotapdf1.PDF
    Error: Content type image/jpeg is not valid for PDF

1 mods found
```

Show info for saves with title containing "chess".

```
./bored-maude info --saves chess
or
./bored-maude info -s chess
```

```
Chess
=====
File   : /home/fran/Tabletop Simulator/Saves/TS_Save_1.json
Id     : TS_Save_1
Updated: 1/2/2021 2:01:01 AM
Version: v12.3.4
Assets : 4/4 cached
  ASSETBUNDLE: 1/1 cached
  AUDIO      : 1/1 cached
  IMAGE      : 1/1 cached
  MODEL      : 0
  PDF        : 1/1 cached
Errors : 0

1 mods found
```

### Download

Downloads into Tabletop Simulator cache all mod assets not already cached. In addition to the common parameters _path_,
_saves_ and _filters_, it supports the following options:

- _dry-run_: Test mode, do not save downloaded assets into Tabletop Simulator cache
- _overwrite_: Download all assets, including the ones already cached, and overwrite existing files
- _connect-timeout_: Set HTTP connection timeout in seconds. If the connections is slow and downloads frequently time
  out, try setting this to 3 seconds and increase if needed

When downloading file, it will check if the file type matches expected, e.g. if image url returns a plain text it is
probably an error page.

#### Examples

Download assets for all mods with connect timeout 5 seconds.

```
./bored-maude download --connect-timeout 5
or
./bored-maude download -t 5
```

```
Checkers [23456789]
===================
Downloading http://www.test.com/asset/1 ... ERROR: 404 Not Found
Download finished, 0 new assets downloaded, 1 errors

Chess [12345678]
================
Downloading http://www.test.com/audio/1 ... saved as '/home/Tabletop Simulator/Mods/Audio/httpwwwtestcomaudio1.mp3'
Download finished, 1 new assets downloaded, 0 errors

2 mods downloaded, 1 mods have errors
```

Download assets for "chess" in dry run mode, overwriting existing assets in the cache.

```
./bored-maude download --overwrite --dry-run chess
or
./bored-maude download -o -d chess
```

```
Chess [12345678]
================
Downloading http://www.test.com/audio/1 ... saved as '/home/Tabletop Simulator/Mods/Audio/httpwwwtestcomaudio1.mp3'
Download finished, 1 new assets downloaded, 0 errors

1 mods downloaded (dry run)
```

### Backup

Downloads any missing assets (same as [Download](#download) command), and then creates the mod backup: zip archive
containing the mod and all its assets. In addition to the common parameters _path_, _saves_ and _filters_, it supports
all [Download](#download) parameters, and the following options.

- _backup-dir_: Save mod backup files to this directory. If backup dir is not provided, use current directory
- _backup-incomplete_: Create a backup even if some non-audio assets could not be downloaded. Missing audio assets are
  always ignored
- _dry-run_: Do not save downloaded files or create backup archive

If some assets cannot be downloaded, backup will fail and backup archive will not be created. However, this does not
apply to missing audio assets, as they are usually not critical and their absence will not break the mod.

#### Examples

Backup all mods, overwriting all existing assets when downloading.

```
./bored-maude backup --overwrite
or
./bored-maude backup -o
```

```
Checkers [23456789] backup started ... failed: Some assets are not downloaded
Chess [12345678] backup started ... saved to 'Chess (12345678).zip'

1/2 mods backed up, 1 backups failed
```

Backup "chess" to _Backups/_ directory, ignoring missing assets if any.

```
./bored-maude backup --backup-incomplete --backup-dir Backups chess
or
./bored-maude backup --i --b Backups chess
```

```
Chess [12345678] backup started ... saved to 'backup/Chess (12345678).zip' (incomplete)

1/1 mods backed up, 1 backups incomplete
```

### Restore

Copies assets from a mod backup archive into Tabletop Simulator cache. It supports _.zip_ backups created by Bored
Maude, as well as _.ttsmod_ backups created by [TTS Mod Backup](https://www.nexusmods.com/tabletopsimulator/mods/263).

Note that restore will always overwrite existing files.

Restore command supports the following options.

- _dry-run_: Extract backup archive but do not copy files into Tabletop Simulator cache
- _backups_: Wildcard patterns matching backup files to restore.
  See [Glob Expression Understanding](https://mincong.io/2019/04/16/glob-expression-understanding/) for syntax and
  examples

#### Examples

Restore mods from backup zip files whose names start with Chess or Checkers.

```
./bored-maude restore Backups/Chess*.zip Backups/Checkers*.zip
```

```
Checkers [23456789] restored from 'Backups/Checkers (23456789).zip'
Chess [12345678] restored from 'Backups/Chess (12345678).zip'

2/2 mods restored, 0 restores failed
```

### Audit

Audits all mod assets for errors or unavailability. This is essentially shorthand for

```
./bored-maude download --dry-run --overwrite
./bored-maude info
```

Supports the following options.

- _saves_: If this flag is enabled, run the command on saves instead of workshop mods
- _connect-timeout_: Set HTTP connection timeout in seconds. If the connections is slow and downloads frequently time
  out, try setting this to 3 seconds and increase if needed
- _filters_: Filter mods to run the command on by id or partial name, case-insensitive. Multiple filters can be
  provided, or none to run the command on all mods

#### Examples

Audit "Checkers" mods.

```
./bored-maude audit Checkers
```

```
Checkers
========
File   : /home/fran/Tabletop Simulator/Mods/Workshop/23456789.json
Id     : 23456789
Updated: 1/1/2021 2:01:02 AM
Version: v12.3.4
Assets : 0
  ASSETBUNDLE: 0
  AUDIO      : 0
  IMAGE      : 0
  MODEL      : 0
  PDF        : 0
Errors : 0

1 mods found
```

### Cleanup

Cleans up orphaned files from Tabletop Simulator cache, not linked from any current mod.

- _dry-run_: Print list only, do not delete files

#### Examples

Run cleanup.

```
./bored-maude cleanup
```

```
Orphans found: 
  /home/Tabletop Simulator/Mods/Audio/httpwwwtestcomaudio1.mp3
  /home/Tabletop Simulator/Mods/Images/httpwwwtestcomimage1.jpg

2 orphans deleted, 73 KB total
```

## Troubleshooting and Support

All logs are located in _./logs_, and contain detailed information on why an operation failed. If the error is
unexpected, you can post the log and any additional information to a new GitLab issue or Steam discussion.

Please post any feedback, issues and suggestions
to [Steam](https://steamcommunity.com/app/286160/discussions/0/3185738120272926052/).

## License

[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
