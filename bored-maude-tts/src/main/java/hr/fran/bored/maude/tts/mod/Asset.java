/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.common.SteamCloud;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.immutables.value.Value.Lazy;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Locale;

import static org.immutables.value.Value.Immutable;

@Immutable
public abstract class Asset implements WithAsset, Comparable<Asset> {

    private static final Comparator<Asset> COMPARATOR =
            Comparator.comparing(Asset::getType).thenComparing(Asset::getUrl);

    public abstract AssetType getType();

    public abstract String getJsonUrl();

    public abstract @Nullable Path getCacheFile();

    public abstract @Nullable String getError();

    @Lazy
    public String getUrl() {
        return SteamCloud.toNewUrl(getJsonUrl());
    }

    @Lazy
    public boolean isCached() {
        return getCacheFile() != null;
    }

    @Lazy
    public boolean hasError() {
        return getError() != null;
    }

    @Lazy
    public @Nullable String getChecksum() {
        if (SteamCloud.isSteamCloudUrl(getJsonUrl())) {
            return getSha1FromUrl();
        }
        return getSha1FromFile();
    }

    private String getSha1FromUrl() {
        String urlPath = FilenameUtils.getPathNoEndSeparator(getUrl());
        return FilenameUtils.getName(urlPath).toLowerCase(Locale.ROOT);
    }

    private @Nullable String getSha1FromFile() {
        Path cacheFile = getCacheFile();
        if (cacheFile == null) {
            return null;
        }

        try (InputStream is = Files.newInputStream(cacheFile);
             BufferedInputStream bis = new BufferedInputStream(is)
        ) {
            return DigestUtils.sha1Hex(bis);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public int compareTo(@NonNull Asset o) {
        return COMPARATOR.compare(this, o);
    }

    @Override
    public String toString() {
        return "%s %s".formatted(getType(), getUrl());
    }

    public static class Builder extends ImmutableAsset.Builder {
    }

}
