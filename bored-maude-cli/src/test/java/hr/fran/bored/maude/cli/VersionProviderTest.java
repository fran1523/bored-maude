/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli;

import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

class VersionProviderTest {

    private static final Pattern SEMANTIC_VERSION = Pattern.compile("^\\d+\\.\\d+\\.\\d+");

    private final VersionProvider sut = new VersionProvider();

    @Test
    void should_match_version_pattern() {
        String[] result = this.sut.getVersion();

        assertThat(result).hasSize(1);
        assertThat(result[0]).containsPattern(SEMANTIC_VERSION);
    }
}
