/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;

import java.util.List;
import java.util.function.Function;

import static picocli.CommandLine.Command;

@Command(name = "list",
        description = "List installed mods",
        mixinStandardHelpOptions = true)
public class ListCommand extends AbstractListCommand {

    protected static final int TERMINAL_WIDTH = 80;
    private static final String ALIGN_LEFT = "-";
    private static final String ALIGN_RIGHT = "";

    public ListCommand(TabletopSimulator tabletopSimulator) {
        super(tabletopSimulator);
    }

    @Override
    public void run() {
        Options options = getOptions();
        ModFilter modFilter = getModFilter();
        List<ModInfo> modInfos = getSortedModInfos(modFilter, options);

        printModList(modInfos, options);
    }

    private void printModList(List<ModInfo> modInfos, Options options) {
        if (modInfos.isEmpty()) {
            // TODO move to superclass
            getOut().printf("No mods found%s%n", getFilterSuffix());
            return;
        }

        printModTable(modInfos, options);
        getOut().println();
        printSummary(modInfos.size());
    }

    private void printModTable(List<ModInfo> modInfos, Options options) {
        String tableRowFormat = getTableRowFormat(modInfos, options);

        printTableHeader(tableRowFormat);
        printTableSeparator();
        for (ModInfo modInfo : modInfos) {
            Mod mod = this.tabletopSimulator.getMod(modInfo, options);
            printTableRow(tableRowFormat, mod);
        }
    }

    private String getTableRowFormat(List<ModInfo> modInfos, Options options) {
        String columnIdAlignment = switch (options.getModType()) {
            case MOD -> ALIGN_RIGHT;
            case SAVE -> ALIGN_LEFT;
        };
        int columnIdWidth = getMaxValueLength(modInfos, ModInfo::getId);
        int columnUpdatedWidth = getMaxValueLength(modInfos, ModInfo::getDate);
        int columnIssuesWidth = 1;
        int columnNameWidth = TERMINAL_WIDTH - columnIdWidth - columnUpdatedWidth - columnIssuesWidth - 3;

        return "%" + columnIdAlignment + columnIdWidth + "s"
                + " %" + ALIGN_LEFT + columnNameWidth + "." + columnNameWidth + "s"
                + " %" + ALIGN_LEFT + columnUpdatedWidth + "." + columnUpdatedWidth + "s"
                + " %1.1s%n";
    }

    private int getMaxValueLength(List<ModInfo> modInfos, Function<ModInfo, String> getValue) {
        int defaultLength = 10;
        int maxLength = 25;

        int length = modInfos.stream()
                .map(getValue)
                .mapToInt(String::length)
                .max()
                .orElse(defaultLength);

        return Math.min(length, maxLength);
    }

    private void printTableHeader(String tableRowFormat) {
        getOut().printf(tableRowFormat, "Id", "Name", "Updated", "");
    }

    protected void printTableSeparator() {
        getOut().println("-".repeat(TERMINAL_WIDTH));
    }

    private void printTableRow(String tableRowFormat, Mod mod) {
        ModInfo modInfo = mod.getModInfo();
        getOut().printf(tableRowFormat,
                modInfo.getId(),
                modInfo.getName(),
                modInfo.getDate(),
                hasIssues(mod) ? "*" : ""
        );
    }

    private boolean hasIssues(Mod mod) {
        return mod.getAssets().stream()
                .anyMatch(asset -> !asset.isCached() || asset.hasError());
    }

    private void printSummary(int modCount) {
        getOut().printf("%s mods found%n", modCount);
    }

}
