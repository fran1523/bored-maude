/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TabletopSimulator;
import hr.fran.bored.maude.tts.mod.Asset;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import hr.fran.bored.maude.tts.mod.ModInfo;
import org.checkerframework.checker.nullness.qual.Nullable;
import picocli.CommandLine.Model.CommandSpec;

import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

import static picocli.CommandLine.Option;
import static picocli.CommandLine.Spec;

public abstract class AbstractCommand implements Runnable {

    // TODO
    protected static final String INDENT = " ".repeat(2);

    @Spec
    private CommandSpec commandSpec;

    @Option(
            names = {"-p", "--path"},
            description = """
                    Tabletop Simulator path, e.g. \
                    "C:\\Users\\[Profile]\\Documents\\My Games\\Tabletop Simulator". \
                    Optional, should be autodetected"""
    )
    private @Nullable Path path;

    protected final TabletopSimulator tabletopSimulator;

    protected AbstractCommand(TabletopSimulator tabletopSimulator) {
        this.tabletopSimulator = tabletopSimulator;
    }

    protected PrintWriter getOut() {
        return this.commandSpec.commandLine().getOut();
    }

    protected Options.Builder getOptionsBuilder() {
        Options.Builder builder = Options.builder();
        if (this.path != null) {
            builder.ttsDir(this.path);
        }

        return builder;
    }

    protected Options getOptions() {
        return getOptionsBuilder().build();
    }

    protected List<ModInfo> getSortedModInfos(ModFilter modFilter, Options options) {
        return this.tabletopSimulator.getModInfos(modFilter, options).stream()
                .sorted()
                .toList();
    }

    protected void printTitle(String title) {
        getOut().println(title);
        getOut().println("=".repeat(title.length()));
    }

    protected void printNamedValue(Object name, int nameWidth, @Nullable Object value) {
        printNamedValue(0, name, nameWidth, value);
    }

    protected void printNamedValue(int indentLevel, Object name, int nameWidth, @Nullable Object value) {
        getOut().print(INDENT.repeat(indentLevel));
        String format = "%-" + nameWidth + "s: %s%n";
        getOut().printf(format, name, Objects.toString(value, ""));
    }

    protected void printAsset(int indentLevel, Asset asset) {
        getOut().print(INDENT.repeat(indentLevel));
        getOut().printf("%s%n", asset.getUrl());

        int indentSubLevel = indentLevel + 1;
        int nameWidth = asset.hasError() ? 5 : 4;
        printNamedValue(indentSubLevel, "Type", nameWidth, asset.getType());
        if (asset.isCached()) {
            printNamedValue(indentSubLevel, "File", nameWidth, asset.getCacheFile());
        }
        if (asset.hasError()) {
            printNamedValue(indentSubLevel, "Error", nameWidth, asset.getError());
        }
    }

    protected String getDisplayName(Mod mod) {
        ModInfo modInfo = mod.getModInfo();
        String name = modInfo.getName();
        return name.contains(" ")
                ? "'%s' [%s]".formatted(name, modInfo.getId())
                : "%s [%s]".formatted(name, modInfo.getId());
    }

    protected String getModIncompleteSuffix(boolean isModIncomplete) {
        return isModIncomplete
                ? " (incomplete)"
                : "";
    }

}
