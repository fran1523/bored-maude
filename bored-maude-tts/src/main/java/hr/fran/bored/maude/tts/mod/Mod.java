/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Lazy;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

import static hr.fran.bored.maude.tts.mod.AssetType.AUDIO;

@Immutable
public abstract class Mod implements WithMod, Comparable<Mod> {

    private static final Comparator<Mod> COMPARATOR = Comparator.comparing(Mod::getModInfo);

    public abstract ModInfo getModInfo();

    @Default
    public Collection<Asset> getAssets() {
        return Set.of();
    }

    @Lazy
    public Collection<Asset> getAssets(AssetType type) {
        return getAssets().stream()
                .filter(asset -> asset.getType() == type)
                .collect(Collectors.toUnmodifiableSet());
    }

    @Lazy
    public Collection<Asset> getAssetsWithError() {
        return getAssets().stream()
                .filter(Asset::hasError)
                .collect(Collectors.toUnmodifiableSet());
    }

    @Lazy
    public long getCachedAssetsCount() {
        return getAssets().stream()
                .filter(Asset::isCached)
                .count();
    }

    @Lazy
    public boolean hasUncachedNonAudioAssets() {
        return getAssets().stream()
                .anyMatch(asset -> !asset.isCached() && asset.getType() != AUDIO);
    }

    public Mod withAssets(Asset... assets) {
        return this.withAssets(Arrays.asList(assets));
    }

    @Override
    public int compareTo(@NonNull Mod o) {
        return COMPARATOR.compare(this, o);
    }

    @Override
    public String toString() {
        return getModInfo().toString();
    }

    public static class Builder extends ImmutableMod.Builder {
    }

}
