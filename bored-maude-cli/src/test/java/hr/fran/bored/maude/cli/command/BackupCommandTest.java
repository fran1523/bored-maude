/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.cli.command;

import hr.fran.bored.maude.tts.Options;
import hr.fran.bored.maude.tts.TtsException;
import hr.fran.bored.maude.tts.backup.BackupFile;
import hr.fran.bored.maude.tts.mod.Mod;
import hr.fran.bored.maude.tts.mod.ModFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import static hr.fran.bored.maude.tts.mod.ModType.SAVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class BackupCommandTest extends CommandTestBase {

    private CommandLine sut;

    @BeforeEach
    protected void setup() {
        this.sut = getTestCommandLine(BackupCommand.class);
    }

    @Test
    void should_backup_mod_when_using_default_parameters() {
        Path backupFile = Path.of("Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), any(Options.class)))
                .thenReturn(backupFileOf(CHESS_CACHED, backupFile));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... saved to '%s'

                1/1 mods backed up
                """.formatted(backupFile)
        );
    }

    @Test
    void should_backup_mod_when_backup_dir_is_provided() {
        Path backupFile = Path.of("Backups", "Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), argThat(
                options -> options.getBackupDir().toString().equals("Backups")
        ))).thenReturn(backupFileOf(CHESS_CACHED, backupFile));

        this.sut.execute("--backup-dir", "Backups");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... saved to '%s'

                1/1 mods backed up
                """.formatted(backupFile)
        );
    }

    @Test
    void should_backup_mod_when_backupIncompleteMods_is_enabled() {
        Path backupFile = Path.of("Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), argThat(Options::isBackupIncompleteMods)))
                .thenReturn(backupFileOf(CHESS, backupFile));

        this.sut.execute("--backup-incomplete");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... saved to '%s' (incomplete)

                1/1 mods backed up, 1 backups incomplete
                """.formatted(backupFile)
        );
    }

    @Test
    void should_backup_mod_when_http_timeout_is_provided() {
        Path backupFile = Path.of("Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), argThat(
                options -> options.getConnectTimeoutInSeconds().equals(Optional.of(60))
        ))).thenReturn(backupFileOf(CHESS_CACHED, backupFile));

        this.sut.execute("--connect-timeout", "60");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... saved to '%s'
                
                1/1 mods backed up
                """.formatted(backupFile)
        );
    }

    @Test
    void should_backup_mod_when_overwrite_is_enabled() {
        Path backupFile = Path.of("Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), argThat(Options::isOverwriteCacheFiles)))
                .thenReturn(backupFileOf(CHESS_CACHED, backupFile));

        this.sut.execute("--overwrite");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... saved to '%s'
                
                1/1 mods backed up
                """.formatted(backupFile)
        );
    }

    @Test
    void should_backup_mod_when_backing_up_saves() {
        Path backupFile = Path.of("Chess (TS_Save_1).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_SAVE_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_SAVE_INFO), any(Options.class))).thenReturn(CHESS_SAVE);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS_SAVE),
                argThat(options -> options.getModType() == SAVE))).thenReturn(backupFileOf(CHESS_SAVE, backupFile));

        this.sut.execute("--saves");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [TS_Save_1] backup started ... saved to '%s'
                
                1/1 mods backed up
                """.formatted(backupFile)
        );
    }

    @Test
    void should_backup_mod_when_filter_is_provided() {
        Path backupFile = Path.of("Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(new ModFilter(CHESS_INFO.getId())), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), any(Options.class)))
                .thenReturn(backupFileOf(CHESS_CACHED, backupFile));

        this.sut.execute("12345678");

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... saved to '%s'
                
                1/1 mods backed up
                """.formatted(backupFile)
        );
    }

    @Test
    void should_print_error_when_backup_fails() {
        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), any(Options.class)))
                .thenThrow(new TtsException("Some assets are not downloaded"));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Chess [12345678] backup started ... failed: Some assets are not downloaded
                
                0/1 mods backed up, 1 backups failed
                """
        );
    }

    @Test
    void should_backup_multiple_mods_when_some_backups_fail() {
        Path backupFile = Path.of("Chess (12345678).zip");

        when(this.tabletopSimulator.getModInfos(eq(ModFilter.MATCH_ALL), any(Options.class)))
                .thenReturn(List.of(CHESS_INFO, CHECKERS_INFO));
        when(this.tabletopSimulator.getMod(eq(CHESS_INFO), any(Options.class))).thenReturn(CHESS);
        when(this.tabletopSimulator.getMod(eq(CHECKERS_INFO), any(Options.class))).thenReturn(CHECKERS);
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHESS), any(Options.class)))
                .thenReturn(backupFileOf(CHESS_CACHED, backupFile));
        when(this.tabletopSimulator.downloadAndCreateBackup(eq(CHECKERS), any(Options.class)))
                .thenThrow(new TtsException("Some assets are not downloaded"));

        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                Checkers [23456789] backup started ... failed: Some assets are not downloaded
                Chess [12345678] backup started ... saved to '%s'
                
                1/2 mods backed up, 1 backups failed
                """.formatted(backupFile)
        );
    }

    @Test
    void should_print_warning_when_no_mods_match_filter() {
        this.sut.execute();

        assertThat(systemErrAndOut.getText()).isEqualToNormalizingNewlines("""
                No mods found
                """);
    }

    private BackupFile backupFileOf(Mod mod, Path file) {
        return new BackupFile.Builder().mod(mod).file(file).build();
    }

}
