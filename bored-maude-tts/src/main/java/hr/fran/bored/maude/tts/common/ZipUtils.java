/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.common;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public final class ZipUtils {

    private ZipUtils() {
    }

    public static <T> Stream<T> applyToZipEntries(
            Path zipFile,
            BiFunction<ZipEntry, ZipInputStream, Optional<T>> function
    ) {
        try (InputStream in = Files.newInputStream(zipFile);
             BufferedInputStream buffIn = new BufferedInputStream(in);
             ZipInputStream zipIn = new ZipInputStream(buffIn)
        ) {
            List<T> results = new ArrayList<>();

            for (ZipEntry zipEntry = zipIn.getNextEntry();
                 zipEntry != null;
                 zipEntry = zipIn.getNextEntry()
            ) {
                if (zipEntry.isDirectory()) {
                    continue;
                }

                Optional<T> result = function.apply(zipEntry, zipIn);
                result.ifPresent(results::add);
            }

            return results.stream();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
