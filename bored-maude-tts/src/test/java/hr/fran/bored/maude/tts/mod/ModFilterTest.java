/*
 * Copyright (C) 2021-2024 Fran
 *
 * This file is part of Bored Maude.
 *
 * Bored Maude is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bored Maude is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bored Maude. If not, see <http://www.gnu.org/licenses/>.
 */

package hr.fran.bored.maude.tts.mod;

import hr.fran.bored.maude.tts.UnitTestBase;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ModFilterTest extends UnitTestBase {

    @Test
    void matchAll_should_return_true_when_matching_any_mod() {
        assertThat(ModFilter.MATCH_ALL.matches(CHESS_MOD_INFO)).isTrue();
    }

    @Test
    void should_return_true_when_mod_id_matches() {
        assertThat(new ModFilter(CHESS_MOD_INFO.getId()).matches(CHESS_MOD_INFO)).isTrue();
    }

    @Test
    void should_return_true_when_partial_mod_name_matches() {
        assertThat(new ModFilter("ess").matches(CHESS_MOD_INFO)).isTrue();
    }

    @Test
    void should_return_false_when_mod_does_not_match() {
        assertThat(new ModFilter("checkers").matches(CHESS_MOD_INFO)).isFalse();
    }

    @Test
    void should_return_true_when_any_filter_matches() {
        assertThat(new ModFilter("chess", "checkers").matches(CHESS_MOD_INFO)).isTrue();
    }

    @Test
    void should_return_false_when_no_filters_match() {
        assertThat(new ModFilter("backgammon", "checkers").matches(CHESS_MOD_INFO)).isFalse();
    }

    @Test
    void equals_should_return_true_when_filters_are_the_same() {
        ModFilter first = new ModFilter("chess");
        ModFilter second = new ModFilter(List.of("chess", "chess"));

        assertThat(first).isEqualTo(second).hasSameHashCodeAs(second);
    }

}
